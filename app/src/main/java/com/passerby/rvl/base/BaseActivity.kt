package com.passerby.rvl.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/22 23:22
 */
abstract class BaseActivity : AppCompatActivity() {

    internal lateinit var originView: View

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var bindView = bindContentView()
        if (bindView == null) {
            val layoutId = bindLayoutId()
            if (layoutId != 0) {
                bindView = LayoutInflater.from(this)
                    .inflate(layoutId, null, false)
            } else {
                bindView = FrameLayout(this)
            }
        }
        originView = bindView!!
        setContentView(bindView)
    }

    abstract fun bindLayoutId(): Int

    open fun bindContentView(): View? = null

}

class ViewBindingProvider<VB : ViewBinding>(
    private val activity: BaseActivity,
    private val vbClass: Class<VB>
) : ReadOnlyProperty<BaseActivity, VB> {

    private lateinit var binding: VB

    override fun getValue(thisRef: BaseActivity, property: KProperty<*>): VB {
        if (!::binding.isInitialized) {
            val bind = vbClass.getDeclaredMethod("bind", View::class.java)
            binding = bind.invoke(null, activity.originView) as VB
        }
        return binding
    }
}


fun <VB : ViewBinding> BaseActivity.viewBinding(vb: Class<VB>): ViewBindingProvider<VB> {
    return ViewBindingProvider(this, vb)
}