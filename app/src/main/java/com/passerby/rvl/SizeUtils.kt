package com.passerby.rvl

import android.content.res.Resources
import android.util.TypedValue

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/28 0:20
 */

val Float.dpf: Float
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
        this, Resources.getSystem().displayMetrics)

val Float.dp: Int
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
        this, Resources.getSystem().displayMetrics).toInt()

val Int.dpf: Float
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(), Resources.getSystem().displayMetrics)

val Int.dp: Int
    get() = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
        this.toFloat(), Resources.getSystem().displayMetrics).toInt()