package com.passerby.rvl

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.passerby.rvl.base.BaseActivity
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.test.RecyclerActivity
import com.passerby.rvl.test.decoration.RecyclerCheckableActivity
import com.passerby.rvl.test.nestableAdapter.RecyclerNestedAdapterActivity

import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.recyclerviewlib.recycler.decoration.DividerColor
import com.passerby.recyclerviewlib.recycler.decoration.LinearItemDecoration
import com.passerby.rvl.databinding.ActivityMainBinding
import com.passerby.rvl.databinding.ItemMainFolderBinding

class MainActivity : BaseActivity() {

    private val binding by viewBinding(ActivityMainBinding::class.java)

    override fun bindLayoutId() = R.layout.activity_main

    private val items by lazy {
        arrayListOf(
            Route.RecyclerItemDecoration,
            Route.RecyclerCheckable,
            Route.RecyclerNestableAdapter
        )
    }

    private val adapter by lazy {
        object : RvAdapter<Route, RvHolder<Route>>(this) {
            override fun getLayoutId(viewType: Int): Int {
                return R.layout.item_main_folder
            }

            override fun createHolder(context: Context, itemView: View,
                viewType: Int): RvHolder<Route> {
                return object : RvHolder<Route>(context, itemView) {
                    private val binding by lazy { ItemMainFolderBinding.bind(itemView) }
                    override fun onUpdate(item: Route, position: Int) {
                        binding.tvName.text = item.itemName
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.rv.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rv.addItemDecoration(LinearItemDecoration().apply {
            setSpacingHeight(20.dp)
            setDividerColor(DividerColor(0xFF000000.toInt()))
        })
        binding.rv.adapter = adapter
        adapter.addItems(items)
        adapter.addOnItemClickListener { _, item, _ ->
            startActivity(Intent(this@MainActivity, item.targetClass))
        }


    }


    enum class Route(val itemName: String, val targetClass: Class<*>) {
        //分割线
        RecyclerItemDecoration("RecyclerItemDecoration", RecyclerActivity::class.java),
        RecyclerCheckable("RecyclerCheckable", RecyclerCheckableActivity::class.java),
        RecyclerNestableAdapter("嵌套Adapter", RecyclerNestedAdapterActivity::class.java),
    }
}