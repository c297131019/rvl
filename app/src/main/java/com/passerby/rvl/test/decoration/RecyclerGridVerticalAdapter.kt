package com.passerby.rvl.test.decoration

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridVerticalOneBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/12/5 18:24
 */
class RecyclerGridVerticalAdapter(context: Context) :
    RvAdapter<User, RvHolder<User>>(context) {

    override fun getItemViewType(position: Int): Int {
        return R.layout.item_recycler_grid_vertical
    }

    override fun getLayoutId(viewType: Int): Int {
        return viewType
    }

    override fun createHolder(context: Context, itemView: View,
        viewType: Int): RvHolder<User> {
        return GridVerticalHolder(context, itemView)
    }

    class GridVerticalHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridVerticalOneBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }
}