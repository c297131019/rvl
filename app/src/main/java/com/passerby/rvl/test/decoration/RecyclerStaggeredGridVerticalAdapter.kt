package com.passerby.rvl.test.decoration

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridVerticalOneBinding
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridVerticalThreeBinding
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridVerticalTwoBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/12/5 18:24
 */
class RecyclerStaggeredGridVerticalAdapter(context: Context) :
    RvAdapter<User, RvHolder<User>>(context) {

    val layouts  = arrayListOf(
        R.layout.item_recycler_stagger_grid_vertical_one,
        R.layout.item_recycler_stagger_grid_vertical_two,
        R.layout.item_recycler_stagger_grid_vertical_one
    )

    override fun getItemViewType(position: Int): Int {
        return when (position % 2) {
            1 -> R.layout.item_recycler_stagger_grid_vertical_one
            2 -> R.layout.item_recycler_stagger_grid_vertical_two
            else -> R.layout.item_recycler_stagger_grid_vertical_three
        }
    }

    override fun getLayoutId(viewType: Int): Int {
        return viewType
    }


    override fun createHolder(context: Context, itemView: View,
        viewType: Int): RvHolder<User> {
        return when (viewType) {
            R.layout.item_recycler_stagger_grid_vertical_one -> StaggerGridVerticalOneHolder(
                context, itemView)
            R.layout.item_recycler_stagger_grid_vertical_two -> StaggerGridVerticalTwoHolder(
                context, itemView)
            else -> StaggerGridVerticalThreeHolder(context, itemView)
        }
    }

    class StaggerGridVerticalOneHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridVerticalOneBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }

    class StaggerGridVerticalTwoHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridVerticalTwoBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }

    class StaggerGridVerticalThreeHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridVerticalThreeBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }


}