package com.passerby.rvl.test.decoration

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.passerby.rvl.base.BaseActivity
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ActivityRecyclerCheckableBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/18 0:48
 */
class RecyclerCheckableActivity : BaseActivity() {

    private val binding by viewBinding(ActivityRecyclerCheckableBinding::class.java)

    private val adapter by lazy { RecyclerCheckableAdapter(this) }

    override fun bindLayoutId() = R.layout.activity_recycler_checkable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.btnSingle.setOnClickListener(::onClickSingle)
        binding.btnMulti.setOnClickListener(::onClickMulti)
        binding.btnTenCount.setOnClickListener(::onClickTenCount)
        binding.btnRandomAdd.setOnClickListener(::onClickRandomAdd)
        binding.btnRandomMinus.setOnClickListener(::onClickRandomMinus)
        binding.btnClearCheck.setOnClickListener(::onClickClearCheck)
        binding.btnClearItem.setOnClickListener(::onClickClearItem)
        binding.btnReset.setOnClickListener(::onClickReset)
        binding.recycler.adapter = adapter
        adapter.addOnItemClickListener { holder, item, position ->
            adapter.setChecked(item, !adapter.isCheckedPosition(position))
        }
    }

    fun onClickSingle(view: View) {
        adapter.maxCheckCount = 1
    }

    fun onClickMulti(view: View) {
        adapter.maxCheckCount = Int.MAX_VALUE
    }

    fun onClickTenCount(view: View) {
        adapter.maxCheckCount = 10
    }

    fun onClickRandomAdd(view: View) {
        val index = (Math.random() * adapter.itemCount).toInt()
        Toast.makeText(this, "增加了$index", Toast.LENGTH_SHORT).show()
        //由于Item的文本是根据bind返回的Position决定的，这里设置的不生效，并且页面看起来没有刷新，这不是功能的问题，是写法的问题
        adapter.addItem(index, CheckableUser("李 $index 张 ${index + 1}"))
    }

    fun onClickRandomMinus(view: View) {
        val index = (Math.random() * adapter.itemCount).toInt()
        Toast.makeText(this, "移除了$index", Toast.LENGTH_SHORT).show()
        //由于Item的文本是根据bind返回的Position决定的，这里移除的不生效，并且页面看起来没有刷新，这不是功能的问题，是写法的问题
        adapter.removeItem(index)
    }

    fun onClickClearItem(view: View) {
        adapter.clearItems()
    }

    fun onClickClearCheck(view: View) {
        adapter.clearChecked()
    }

    fun onClickReset(view: View) {
        adapter.clearAndAddItems(CheckableUsers())
    }
}