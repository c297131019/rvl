package com.passerby.rvl.test.nestableAdapter

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/30 16:28
 */


data class Item(val name: String)
data class Item1(val name: String)

fun createItems(title: String) = (0 until 5).map { index ->
    Item("$title - $index")
}

fun createItems1(title: String) = (0 until 5).map { index ->
    Item1("$title - $index")
}

