package com.passerby.rvl.test

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/28 0:09
 */
class RecyclerAdapter(context: Context) :
    RvAdapter<RecyclerItem, RecyclerAdapter.RecyclerHolder>(context) {

    override fun getLayoutId(viewType: Int) = R.layout.item_recycler

    override fun createHolder(context: Context, itemView: View, viewType: Int) =
        RecyclerHolder(context, itemView)

    class RecyclerHolder(context: Context, view: View) :
        RvHolder<RecyclerItem>(context, view) {
        val binding by lazy { ItemRecyclerBinding.bind(itemView) }
        override fun onUpdate(item: RecyclerItem, position: Int) {
            binding.tvName.text = item.itemName
        }
    }
}