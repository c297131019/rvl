package com.passerby.rvl.test.decoration.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import androidx.core.content.ContextCompat
import com.passerby.recyclerviewlib.recycler.adapter.BindingRvHolder
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemDecorationProfileBinding
import com.passerby.rvl.databinding.ItemRecyclerBinding
import com.passerby.rvl.test.RecyclerItem
import com.passerby.rvl.test.decoration.entity.DecorationMenuEntity

/**
 * @Author by chenz
 * @Date 2022/8/31 23:23
 * Description : Decoration的场景预设Adapter
 */
class DecorationMenuAdapter(context: Context)
    : RvAdapter<DecorationMenuEntity, RvHolder<DecorationMenuEntity>>(context) {
    /**
     * 获取布局Id
     */
    override fun getLayoutId(viewType: Int) = R.layout.item_decoration_profile

    /**
     * 构造Holder
     *
     * @param context 上下文
     * @param itemView ItemView
     * @param viewType 多布局类型
     */
    override fun createHolder(context: Context, itemView: View, viewType: Int): RvHolder<DecorationMenuEntity> {
        return RecyclerHolder(context, itemView)
    }

    class RecyclerHolder(context: Context, view: View) :
        RvHolder<DecorationMenuEntity>(context, view) {
        val binding by lazy { ItemDecorationProfileBinding.bind(itemView) }
        override fun onUpdate(item: DecorationMenuEntity, position: Int) {
            binding.tvMenu.text = item.menuName
            if (item.menuName.isEmpty()) {
                binding.tvMenu.setBackgroundColor(Color.TRANSPARENT)
            } else {
                binding.tvMenu.setBackgroundColor(Color.parseColor("#FF047CDC"))
            }
        }
    }

}