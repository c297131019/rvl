package com.passerby.rvl.test.decoration.entity

/**
 * @Author by chenz
 * @Date 2022/8/31 23:30
 * Description :
 */
enum class DecorationMenu {

    Empty,

    //增加一个数据
    DataCountAdd,

    //减少一个数据
    DataCountMinus,

    //垂直列表
    LayoutVertical,

    //水平列表
    LayoutHorizontal,

    //垂直间距增加
    SpacingWidthAdd,

    //垂直间距减少
    SpacingWidthMinus,

    //水平间距增加
    SpacingHeightAdd,

    //水平间距减少
    SpacingHeightMinus,

    //列数增加
    SpanCountAdd,

    //列数减少
    SpanCountMinus,

    //垂直分割线增加
    DividerVerticalWidthAdd,

    //垂直分割线减少
    DividerVerticalWidthMinus,

    //水平分割线增加
    DividerHorizontalHeightAdd,

    //水平分割线减少
    DividerHorizontalHeightMinus,

    //垂直分割线顶部间距增加
    DividerVerticalTopPaddingAdd,

    //垂直分割线顶部间距减少
    DividerVerticalTopPaddingMinus,

    //垂直分割线底部间距增加
    DividerVerticalBottomPaddingAdd,

    //垂直分割线底部间距减少
    DividerVerticalBottomPaddingMinus,

    //水平分割线左边间距增加
    DividerHorizontalLeftPaddingAdd,

    //水平分割线左边间距减少
    DividerHorizontalLeftPaddingMinus,

    //水平分割线右边间距增加
    DividerHorizontalRightPaddingAdd,

    //水平分割线右边间距减少
    DividerHorizontalRightPaddingMinus,

    //是否绘制左边边距分割线
    IsDrawLeftBorderDivide,

    //是否绘制右边边距分割线
    IsDrawRightBorderDivide,

    //是否绘制顶部边距分割线
    IsDrawTopBorderDivide,

    //是否绘制底部边距分割线
    IsDrawBottomBorderDivide,

    //分割线左/上对齐
    DividerGravityStart,

    //分割线居中对齐
    DividerGravityCenter,

    //分割线右/下对齐
    DividerGravityEnd,

    /*LinearItemDecoration*/

    //是否显示垂直方向的左边边缘间距
    IsShowVerticalLeftBorder,

    //是否显示水平方向的上边边缘间距
    IsShowHorizontalTopBorder,

    //是否显示垂直方向的右边边缘间距
    IsShowVerticalRightBorder,

    //是否显示水平方向的下边边缘间距
    IsShowHorizontalBottomBorder,

    /*GridItemDecoration*/

    //是否显示垂直方向的两边边缘间距，左右
    IsShowVerticalBorder,

    //是否显示水平方向的两边边缘间距，上下
    IsShowHorizontalBorder
}