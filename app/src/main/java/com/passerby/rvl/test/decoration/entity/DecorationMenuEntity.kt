package com.passerby.rvl.test.decoration.entity

/**
 * @Author by chenz
 * @Date 2022/8/31 23:25
 * Description :
 */
data class DecorationMenuEntity(val menuName: String, val menu: DecorationMenu)