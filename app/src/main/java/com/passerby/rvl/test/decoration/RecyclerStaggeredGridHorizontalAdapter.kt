package com.passerby.rvl.test.decoration

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridHorizontalOneBinding
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridHorizontalThreeBinding
import com.passerby.rvl.databinding.ItemRecyclerStaggerGridHorizontalTwoBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/12/5 18:24
 */
class RecyclerStaggeredGridHorizontalAdapter(context: Context) :
    RvAdapter<User, RvHolder<User>>(context) {


    override fun getItemViewType(position: Int): Int {
        return when (position % 1) {
            1 -> R.layout.item_recycler_stagger_grid_horizontal_one
            0 -> R.layout.item_recycler_stagger_grid_horizontal_two
            else -> R.layout.item_recycler_stagger_grid_horizontal_three
        }
    }

    override fun getLayoutId(viewType: Int): Int {
        return viewType
    }


    override fun createHolder(context: Context, itemView: View,
        viewType: Int): RvHolder<User> {
        return when (viewType) {
            R.layout.item_recycler_stagger_grid_horizontal_one -> StaggerGridHorizontalOneHolder(
                context, itemView)
            R.layout.item_recycler_stagger_grid_horizontal_two -> StaggerGridHorizontalTwoHolder(
                context, itemView)
            else -> StaggerGridHorizontalThreeHolder(context, itemView)
        }
    }

    class StaggerGridHorizontalOneHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridHorizontalOneBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }

    class StaggerGridHorizontalTwoHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridHorizontalTwoBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }

    class StaggerGridHorizontalThreeHolder(context: Context, view: View) :
        RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerStaggerGridHorizontalThreeBinding.bind(view) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }


}