package com.passerby.rvl.test.decoration.entity

import android.net.wifi.WifiManager

/**
 * @Author by chenz
 * @Date 2022/9/1 0:58
 * Description :
 */
fun sharedProviderMenu(): ArrayList<DecorationMenuEntity> {
    return arrayListOf(
        DecorationMenuEntity("增加Item", DecorationMenu.DataCountAdd),
        DecorationMenuEntity("减少Item", DecorationMenu.DataCountMinus),
        DecorationMenuEntity("垂直列表", DecorationMenu.LayoutVertical),
        DecorationMenuEntity("水平列表", DecorationMenu.LayoutHorizontal),
        DecorationMenuEntity("垂直间距+", DecorationMenu.SpacingWidthAdd),
        DecorationMenuEntity("垂直间距-", DecorationMenu.SpacingWidthMinus),
        DecorationMenuEntity("水平间距+", DecorationMenu.SpacingHeightAdd),
        DecorationMenuEntity("水平间距-", DecorationMenu.SpacingHeightMinus),
        DecorationMenuEntity("垂直分割线+", DecorationMenu.DividerVerticalWidthAdd),
        DecorationMenuEntity("垂直分割线-", DecorationMenu.DividerVerticalWidthMinus),
        DecorationMenuEntity("水平分割线+", DecorationMenu.DividerHorizontalHeightAdd),
        DecorationMenuEntity("水平分割线-", DecorationMenu.DividerHorizontalHeightMinus),
        DecorationMenuEntity("左边Padding+", DecorationMenu.DividerHorizontalLeftPaddingAdd),
        DecorationMenuEntity("左边Padding-", DecorationMenu.DividerHorizontalLeftPaddingMinus),
        DecorationMenuEntity("顶部Padding+", DecorationMenu.DividerVerticalTopPaddingAdd),
        DecorationMenuEntity("顶部padding-", DecorationMenu.DividerVerticalTopPaddingMinus),
        DecorationMenuEntity("右边padding+", DecorationMenu.DividerHorizontalRightPaddingAdd),
        DecorationMenuEntity("右边padding-", DecorationMenu.DividerHorizontalRightPaddingMinus),
        DecorationMenuEntity("底部Padding+", DecorationMenu.DividerVerticalBottomPaddingAdd),
        DecorationMenuEntity("底部Padding-", DecorationMenu.DividerVerticalBottomPaddingMinus),
        DecorationMenuEntity("显示左边分割线", DecorationMenu.IsDrawLeftBorderDivide),
        DecorationMenuEntity("显示顶部分割线", DecorationMenu.IsDrawTopBorderDivide),
        DecorationMenuEntity("显示右边分割线", DecorationMenu.IsDrawRightBorderDivide),
        DecorationMenuEntity("显示底部分割线", DecorationMenu.IsDrawBottomBorderDivide),
        DecorationMenuEntity("分割线左上", DecorationMenu.DividerGravityStart),
        DecorationMenuEntity("分割线居中", DecorationMenu.DividerGravityCenter),
        DecorationMenuEntity("分割线右下", DecorationMenu.DividerGravityEnd),
        DecorationMenuEntity("", DecorationMenu.Empty),
    )
}

fun linearMenus(): ArrayList<DecorationMenuEntity> {
    return sharedProviderMenu().apply {
        add(DecorationMenuEntity("显示左边边距", DecorationMenu.IsShowVerticalLeftBorder))
        add(DecorationMenuEntity("显示顶部边距", DecorationMenu.IsShowHorizontalTopBorder))
        add(DecorationMenuEntity("显示右边边距", DecorationMenu.IsShowVerticalRightBorder))
        add(DecorationMenuEntity("显示底部边距", DecorationMenu.IsShowHorizontalBottomBorder))
    }
}

fun gridMenus(): ArrayList<DecorationMenuEntity> {
    return sharedProviderMenu().apply {
        add(DecorationMenuEntity("显示垂直左右边距", DecorationMenu.IsShowVerticalBorder))
        add(DecorationMenuEntity("显示水平上下边距", DecorationMenu.IsShowHorizontalBorder))
        add(DecorationMenuEntity("列数+", DecorationMenu.SpanCountAdd))
        add(DecorationMenuEntity("列数-", DecorationMenu.SpanCountMinus))
    }
}