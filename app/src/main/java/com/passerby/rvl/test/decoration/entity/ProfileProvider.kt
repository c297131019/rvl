package com.passerby.rvl.test.decoration.entity

import android.content.Context
import android.graphics.Color
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.passerby.recyclerviewlib.recycler.decoration.DividerColor
import com.passerby.recyclerviewlib.recycler.decoration.LinearItemDecoration
import com.passerby.rvl.dp

/**
 * @Author by chenz
 * @Date 2022/9/4 13:38
 * Description :
 */
fun linearProfiles(context: Context): ArrayList<DecorationProfileEntity> {
    return arrayListOf(
        object : DecorationProfileEntity("方案一", "显示左右垂直边距，垂直边距为10，显示左右垂直分割线，分割线为0，这时水平分割线会超出Item左右") {
            override fun profile() = Profile(
                20,
                LinearLayoutManager(context, RecyclerView.VERTICAL, false),
                LinearItemDecoration(10.dp, 2.dp).apply {
                    isShowVerticalLeftBorder = true
                    isShowVerticalRightBorder = true
                },
                DividerColor(Color.RED).apply {
                    verticalWidth = 0
                    horizontalHeight = 4.dp
                    isDrawLeftBorderDivide = true
                    isDrawRightBorderDivide = true
                },
            )
        }
    )
}

fun gridProfiles(context: Context): ArrayList<DecorationProfileEntity> {
    return arrayListOf()
}