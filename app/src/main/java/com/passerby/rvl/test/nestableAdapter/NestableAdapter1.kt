package com.passerby.rvl.test.nestableAdapter

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.annotation.ColorRes
import com.passerby.recyclerviewlib.recycler.adapter.NestableRvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/30 16:28
 */
class NestableAdapter1(context: Context, @ColorRes val bgColor: Int) :
    NestableRvAdapter<Item1, NestableAdapter1.NestableHolder1>(context) {

    override fun getLocalItemViewType(position: Int) = 5

    override fun getLayoutId(viewType: Int) = R.layout.item_nested

    override fun createHolder(context: Context, itemView: View, viewType: Int): NestableHolder1 {
        return NestableHolder1(context, itemView)
    }

    inner class NestableHolder1(context: Context, itemView: View) :
        RvHolder<Item1>(context, itemView) {
        override fun onUpdate(item: Item1, position: Int) {
            itemView.setBackgroundResource(bgColor)
            itemView.findViewById<TextView>(R.id.tvName).text = item.name
        }
    }
}