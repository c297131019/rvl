package com.passerby.rvl.test.decoration

import android.os.Bundle
import android.util.Log
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.passerby.rvl.dp
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.decoration.*
import com.passerby.rvl.base.BaseActivity
import com.passerby.rvl.test.decoration.adapter.DecorationMenuAdapter
import com.passerby.rvl.test.decoration.adapter.DecorationProfileAdapter
import com.passerby.rvl.test.decoration.entity.DecorationMenu
import com.passerby.rvl.test.decoration.entity.DecorationMenuEntity
import com.passerby.rvl.test.decoration.entity.DecorationProfileEntity

/**
 * Date: 2021/12/23
 * author: ChenZhiJian
 */
abstract class BaseRecyclerItemDecorationActivity : BaseActivity() {


    abstract fun recyclerView(): RecyclerView
    abstract fun menuRecyclerView(): RecyclerView?
    abstract fun profileRecyclerView(): RecyclerView?
    abstract fun menus(): List<DecorationMenuEntity>
    abstract fun profiles(): List<DecorationProfileEntity>

    protected abstract val verticalAdapter: RvAdapter<User, *>
    protected abstract val horizontalAdapter: RvAdapter<User, *>
    protected abstract var itemDecoration: AbsItemDecoration
    protected var dataCount = 20
    protected var orientation = RecyclerView.VERTICAL
    protected abstract var verticalLayoutManager: RecyclerView.LayoutManager
    protected abstract var horizontalLayoutManager: RecyclerView.LayoutManager
    protected var dividerColor = DividerColor(0xFFFF0000.toInt())
    protected var spacingWidth = 0
    protected var spacingHeight = 0
    protected var spanCount = 0
    protected var dividerWidth = 0
    protected var dividerHeight = 0
    protected var dividerLeftPadding = 0
    protected var dividerTopPadding = 0
    protected var dividerRightPadding = 0
    protected var dividerBottomPadding = 0
    protected var dividerGravity = Gravity.Center
    protected var isDrawLeftDivider = false
    protected var isDrawTopDivider = false
    protected var isDrawRightDivider = false
    protected var isDrawBottomDivider = false
    protected var isShowLeftBorder = false
    protected var isShowTopBorder = false
    protected var isShowRightBorder = false
    protected var isShowBottomBorder = false
    protected var isShowVerticalBorder = false
    protected var isShowHorizontalBorder = false

    protected var adapter: RvAdapter<User, *>? = null
    private val profileAdapter by lazy {
        DecorationProfileAdapter(this).apply {
            addItems(profiles())
        }
    }
    private val menuAdapter by lazy {
        DecorationMenuAdapter(this).apply {
            addItems(menus())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initProfile()
        initMenu()
        initRv()
    }

    private fun initRv() {
        recyclerView().layoutManager = verticalLayoutManager
        recyclerView().adapter = verticalAdapter
        recyclerView().addItemDecoration(itemDecoration)
        itemDecoration.setDividerColor(dividerColor)
        adapter = verticalAdapter
        verticalAdapter.addItems(Users(dataCount))
        horizontalAdapter.addItems(Users(dataCount))
    }

    private fun initProfile() {
        profileRecyclerView() ?: return
        profileRecyclerView()?.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        profileRecyclerView()?.addItemDecoration(LinearItemDecoration(0, 10.dp))
        profileRecyclerView()?.adapter = profileAdapter
        profileAdapter.addOnItemClickListener { holder, item, position ->
            val profile = item.profile()
            updateLayoutManager(profile.layoutManager)
            updateItemDecoration(profile.itemDecoration)
            updateDivider(profile.divider)
            updateDataCount(profile.dataCount)
            notifyDataSetChange()
        }
    }


    private fun initMenu() {
        menuRecyclerView() ?: return
        menuRecyclerView()?.layoutManager =
            GridLayoutManager(this, 4, GridLayoutManager.VERTICAL, false)
        menuRecyclerView()?.addItemDecoration(GridItemDecoration(10.dp, 10.dp))
        menuRecyclerView()?.adapter = menuAdapter
        menuAdapter.addOnItemClickListener { holder, item, position ->
            when (item.menu) {
                DecorationMenu.DataCountAdd -> {
                    addDataCount()
                }
                DecorationMenu.DataCountMinus -> {
                    minusDataCount()
                }
                DecorationMenu.LayoutVertical -> {
                    layoutVertical()
                }
                DecorationMenu.LayoutHorizontal -> {
                    layoutHorizontal()
                }
                DecorationMenu.SpacingWidthAdd -> {
                    spacingWidthAdd()
                }
                DecorationMenu.SpacingWidthMinus -> {
                    spacingWidthMinus()
                }
                DecorationMenu.SpacingHeightAdd -> {
                    spacingHeightAdd()
                }
                DecorationMenu.SpacingHeightMinus -> {
                    spacingHeightMinus()
                }
                DecorationMenu.SpanCountAdd -> {
                    spanCountAdd()
                }
                DecorationMenu.SpanCountMinus -> {
                    spanCountMinus()
                }
                DecorationMenu.DividerVerticalWidthAdd -> {
                    dividerWidthAdd()
                }
                DecorationMenu.DividerVerticalWidthMinus -> {
                    dividerWidthMinus()
                }
                DecorationMenu.DividerHorizontalHeightAdd -> {
                    dividerHeightAdd()
                }
                DecorationMenu.DividerHorizontalHeightMinus -> {
                    dividerHeightMinus()
                }
                DecorationMenu.DividerVerticalTopPaddingAdd -> {
                    dividerTopPaddingAdd()
                }
                DecorationMenu.DividerVerticalTopPaddingMinus -> {
                    dividerTopPaddingMinus()
                }
                DecorationMenu.DividerVerticalBottomPaddingAdd -> {
                    dividerBottomPaddingAdd()
                }
                DecorationMenu.DividerVerticalBottomPaddingMinus -> {
                    dividerBottomPaddingMinus()
                }
                DecorationMenu.DividerHorizontalLeftPaddingAdd -> {
                    dividerLeftPaddingAdd()
                }
                DecorationMenu.DividerHorizontalLeftPaddingMinus -> {
                    dividerLeftPaddingMinus()
                }
                DecorationMenu.DividerHorizontalRightPaddingAdd -> {
                    dividerRightPaddingAdd()
                }
                DecorationMenu.DividerHorizontalRightPaddingMinus -> {
                    dividerRightPaddingMinus()
                }
                DecorationMenu.IsDrawLeftBorderDivide -> {
                    isDrawLeftDivider()
                }
                DecorationMenu.IsDrawRightBorderDivide -> {
                    isDrawRightDivider()
                }
                DecorationMenu.IsDrawTopBorderDivide -> {
                    isDrawTopDivider()
                }
                DecorationMenu.IsDrawBottomBorderDivide -> {
                    isDrawBottomDivider()
                }
                DecorationMenu.DividerGravityStart -> {
                    dividerGravity(Gravity.Start)
                }
                DecorationMenu.DividerGravityCenter -> {
                    dividerGravity(Gravity.Center)
                }
                DecorationMenu.DividerGravityEnd -> {
                    dividerGravity(Gravity.End)
                }
                DecorationMenu.IsShowVerticalLeftBorder -> {
                    isShowLeftBorder()
                }
                DecorationMenu.IsShowHorizontalTopBorder -> {
                    isShowTopBorder()
                }
                DecorationMenu.IsShowVerticalRightBorder -> {
                    isShowRightBorder()
                }
                DecorationMenu.IsShowHorizontalBottomBorder -> {
                    isShowBottomBorder()
                }
                DecorationMenu.IsShowVerticalBorder -> {
                    isShowVerticalBorder()
                }
                DecorationMenu.IsShowHorizontalBorder -> {
                    isShowHorizontalBorder()
                }
            }
            notifyDataSetChange()
        }
    }

    private fun updateDataCount(count: Int) {
        val users = if (count <= 0) {
            Users(0)
        } else {
            Users(count)
        }
        if (orientation == RecyclerView.VERTICAL) {
            verticalAdapter.clearAndAddItems(users)
        } else {
            horizontalAdapter.clearAndAddItems(users)
        }
        dataCount = count
    }

    private fun updateItemDecoration(newItemDecoration: AbsItemDecoration) {
        if (recyclerView().itemDecorationCount != 0) {
            recyclerView().removeItemDecoration(itemDecoration)
        }
        recyclerView().addItemDecoration(newItemDecoration)
        itemDecoration = newItemDecoration
        spacingWidth = newItemDecoration.spacingWidth
        spacingHeight = newItemDecoration.spacingHeight
        when (newItemDecoration) {
            is LinearItemDecoration -> {
                isShowLeftBorder = newItemDecoration.isShowVerticalLeftBorder
                isShowTopBorder = newItemDecoration.isShowHorizontalTopBorder
                isShowRightBorder = newItemDecoration.isShowVerticalRightBorder
                isShowBottomBorder = newItemDecoration.isShowHorizontalBottomBorder
            }
            is GridItemDecoration -> {
                isShowVerticalBorder = newItemDecoration.isShowVerticalBorder
                isShowHorizontalBorder = newItemDecoration.isShowHorizontalBorder
            }
        }
    }

    private fun updateLayoutManager(newLayoutManager: RecyclerView.LayoutManager) {
        recyclerView().layoutManager = newLayoutManager
        if (newLayoutManager is LinearLayoutManager) {
            if (newLayoutManager.orientation == RecyclerView.VERTICAL) {
                verticalLayoutManager = newLayoutManager
            } else {
                horizontalLayoutManager = newLayoutManager
            }
            orientation = newLayoutManager.orientation
        } else if (newLayoutManager is StaggeredGridLayoutManager) {
            if (newLayoutManager.orientation == RecyclerView.VERTICAL) {
                verticalLayoutManager = newLayoutManager
            } else {
                horizontalLayoutManager = newLayoutManager
            }
            orientation = newLayoutManager.orientation
        }
    }

    private fun updateDivider(newDividerColor: DividerColor) {
        itemDecoration.setDividerColor(newDividerColor)
        dividerColor = newDividerColor
        dividerWidth = newDividerColor.verticalWidth
        dividerHeight = newDividerColor.horizontalHeight
        dividerLeftPadding = newDividerColor.horizontalLeftPadding
        dividerTopPadding = newDividerColor.verticalTopPadding
        dividerRightPadding = newDividerColor.horizontalRightPadding
        dividerBottomPadding = newDividerColor.verticalBottomPadding
        dividerGravity = newDividerColor.gravity
        isDrawLeftDivider = newDividerColor.isDrawLeftBorderDivide
        isDrawTopDivider = newDividerColor.isDrawTopBorderDivide
        isDrawRightDivider = newDividerColor.isDrawRightBorderDivide
        isDrawBottomDivider = newDividerColor.isDrawBottomBorderDivide
    }

    private fun addDataCount() {
        dataCount++
        if (orientation == RecyclerView.VERTICAL) {
            verticalAdapter.addItem(User(verticalAdapter.itemCount - 1))
        } else {
            horizontalAdapter.addItem(User(verticalAdapter.itemCount - 1))
        }
    }

    private fun minusDataCount() {
        dataCount--
        if (dataCount <= 0) {
            dataCount = 0
        }
        if (orientation == RecyclerView.VERTICAL) {
            verticalAdapter.removeItem(verticalAdapter.itemCount - 1)
        } else {
            horizontalAdapter.removeItem(verticalAdapter.itemCount - 1)
        }
    }

    private fun layoutVertical() {
        orientation = RecyclerView.VERTICAL
        recyclerView().layoutManager = verticalLayoutManager
        recyclerView().adapter = verticalAdapter
        updateDataCount(dataCount)
        recyclerView().updateLayoutParams<ViewGroup.MarginLayoutParams> {
            width = ViewGroup.MarginLayoutParams.MATCH_PARENT
            height = ViewGroup.MarginLayoutParams.MATCH_PARENT
        }
    }

    private fun layoutHorizontal() {
        orientation = RecyclerView.HORIZONTAL
        recyclerView().layoutManager = horizontalLayoutManager
        recyclerView().adapter = horizontalAdapter
        updateDataCount(dataCount)
        recyclerView().updateLayoutParams<ViewGroup.MarginLayoutParams> {
            width = ViewGroup.MarginLayoutParams.MATCH_PARENT
            height = ViewGroup.MarginLayoutParams.WRAP_CONTENT
        }
    }

    private fun spacingWidthAdd() {
        spacingWidth++
        itemDecoration.setSpacingWidth(spacingWidth)
    }

    private fun spacingWidthMinus() {
        spacingWidth--
        if (spacingWidth <= 0) {
            spacingWidth = 0
        }
        itemDecoration.setSpacingWidth(spacingWidth)
    }

    private fun spacingHeightAdd() {
        spacingHeight++
        itemDecoration.setSpacingHeight(spacingHeight)
    }

    private fun spacingHeightMinus() {
        spacingHeight--
        if (spacingHeight <= 0) {
            spacingHeight = 0
        }
        itemDecoration.setSpacingHeight(spacingHeight)
    }

    private fun spanCountAdd() {
        spanCount++
        if (orientation == RecyclerView.VERTICAL) {
            (verticalLayoutManager as? GridLayoutManager)?.spanCount = spanCount
            (verticalLayoutManager as? StaggeredGridLayoutManager)?.spanCount = spanCount
        } else {
            (horizontalLayoutManager as? GridLayoutManager)?.spanCount = spanCount
            (horizontalLayoutManager as? StaggeredGridLayoutManager)?.spanCount = spanCount
        }
    }

    private fun spanCountMinus() {
        spanCount--
        if (spanCount <= 1) {
            spanCount = 1
        }
        if (orientation == RecyclerView.VERTICAL) {
            (verticalLayoutManager as? GridLayoutManager)?.spanCount = spanCount
            (verticalLayoutManager as? StaggeredGridLayoutManager)?.spanCount = spanCount
        } else {
            (horizontalLayoutManager as? GridLayoutManager)?.spanCount = spanCount
            (horizontalLayoutManager as? StaggeredGridLayoutManager)?.spanCount = spanCount
        }
    }

    private fun dividerWidthAdd() {
        dividerWidth++
        dividerColor.verticalWidth = dividerWidth
    }

    private fun dividerWidthMinus() {
        dividerWidth--
        if (dividerWidth <= 0) {
            dividerWidth = 0
        }
        dividerColor.verticalWidth = dividerWidth
    }

    private fun dividerHeightAdd() {
        dividerHeight++
        dividerColor.horizontalHeight = dividerHeight
    }

    private fun dividerHeightMinus() {
        dividerHeight--
        if (dividerHeight <= 0) {
            dividerHeight = 0
        }
        dividerColor.horizontalHeight = dividerHeight
    }

    private fun dividerLeftPaddingAdd() {
        dividerLeftPadding++
        dividerColor.horizontalLeftPadding = dividerLeftPadding
    }

    private fun dividerLeftPaddingMinus() {
        dividerLeftPadding--
        if (dividerLeftPadding <= 0) {
            dividerLeftPadding = 0
        }
        dividerColor.horizontalLeftPadding = dividerLeftPadding
    }

    private fun dividerTopPaddingAdd() {
        dividerTopPadding++
        dividerColor.verticalTopPadding = dividerTopPadding
    }

    private fun dividerTopPaddingMinus() {
        dividerTopPadding--
        if (dividerTopPadding <= 0) {
            dividerTopPadding = 0
        }
        dividerColor.verticalTopPadding = dividerTopPadding
    }

    private fun dividerRightPaddingAdd() {
        dividerRightPadding++
        dividerColor.horizontalRightPadding = dividerRightPadding
    }

    private fun dividerRightPaddingMinus() {
        dividerRightPadding--
        if (dividerRightPadding <= 0) {
            dividerRightPadding = 0
        }
        dividerColor.horizontalRightPadding = dividerRightPadding
    }

    private fun dividerBottomPaddingAdd() {
        dividerBottomPadding++
        dividerColor.verticalBottomPadding = dividerBottomPadding
    }

    private fun dividerBottomPaddingMinus() {
        dividerBottomPadding--
        if (dividerBottomPadding <= 0) {
            dividerBottomPadding = 0
        }
        dividerColor.verticalBottomPadding = dividerBottomPadding
    }

    private fun dividerGravity(gravity: Gravity) {
        dividerGravity = gravity
        dividerColor.gravity = dividerGravity
    }

    private fun isDrawLeftDivider() {
        isDrawLeftDivider = !isDrawLeftDivider
        dividerColor.isDrawLeftBorderDivide = isDrawLeftDivider
    }

    private fun isDrawTopDivider() {
        isDrawTopDivider = !isDrawTopDivider
        dividerColor.isDrawTopBorderDivide = isDrawTopDivider
    }

    private fun isDrawRightDivider() {
        isDrawRightDivider = !isDrawRightDivider
        dividerColor.isDrawRightBorderDivide = isDrawRightDivider
    }

    private fun isDrawBottomDivider() {
        isDrawBottomDivider = !isDrawBottomDivider
        dividerColor.isDrawBottomBorderDivide = isDrawBottomDivider
    }

    private fun isShowLeftBorder() {
        isShowLeftBorder = !isShowLeftBorder
        (itemDecoration as LinearItemDecoration)?.setShowVerticalLeftBorder(isShowLeftBorder)
    }

    private fun isShowTopBorder() {
        isShowTopBorder = !isShowTopBorder
        (itemDecoration as LinearItemDecoration)?.setShowHorizontalTopBorder(isShowTopBorder)
    }

    private fun isShowRightBorder() {
        isShowRightBorder = !isShowRightBorder
        (itemDecoration as LinearItemDecoration)?.setShowVerticalRightBorder(isShowRightBorder)
    }

    private fun isShowBottomBorder() {
        isShowBottomBorder = !isShowBottomBorder
        (itemDecoration as LinearItemDecoration)?.setShowHorizontalBottomBorder(isShowBottomBorder)
    }

    private fun isShowVerticalBorder() {
        isShowVerticalBorder = !isShowVerticalBorder
        (itemDecoration as GridItemDecoration)?.setShowVerticalBorder(isShowVerticalBorder)
    }

    private fun isShowHorizontalBorder() {
        isShowHorizontalBorder = !isShowHorizontalBorder
        (itemDecoration as GridItemDecoration)?.setShowHorizontalBorder(isShowHorizontalBorder)
    }

    private fun notifyDataSetChange() {
        recyclerView().adapter?.notifyDataSetChanged()
        recyclerView().invalidateItemDecorations()
        printProfile()
    }

    private fun printProfile() {
        val layoutManagerStr = if (orientation == RecyclerView.VERTICAL) {
            when (verticalLayoutManager) {
                is LinearLayoutManager -> """LinearLayoutManager(context, RecyclerView.VERTICAL, ${(verticalLayoutManager as LinearLayoutManager).reverseLayout})"""
                is GridLayoutManager -> """GridLayoutManager(context, ${(verticalLayoutManager as GridLayoutManager).spanCount}, RecyclerView.VERTICAL, ${(verticalLayoutManager as GridLayoutManager).reverseLayout})"""
                is StaggeredGridLayoutManager -> """StaggeredGridLayoutManager(${(verticalLayoutManager as StaggeredGridLayoutManager).spanCount}, RecyclerView.VERTICAL).apply{}.apply{
                    setReverseLayout(${(verticalLayoutManager as StaggeredGridLayoutManager).reverseLayout})
                    }"""
                else -> ""
            }
        } else {
            when (horizontalLayoutManager) {
                is LinearLayoutManager -> """LinearLayoutManager(context, RecyclerView.VERTICAL, ${(horizontalLayoutManager as LinearLayoutManager).reverseLayout})"""
                is GridLayoutManager -> """GridLayoutManager(context, ${(horizontalLayoutManager as GridLayoutManager).spanCount}, RecyclerView.VERTICAL, ${(horizontalLayoutManager as GridLayoutManager).reverseLayout})"""
                is StaggeredGridLayoutManager -> """StaggeredGridLayoutManager(${(horizontalLayoutManager as StaggeredGridLayoutManager).spanCount}, RecyclerView.VERTICAL).apply{}.apply{
                    setReverseLayout(${(horizontalLayoutManager as StaggeredGridLayoutManager).reverseLayout})
                    }"""
                else -> ""
            }
        }
        val itemDecorationString = when (itemDecoration) {
            is LinearItemDecoration -> """
                LinearItemDecoration(${itemDecoration.spacingWidth}, ${itemDecoration.spacingHeight}).apply{
                    isShowVerticalLeftBorder = ${(itemDecoration as LinearItemDecoration).isShowVerticalLeftBorder}
                    isShowHorizontalTopBorder = ${(itemDecoration as LinearItemDecoration).isShowHorizontalTopBorder}
                    isShowVerticalRightBorder = ${(itemDecoration as LinearItemDecoration).isShowVerticalRightBorder}
                    isShowHorizontalBottomBorder = ${(itemDecoration as LinearItemDecoration).isShowHorizontalBottomBorder}        
                }
            """
            is GridItemDecoration -> """
                GridItemDecoration(${itemDecoration.spacingWidth}, ${itemDecoration.spacingHeight}).apply {
                    isShowVerticalBorder = ${(itemDecoration as GridItemDecoration).isShowVerticalBorder}
                    isShowHorizontalBorder = ${(itemDecoration as GridItemDecoration).isShowHorizontalBorder}
                }
            """
            else -> ""
        }
        val dividerColorStr = """
                DividerColor(${dividerColor.color}).apply {
                    gravity = Gravity.${dividerColor.gravity}
                    verticalWidth = ${dividerColor.verticalWidth}
                    horizontalHeight = ${dividerColor.horizontalHeight}
                    horizontalLeftPadding = ${dividerColor.horizontalLeftPadding}
                    verticalTopPadding = ${dividerColor.verticalTopPadding}
                    horizontalRightPadding = ${dividerColor.horizontalRightPadding}
                    verticalBottomPadding = ${dividerColor.verticalBottomPadding}
                    isDrawLeftBorderDivide = ${dividerColor.isDrawLeftBorderDivide}
                    isDrawTopBorderDivide = ${dividerColor.isDrawTopBorderDivide}
                    isDrawRightBorderDivide = ${dividerColor.isDrawRightBorderDivide}
                    isDrawBottomBorderDivide = ${dividerColor.isDrawBottomBorderDivide}
                }
        """
        Log.d("ItemDecoration", """\n
            Profile(
                ${dataCount},
                ${layoutManagerStr},
                ${itemDecorationString},
                $dividerColorStr
            )
        """)
    }
}

