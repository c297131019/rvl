package com.passerby.rvl.test.decoration

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerLinearHorizontalBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/27 23:55
 */
class RecyclerLinearHorizontalAdapter(context: Context) :
    RvAdapter<User, RecyclerLinearHorizontalAdapter.LinearHolder>(context) {

    override fun getLayoutId(viewType: Int) = R.layout.item_recycler_linear_horizontal

    override fun createHolder(context: Context, itemView: View, viewType: Int): LinearHolder {
        return LinearHolder(context, itemView)
    }

    class LinearHolder(context: Context, view: View) : RvHolder<User>(context, view) {
        val binding by lazy { ItemRecyclerLinearHorizontalBinding.bind(itemView) }
        override fun onUpdate(item: User, position: Int) {
            binding.tvName.text = item.name
        }
    }
}