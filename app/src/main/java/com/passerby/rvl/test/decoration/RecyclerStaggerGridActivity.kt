package com.passerby.rvl.test.decoration

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.decoration.AbsItemDecoration
import com.passerby.recyclerviewlib.recycler.decoration.StaggeredGridItemDecoration
import com.passerby.rvl.R
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.databinding.ActivityRecyclerStaggerGridBinding
import com.passerby.rvl.test.decoration.entity.DecorationMenuEntity
import com.passerby.rvl.test.decoration.entity.DecorationProfileEntity
import com.passerby.rvl.test.decoration.entity.gridMenus
import com.passerby.rvl.test.decoration.entity.gridProfiles

/**
 * Description :
 * @Author by chenz
 * @Date 2021/12/5 18:23
 */
class RecyclerStaggerGridActivity : BaseRecyclerItemDecorationActivity() {

    override fun bindLayoutId() = R.layout.activity_recycler_stagger_grid

    private val binding by viewBinding(ActivityRecyclerStaggerGridBinding::class.java)
    private var span = 2
    override val verticalAdapter: RvAdapter<User, *> by lazy {
        RecyclerStaggeredGridVerticalAdapter(this)
    }
    override val horizontalAdapter: RvAdapter<User, *> by lazy {
        RecyclerStaggeredGridHorizontalAdapter(this)
    }
    override var itemDecoration: AbsItemDecoration =
        StaggeredGridItemDecoration(0, 0)
    override var verticalLayoutManager: RecyclerView.LayoutManager =
        StaggeredGridLayoutManager(span, RecyclerView.VERTICAL)
    override var horizontalLayoutManager: RecyclerView.LayoutManager =
        StaggeredGridLayoutManager(span, RecyclerView.HORIZONTAL)


    override fun recyclerView() = binding.rv

    override fun menuRecyclerView() = binding.iclMenu.rvMenu

    override fun profileRecyclerView() = binding.iclMenu.rvProfile

    override fun menus(): List<DecorationMenuEntity> = gridMenus()

    override fun profiles(): List<DecorationProfileEntity> = gridProfiles(this)
}