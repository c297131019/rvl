package com.passerby.rvl.test.decoration

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/27 23:52
 */
data class User(val name: String)

data class CheckableUser(val name: String, var isChecked: Boolean = false)

fun Users(count: Int = 40): List<User> {
    return (0 until count).mapIndexed { index, i ->
        User(index)
    }
}

fun User(index: Int): User {
    return User("张 $index 李 ${index + 1}")
}

fun CheckableUsers(): List<CheckableUser> {
    return (0 until 50).mapIndexed { index, i ->
        CheckableUser("张 $index 李 ${index + 1}")
    }
}