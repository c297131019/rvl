package com.passerby.rvl.test

import androidx.appcompat.app.AppCompatActivity
import com.passerby.rvl.test.decoration.RecyclerGridActivity
import com.passerby.rvl.test.decoration.RecyclerLinearActivity
import com.passerby.rvl.test.decoration.RecyclerStaggerGridActivity

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/28 0:10
 */
data class RecyclerItem(val itemName: String, val activityClass: Class<out AppCompatActivity>)

fun RecyclerItems(): List<RecyclerItem> {
    return arrayListOf(
        RecyclerItem("线性分割线", RecyclerLinearActivity::class.java),
        RecyclerItem("网格分割线", RecyclerGridActivity::class.java),
        RecyclerItem("瀑布流分割线", RecyclerStaggerGridActivity::class.java),
    )
}