package com.passerby.rvl.test.decoration

import android.content.Context
import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.CheckableRvAdapter
import com.passerby.recyclerviewlib.recycler.adapter.CheckableRvHolder
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemRecyclerCheckableBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/18 0:53
 */
class RecyclerCheckableAdapter(context: Context) :
    CheckableRvAdapter<CheckableUser, RecyclerCheckableAdapter.RecyclerCheckableHolder>(context) {

    override fun getLayoutId(viewType: Int) = R.layout.item_recycler_checkable

    override fun createHolder(context: Context, itemView: View, viewType: Int) =
        RecyclerCheckableHolder(context, itemView)

    class RecyclerCheckableHolder(context: Context, view: View) :
        CheckableRvHolder<CheckableUser>(context, view) {
        private val binding by lazy { ItemRecyclerCheckableBinding.bind(view) }
        override fun onUpdate(item: CheckableUser, position: Int, isChecked: Boolean) {
            binding.cbButton.isChecked = isChecked
            binding.tvName.text = "张 $position 李 ${position + 1}"
        }
    }
}