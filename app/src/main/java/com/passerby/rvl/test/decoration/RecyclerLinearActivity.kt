package com.passerby.rvl.test.decoration

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.decoration.AbsItemDecoration
import com.passerby.recyclerviewlib.recycler.decoration.LinearItemDecoration
import com.passerby.rvl.R
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.databinding.ActivityRecyclerLinearBinding
import com.passerby.rvl.test.decoration.entity.DecorationMenuEntity
import com.passerby.rvl.test.decoration.entity.DecorationProfileEntity
import com.passerby.rvl.test.decoration.entity.linearMenus
import com.passerby.rvl.test.decoration.entity.linearProfiles

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/22 23:19
 */
class RecyclerLinearActivity : BaseRecyclerItemDecorationActivity() {

    override fun bindLayoutId() = R.layout.activity_recycler_linear

    private val binding by viewBinding(ActivityRecyclerLinearBinding::class.java)

    override val verticalAdapter: RvAdapter<User, *> by lazy { RecyclerLinearVerticalAdapter(this) }
    override val horizontalAdapter: RvAdapter<User, *> by lazy { RecyclerLinearHorizontalAdapter(this) }
    override var itemDecoration: AbsItemDecoration = LinearItemDecoration()
    override var verticalLayoutManager: RecyclerView.LayoutManager =
        LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    override var horizontalLayoutManager: RecyclerView.LayoutManager =
        LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

    override fun recyclerView() = binding.rv

    override fun menuRecyclerView() = binding.iclMenu.rvMenu

    override fun profileRecyclerView() = binding.iclMenu.rvProfile

    override fun menus(): List<DecorationMenuEntity> = linearMenus()

    override fun profiles(): List<DecorationProfileEntity> = linearProfiles(this)
}