package com.passerby.rvl.test.decoration

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.passerby.recyclerviewlib.recycler.adapter.RvAdapter
import com.passerby.recyclerviewlib.recycler.decoration.AbsItemDecoration
import com.passerby.recyclerviewlib.recycler.decoration.GridItemDecoration
import com.passerby.rvl.R
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.databinding.ActivityRecyclerGridBinding
import com.passerby.rvl.test.decoration.entity.DecorationMenuEntity
import com.passerby.rvl.test.decoration.entity.DecorationProfileEntity
import com.passerby.rvl.test.decoration.entity.gridMenus
import com.passerby.rvl.test.decoration.entity.gridProfiles

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/28 23:48
 */
class RecyclerGridActivity : BaseRecyclerItemDecorationActivity() {
    override fun bindLayoutId() = R.layout.activity_recycler_grid

    private var span = 2
    private val binding by viewBinding(ActivityRecyclerGridBinding::class.java)
    override val verticalAdapter: RvAdapter<User, *> by lazy { RecyclerGridVerticalAdapter(this) }
    override val horizontalAdapter: RvAdapter<User, *> by lazy { RecyclerGridHorizontalAdapter(this) }
    override var itemDecoration: AbsItemDecoration = GridItemDecoration(0, 0)
    override var verticalLayoutManager: RecyclerView.LayoutManager =
        GridLayoutManager(this, span, RecyclerView.VERTICAL, false)
    override var horizontalLayoutManager: RecyclerView.LayoutManager =
        GridLayoutManager(this, span, RecyclerView.HORIZONTAL, false)

    override fun recyclerView() = binding.rv

    override fun menuRecyclerView() = binding.iclMenu.rvMenu

    override fun profileRecyclerView() = binding.iclMenu.rvProfile

    override fun menus(): List<DecorationMenuEntity> = gridMenus()

    override fun profiles(): List<DecorationProfileEntity> = gridProfiles(this)

}