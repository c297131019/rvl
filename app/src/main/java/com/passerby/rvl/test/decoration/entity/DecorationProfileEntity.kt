package com.passerby.rvl.test.decoration.entity

import androidx.recyclerview.widget.RecyclerView
import com.passerby.recyclerviewlib.recycler.decoration.AbsItemDecoration
import com.passerby.recyclerviewlib.recycler.decoration.DividerColor
import com.passerby.recyclerviewlib.recycler.decoration.DividerStyle

/**
 * @Author by chenz
 * @Date 2022/8/31 23:25
 * Description :
 */
abstract class DecorationProfileEntity(val profileName: String, val profileDesc: String = "") {
    abstract fun profile(): Profile
}

data class Profile(
    //数据数量
    val dataCount: Int,
    //列表类型
    val layoutManager: RecyclerView.LayoutManager,
    //间距样式
    val itemDecoration: AbsItemDecoration,
    //分割线样式
    val divider: DividerColor,
)