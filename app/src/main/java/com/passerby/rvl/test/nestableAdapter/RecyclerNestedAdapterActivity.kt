package com.passerby.rvl.test.nestableAdapter

import android.os.Bundle
import android.view.View
import com.passerby.rvl.base.BaseActivity
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ActivityRecyclerNestedAdapterBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/30 16:18
 */
class RecyclerNestedAdapterActivity : BaseActivity() {

    private val binding by viewBinding(ActivityRecyclerNestedAdapterBinding::class.java)

    private lateinit var adapter: NestableAdapter

    private val opAdapter: NestableAdapter by lazy {
        NestableAdapter(this@RecyclerNestedAdapterActivity,
            R.color.bg_color_5_5).apply {
            this@apply.addItems(createItems("5 - 5"))
        }
    }

    override fun bindLayoutId() = R.layout.activity_recycler_nested_adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initRv()
        binding.addAdapter.setOnClickListener(::onClickAddAdapter)
        binding.setAdapter.setOnClickListener(::onClickSetAdapter)
        binding.removeAdapter.setOnClickListener(::onClickRemoveAdapter)
        binding.addItem.setOnClickListener(::onClickAddItem)
    }

    fun onClickAddAdapter(view: View?) {
        adapter.addAdapter(opAdapter)
    }

    fun onClickSetAdapter(view: View?) {
        adapter.setAdapter(1, opAdapter)

    }

    fun onClickRemoveAdapter(view: View?) {
        adapter.removeAdapter(opAdapter, false)
    }

    fun onClickAddItem(view: View?) {
        adapter.addItem(Item("1 - 2 - ${adapter.itemSize}"))
    }

    private fun initRv() {
        binding.rv.adapter =
            NestableAdapter(this@RecyclerNestedAdapterActivity, R.color.bg_color_1_1).apply {
                addItems(createItems("1 - 1"))
                addAdapter(
                    NestableAdapter(this@RecyclerNestedAdapterActivity,
                    R.color.bg_color_1_2).apply {
                    this@apply.addItems(createItems("1 - 2"))
                    addAdapter(
                        NestableAdapter(this@RecyclerNestedAdapterActivity,
                        R.color.bg_color_2_1).apply {
                        this@apply.addItems(createItems("2 - 1"))
                    })
                        .addAdapter(
                            NestableAdapter(this@RecyclerNestedAdapterActivity,
                            R.color.bg_color_2_2).apply {
                            this@apply.addItems(createItems("2 - 2"))
                        }).addAdapter(
                            NestableAdapter(this@RecyclerNestedAdapterActivity,
                            R.color.bg_color_2_3).apply {
                            this@apply.addItems(createItems("2 - 3"))
                        })
                    adapter = this
                }).addAdapter(
                    NestableAdapter(this@RecyclerNestedAdapterActivity,
                    R.color.bg_color_1_3).apply {
                    this@apply.addItems(createItems("1 - 3"))
                    addAdapter(
                        NestableAdapter(this@RecyclerNestedAdapterActivity,
                        R.color.bg_color_3_1).apply {
                        this@apply.addItems(createItems("3 - 1"))
                    })
                        .addAdapter(
                            NestableAdapter(this@RecyclerNestedAdapterActivity,
                            R.color.bg_color_3_2).apply {
                            this@apply.addItems(createItems("3 - 2"))
                        }).addAdapter(
                            NestableAdapter(this@RecyclerNestedAdapterActivity,
                            R.color.bg_color_3_3).apply {
                            this@apply.addItems(createItems("3 - 3"))
                        })
                        .addAdapter(
                            NestableAdapter1(this@RecyclerNestedAdapterActivity,
                            R.color.bg_color_3_4).apply {
                            this@apply.addItems(createItems1("3 - 3"))
                        })
                })
            }
    }
}