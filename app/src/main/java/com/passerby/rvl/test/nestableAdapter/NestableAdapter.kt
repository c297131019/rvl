package com.passerby.rvl.test.nestableAdapter

import android.content.Context
import android.view.View
import androidx.annotation.ColorRes
import com.passerby.recyclerviewlib.recycler.adapter.BindingRvHolder
import com.passerby.recyclerviewlib.recycler.adapter.NestableRvAdapter
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ItemNestedBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2022/1/30 16:28
 */
class NestableAdapter(context: Context, @ColorRes val bgColor: Int) :
        NestableRvAdapter<Item, NestableAdapter.NestableHolder>(context) {

    override fun getLocalItemViewType(position: Int) = R.layout.item_nested

    override fun getLayoutId(viewType: Int) = R.layout.item_nested

    override fun createHolder(context: Context, itemView: View, viewType: Int): NestableHolder {
        return NestableHolder(context, itemView)
    }

    override fun onUpdate(holder: NestableHolder, item: Item, position: Int) {
//        holder.itemView.setBackgroundResource(bgColor)
//        holder.itemView.findViewById<TextView>(R.id.tvName).text = item.name
        holder.binding.root.setBackgroundResource(bgColor)
        holder.binding.tvName.text = item.name
    }

    inner class NestableHolder(context: Context, itemView: View) :
            BindingRvHolder<Item, ItemNestedBinding>(context, itemView) {
        override fun onUpdate(item: Item, position: Int) {
//            itemView.setBackgroundResource(bgColor)
//            itemView.findViewById<TextView>(R.id.tvName).text = item.name
        }
    }
}