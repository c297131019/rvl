package com.passerby.rvl.test

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.passerby.recyclerviewlib.recycler.decoration.DividerColor
import com.passerby.recyclerviewlib.recycler.decoration.LinearItemDecoration
import com.passerby.rvl.base.BaseActivity
import com.passerby.rvl.base.viewBinding
import com.passerby.rvl.dp
import com.passerby.rvl.R
import com.passerby.rvl.databinding.ActivityRecyclerBinding

/**
 * Description :
 * @Author by chenz
 * @Date 2021/11/22 23:20
 */
class RecyclerActivity : BaseActivity() {

    val binding by viewBinding(ActivityRecyclerBinding::class.java)
    val adapter by lazy { RecyclerAdapter(this) }

    override fun bindLayoutId(): Int {
        return R.layout.activity_recycler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.rv.layoutManager = LinearLayoutManager(this)
        binding.rv.addItemDecoration(LinearItemDecoration().apply {
            setSpacingHeight(10.dp)
            setDividerColor(DividerColor(0xFF000000.toInt()))
        })
        binding.rv.adapter = adapter
        adapter.addItems(RecyclerItems())
        adapter.addOnItemClickListener { holder, item, position ->
            startActivity(Intent(this, item.activityClass))
        }
    }
}