package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Decoration基类，封装绘制、测量流程
 * Date: 2022/08/31
 * author: ChenZhiJian
 */
public abstract class BaseItemDecoration extends AbsItemDecoration {

    //是否显示垂直方向的两边边缘间距，左右
    private boolean isShowVerticalBorder;
    //是否显示水平方向的两边边缘间距，上下
    private boolean isShowHorizontalBorder;


    public BaseItemDecoration() {
    }


    public BaseItemDecoration(int spacingWidth, int spacingHeight) {
        super(spacingWidth, spacingHeight);
    }


    public BaseItemDecoration(int spacingWidth, int spacingHeight, boolean isShowVerticalBorder, boolean isShowHorizontalBorder) {
        super(spacingWidth, spacingHeight);
        this.isShowVerticalBorder = isShowVerticalBorder;
        this.isShowHorizontalBorder = isShowHorizontalBorder;
    }


    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDraw(canvas, parent, state);
        onDrawDivider(canvas, parent);
    }


    /**
     * 开始绘制分割线的流程，在{@link #onDraw(Canvas, RecyclerView, RecyclerView.State)}中调用
     * ```
     * public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
     * super.onDraw(canvas, parent, state);
     * onDrawDivider(canvas, parent);
     * }
     * ```
     *
     * 这一步会ViewHolder.itemView的四周可绘制的区局计算出来，然后交给
     * {@link #drawViewLeft(RecyclerView, Canvas, Gravity, Gravity, int, int, int, int)}
     * {@link #drawViewRight(RecyclerView, Canvas, Gravity, Gravity, int, int, int, int)}
     * {@link #drawViewTop(RecyclerView, Canvas, Gravity, Gravity, int, int, int, int)}
     * {@link #drawViewBottom(RecyclerView, Canvas, Gravity, Gravity, int, int, int, int)}
     * 去绘制四边，可以在这些方法中拦截绘制，根据不同的LayoutManager去决定是否要绘制四边
     *
     * @param canvas Canvas
     * @param parent RecyclerView
     */
    protected void onDrawDivider(Canvas canvas, RecyclerView parent) {
        int left, right, top, bottom;
        Gravity verticalGravity, horizontalGravity;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        canvas.save();
        canvas.clipRect((parent.getClipToPadding() ? parent.getPaddingLeft() : 0),
                        (parent.getClipToPadding() ? parent.getPaddingTop() : 0),
                        parent.getWidth() - (parent.getClipToPadding() ? parent.getPaddingRight() : 0),
                        parent.getHeight() - (parent.getClipToPadding() ? parent.getPaddingBottom() : 0));
        int spacingWidth = Math.max(this.spacingWidth, getDivider() != null ? getDivider().getVerticalWidth() : 0);
        int spacingHeight = Math.max(this.spacingHeight, getDivider() != null ? getDivider().getHorizontalHeight() : 0);
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            //获取View的原始四边位置
            getViewBounds(child, mViewBounds);
            //减去Margin后的位置
            minusBoundsWithMargins(child, mViewBounds);
            //减去Translation后的位置
            minusBoundsWithTranslation(child, mViewBounds);
            left = mViewBounds.left + Math.round(child.getTranslationX());
            right = mViewBounds.right + Math.round(child.getTranslationX());
            top = mViewBounds.top + Math.round(child.getTranslationY());
            bottom = mViewBounds.bottom + Math.round(child.getTranslationY());
            RecyclerView.ViewHolder holder = parent.getChildViewHolder(child);
            int position = holder.getAbsoluteAdapterPosition();
            verticalGravity = getVerticalGravity(parent, layoutManager, holder, position);
            horizontalGravity = getHorizontalGravity(parent, layoutManager, holder, position);
            //因为均分的关系，View实际的四边不是对称的，不能通过获取getDecoratedBoundsWithMargins来计算
            //需要通过视觉看到的View的位置然后计算出需要绘制的点，不过这样会出现重叠的问题，无法避免
            drawViewLeft(parent, canvas, verticalGravity, horizontalGravity,
                         left - spacingWidth, top - spacingHeight, left, bottom + spacingHeight);
            drawViewRight(parent, canvas, verticalGravity, horizontalGravity,
                          right, top - spacingHeight, right + spacingWidth, bottom + spacingHeight);
            drawViewTop(parent, canvas, verticalGravity, horizontalGravity,
                        left - spacingWidth, top - spacingHeight, right + spacingWidth, top);
            drawViewBottom(parent, canvas, verticalGravity, horizontalGravity,
                           left - spacingWidth, bottom, right + spacingWidth, bottom + spacingHeight);
        }
        canvas.restore();
    }


    /**
     * 绘制左边分割线，当确认可以绘制分割线的时候调用{@link #drawDivider(RecyclerView, Canvas, Gravity, Gravity, Direction, int, int, int, int)}
     *
     * ```
     * protected void drawViewLeft(RecyclerView parent, Canvas canvas,
     * Gravity verticalGravity, Gravity horizontalGravity,
     * int left, int top, int right, int bottom) {
     * drawDivider(canvas, verticalGravity, horizontalGravity, VERTICAL, left, top, right, bottom);
     * }
     * ```
     *
     * @param parent RecyclerView
     * @param canvas Canvas
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param left 测量好可绘制的区域的left点，这个区域不代表divider实际绘制的区域
     * @param top 测量好可绘制的区域的top点，这个区域不代表divider实际绘制的区域
     * @param right 测量好可绘制的区域的right点，这个区域不代表divider实际绘制的区域
     * @param bottom 测量好可绘制的区域的bottom点，这个区域不代表divider实际绘制的区域
     */
    protected void drawViewLeft(RecyclerView parent, Canvas canvas,
                                Gravity verticalGravity, Gravity horizontalGravity,
                                int left, int top, int right, int bottom) {
    }


    /**
     * 绘制右边分割线，当确认可以绘制分割线的时候调用{@link #drawDivider(RecyclerView, Canvas, Gravity, Gravity, Direction, int, int, int, int)}
     *
     * ```
     * protected void drawViewLeft(RecyclerView parent, Canvas canvas,
     * Gravity verticalGravity, Gravity horizontalGravity,
     * int left, int top, int right, int bottom) {
     * drawDivider(canvas, verticalGravity, horizontalGravity, VERTICAL, left, top, right, bottom);
     * }
     * ```
     *
     * @param parent RecyclerView
     * @param canvas Canvas
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param left 测量好可绘制的区域的left点，这个区域不代表divider实际绘制的区域
     * @param top 测量好可绘制的区域的top点，这个区域不代表divider实际绘制的区域
     * @param right 测量好可绘制的区域的right点，这个区域不代表divider实际绘制的区域
     * @param bottom 测量好可绘制的区域的bottom点，这个区域不代表divider实际绘制的区域
     */
    protected void drawViewRight(RecyclerView parent, Canvas canvas,
                                 Gravity verticalGravity, Gravity horizontalGravity,
                                 int left, int top, int right, int bottom) {
    }


    /**
     * 绘制上边分割线，当确认可以绘制分割线的时候调用{@link #drawDivider(RecyclerView, Canvas, Gravity, Gravity, Direction, int, int, int, int)}
     *
     * ```
     * protected void drawViewLeft(RecyclerView parent, Canvas canvas,
     * Gravity verticalGravity, Gravity horizontalGravity,
     * int left, int top, int right, int bottom) {
     * drawDivider(canvas, verticalGravity, horizontalGravity, HORIZONTAL, left, top, right, bottom);
     * }
     * ```
     *
     * @param parent RecyclerView
     * @param canvas Canvas
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param left 测量好可绘制的区域的left点，这个区域不代表divider实际绘制的区域
     * @param top 测量好可绘制的区域的top点，这个区域不代表divider实际绘制的区域
     * @param right 测量好可绘制的区域的right点，这个区域不代表divider实际绘制的区域
     * @param bottom 测量好可绘制的区域的bottom点，这个区域不代表divider实际绘制的区域
     */
    protected void drawViewTop(RecyclerView parent, Canvas canvas,
                               Gravity verticalGravity, Gravity horizontalGravity,
                               int left, int top, int right, int bottom) {
    }


    /**
     * 绘制下边分割线，当确认可以绘制分割线的时候调用{@link #drawDivider(RecyclerView, Canvas, Gravity, Gravity, Direction, int, int, int, int)}
     *
     * ```
     * protected void drawViewLeft(RecyclerView parent, Canvas canvas,
     * Gravity verticalGravity, Gravity horizontalGravity,
     * int left, int top, int right, int bottom) {
     * drawDivider(canvas, verticalGravity, horizontalGravity, HORIZONTAL, left, top, right, bottom);
     * }
     * ```
     *
     * @param parent RecyclerView
     * @param canvas Canvas
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param left 测量好可绘制的区域的left点，这个区域不代表divider实际绘制的区域
     * @param top 测量好可绘制的区域的top点，这个区域不代表divider实际绘制的区域
     * @param right 测量好可绘制的区域的right点，这个区域不代表divider实际绘制的区域
     * @param bottom 测量好可绘制的区域的bottom点，这个区域不代表divider实际绘制的区域
     */
    protected void drawViewBottom(RecyclerView parent, Canvas canvas,
                                  Gravity verticalGravity, Gravity horizontalGravity,
                                  int left, int top, int right, int bottom) {
    }


    /**
     * 绘制分割线
     *
     * @param canvas 画布
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param orientation 绘制分割线的方向，是垂直的还是水平的，与列表方向无关。怎么传就怎么解析
     * @param left 分割线x轴起始位置
     * @param top 分割线y轴起始位置
     * @param right 分割线x轴结束位置
     * @param bottom 分割线y轴结束位置
     */
    protected void drawDivider(RecyclerView parent, Canvas canvas, Gravity verticalGravity, Gravity horizontalGravity,
                               Direction orientation, int left, int top, int right, int bottom) {
        if (dividerColor != null) {
            drawDividerColor(parent, dividerColor, canvas, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        } else if (dividerDrawable != null) {
            drawDividerDrawable(parent, dividerDrawable, canvas, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        } else {
            customDrawDivider(parent, canvas, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        }
    }


    /**
     * 绘制纯色分割线
     *
     * @param dividerColor 纯色的divider样式
     * @param canvas 画布
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param orientation 绘制分割线的方向，是垂直的还是水平的，与列表方向无关。怎么传就怎么解析
     * @param left 分割线x轴起始位置
     * @param top 分割线y轴起始位置
     * @param right 分割线x轴结束位置
     * @param bottom 分割线y轴结束位置
     */
    protected void drawDividerColor(RecyclerView parent, DividerColor dividerColor, Canvas canvas,
                                    Gravity verticalGravity, Gravity horizontalGravity,
                                    Direction orientation, int left, int top, int right, int bottom) {
        dividerRect.setEmpty();
        measureDividerRect(parent, dividerColor, dividerRect, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        if (dividerRect.width() == 0 || dividerRect.height() == 0) {
            return;
        }
        dividerPaint.setColor(dividerColor.getColor());
        canvas.save();
        canvas.clipRect(dividerRect);
        canvas.drawRect(dividerRect, dividerPaint);
        canvas.restore();
    }


    /**
     * 绘制Drawable分割线
     *
     * @param dividerDrawable Drawable的divider样式
     * @param canvas 画布
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param orientation 绘制分割线的方向，是垂直的还是水平的，与列表方向无关。怎么传就怎么解析
     * @param left 分割线x轴起始位置
     * @param top 分割线y轴起始位置
     * @param right 分割线x轴结束位置
     * @param bottom 分割线y轴结束位置
     */
    protected void drawDividerDrawable(RecyclerView parent, DividerDrawable dividerDrawable, Canvas canvas,
                                       Gravity verticalGravity, Gravity horizontalGravity,
                                       Direction orientation, int left, int top, int right, int bottom) {
        dividerRect.setEmpty();
        measureDividerRect(parent, dividerColor, dividerRect, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        if (dividerRect.width() == 0 || dividerRect.height() == 0) {
            return;
        }
        Drawable drawable = dividerDrawable.getDrawable();
        //这里是直接画Rect那么长，还是画Rect长 / Drawable长 = Drawable个????，需要增加一种绘制模式
        int save = canvas.save();
        canvas.clipRect(dividerRect);
        drawable.setBounds(dividerRect);
        drawable.draw(canvas);
        canvas.restoreToCount(save);
    }


    /**
     * 自定义绘制分割线
     *
     * @param canvas 画布
     * @param verticalGravity ViewHolder.itemView在列表中的垂直方向的对齐方式
     * @param horizontalGravity ViewHolder.itemView在列表中的水平方向的对齐方式
     * @param orientation 绘制分割线的方向，是垂直的还是水平的，与列表方向无关。怎么传就怎么解析
     * @param left 分割线x轴起始位置
     * @param top 分割线y轴起始位置
     * @param right 分割线x轴结束位置
     * @param bottom 分割线y轴结束位置
     */
    protected void customDrawDivider(RecyclerView parent, Canvas canvas, Gravity verticalGravity, Gravity horizontalGravity,
                                     Direction orientation, int left, int top, int right, int bottom) {
        dividerRect.setEmpty();
        if (onCustomDrawDividerCallback == null) {
            return;
        }
        measureDividerRect(parent, dividerColor, dividerRect, verticalGravity, horizontalGravity, orientation, left, top, right, bottom);
        if (dividerRect.width() == 0 || dividerRect.height() == 0) {
            return;
        }
        int save = canvas.save();
        canvas.clipRect(dividerRect);
        onCustomDrawDividerCallback.onDraw(canvas, backupPaint, dividerRect);
        canvas.restoreToCount(save);
    }


    /**
     * 是否显示垂直方向的两边边缘间距，左右
     */
    public void setShowVerticalBorder(boolean isShow) {
        isShowVerticalBorder = isShow;
    }


    /**
     * 是否显示水平方向的两边边缘间距，上下
     */
    public void setShowHorizontalBorder(boolean isShow) {
        isShowHorizontalBorder = isShow;
    }


    /**
     * 是否显示垂直方向的两边边缘间距，左右
     */
    public boolean isShowVerticalBorder() {
        return isShowVerticalBorder;
    }


    /**
     * 是否显示水平方向的两边边缘间距，上下
     */
    public boolean isShowHorizontalBorder() {
        return isShowHorizontalBorder;
    }
}