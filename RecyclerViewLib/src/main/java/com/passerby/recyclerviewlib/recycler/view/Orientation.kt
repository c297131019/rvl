package com.passerby.recyclerviewlib.recycler.view

import androidx.recyclerview.widget.RecyclerView

/**
 * Description : 布局方向
 * @Author by chenz
 * @Date 2022/1/13 0:18
 */
enum class Orientation(val rawType: Int) {
    /*垂直*/
    Vertical(RecyclerView.VERTICAL),

    /*水平*/
    Horizontal(RecyclerView.HORIZONTAL)
}