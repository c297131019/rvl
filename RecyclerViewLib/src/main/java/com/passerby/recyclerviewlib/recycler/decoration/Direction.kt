package com.passerby.recyclerviewlib.recycler.decoration

/**
 * 方位
 */
enum class Direction {
    /**
     * 左
     */
    LEFT,

    /**
     * 上
     */
    TOP,

    /**
     * 右
     */
    RIGHT,

    /**
     * 下
     */
    BOTTOM
}