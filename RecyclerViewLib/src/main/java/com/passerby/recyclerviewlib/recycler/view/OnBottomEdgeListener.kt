package com.passerby.recyclerviewlib.recycler.view

/**
 * Description : 滑动到底部边缘回调
 * @Author by chenz
 * @Date 2022/1/18 15:29
 */
interface OnBottomEdgeListener {
    fun onBottomEdge()
}