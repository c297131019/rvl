package com.passerby.recyclerviewlib.recycler.decoration;

/**
 * Date: 2021/11/22
 * author: ChenZhiJian
 */
public enum Gravity {
    /*Vertical时表示top，Horizontal时表示left*/
    Start,
    /*Vertical时表示center，Horizontal时表示center*/
    Center,
    /*Vertical时表示bottom，Horizontal时表示right*/
    End,
    /*只有一个元素*/
    Single,
}
