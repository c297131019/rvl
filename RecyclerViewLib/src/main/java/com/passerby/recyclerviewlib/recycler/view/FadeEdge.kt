package com.passerby.recyclerviewlib.recycler.view

import androidx.annotation.FloatRange

/**
 * Description : 边缘羽化值
 * @Author by chenz
 * @Date 2022/1/13 19:16
 *
 * 0.0~1.0的羽化值
 */
data class FadeEdge(
    @FloatRange(from = 0.0, to = 1.0) var fadeLeft: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) var fadeTop: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) var fadeRight: Float = 0f,
    @FloatRange(from = 0.0, to = 1.0) var fadeBottom: Float = 0f) {
}