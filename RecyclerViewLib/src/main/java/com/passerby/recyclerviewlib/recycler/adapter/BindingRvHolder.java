package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;
import android.view.View;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;

/**
 * Description : 支持ViewBinding的Holder，应该是更常用的类型
 */
public class BindingRvHolder<Item, Binding extends ViewBinding> extends RvHolder<Item> {

    protected Binding binding;


    public BindingRvHolder(@NonNull Context context, @NonNull View itemView) {
        super(context, itemView);
        Type type = this.getClass().getGenericSuperclass();
        if (type instanceof ParameterizedType) {
            try {
                Class<Binding> vbClass = (Class<Binding>) ((ParameterizedType) type).getActualTypeArguments()[1];
                Method bind = vbClass.getMethod("bind", View.class);
                binding = (Binding) bind.invoke(null, itemView);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 获取Binding
     */
    public Binding getBinding() {
        return binding;
    }
}
