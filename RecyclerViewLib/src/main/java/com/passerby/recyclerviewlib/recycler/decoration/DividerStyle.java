package com.passerby.recyclerviewlib.recycler.decoration;

/**
 * Date: 2021/11/22
 * author: ChenZhiJian
 */
public class DividerStyle {
    /*verticalWidth 垂直分割线宽度*/
    private int verticalWidth = 0;

    /*horizontalHeight 水平分割线高度*/
    private int horizontalHeight = 0;

    /*垂直分割线顶部padding，表示分割线顶部缩进*/
    private int verticalTopPadding = 0;

    /*垂直分割线底部padding，表示分割线底部缩进*/
    private int verticalBottomPadding = 0;

    /*水平分割线左边padding，表示分割线左边缩进*/
    private int horizontalLeftPadding = 0;

    /*水平分割线右边padding，表示分割线右边缩进*/
    private int horizontalRightPadding = 0;

    /**
     * 在左边的边距绘制分割线，只有分割线设置了边距才会生效，Linear和Grid的设置方式不同
     * 如果是Grid或StaggeredGrid设置该属性，那么{isDrawRightBorderDivide}也会同时生效
     */
    private boolean isDrawLeftBorderDivide;

    /**
     * 在右边的边距绘制分割线，只有分割线设置了边距才会生效，Linear和Grid的设置方式不同
     * 如果是Grid或StaggeredGrid设置该属性，那么{isDrawLeftBorderDivide}也会同时生效
     */
    private boolean isDrawRightBorderDivide;

    /**
     * 在顶部的边距绘制分割线，只有分割线设置了边距才会生效，Linear和Grid的设置方式不同
     * 如果是Grid或StaggeredGrid设置该属性，那么{isDrawBottomBorderDivide}也会同时生效
     */
    private boolean isDrawTopBorderDivide;

    /**
     * 在底部的边距绘制分割线，只有分割线设置了边距才会生效，Linear和Grid的设置方式不同
     * 如果是Grid或StaggeredGrid设置该属性，那么{isDrawTopBorderDivide}也会同时生效
     */
    private boolean isDrawBottomBorderDivide;

    /*割线在间距中的位置，如果是垂直分割线，gravity表示左中右，如果是水平分割线，gravity表示上中下
    只有当分割线的宽度或高度小于间距才有效，否则可以随意*/
    private Gravity gravity = Gravity.Center;


    public int getVerticalWidth() {
        return verticalWidth;
    }


    public void setVerticalWidth(int verticalWidth) {
        this.verticalWidth = verticalWidth;
    }


    public int getHorizontalHeight() {
        return horizontalHeight;
    }


    public void setHorizontalHeight(int horizontalHeight) {
        this.horizontalHeight = horizontalHeight;
    }


    public int getVerticalTopPadding() {
        return verticalTopPadding;
    }


    public void setVerticalTopPadding(int verticalTopPadding) {
        this.verticalTopPadding = verticalTopPadding;
    }


    public int getVerticalBottomPadding() {
        return verticalBottomPadding;
    }


    public void setVerticalBottomPadding(int verticalBottomPadding) {
        this.verticalBottomPadding = verticalBottomPadding;
    }


    public int getHorizontalLeftPadding() {
        return horizontalLeftPadding;
    }


    public void setHorizontalLeftPadding(int horizontalLeftPadding) {
        this.horizontalLeftPadding = horizontalLeftPadding;
    }


    public int getHorizontalRightPadding() {
        return horizontalRightPadding;
    }


    public void setHorizontalRightPadding(int horizontalRightPadding) {
        this.horizontalRightPadding = horizontalRightPadding;
    }


    public Gravity getGravity() {
        return gravity;
    }


    public void setGravity(Gravity gravity) {
        this.gravity = gravity;
    }


    public boolean isDrawLeftBorderDivide() {
        return isDrawLeftBorderDivide;
    }


    public void setDrawLeftBorderDivide(boolean drawLeftBorderDivide) {
        this.isDrawLeftBorderDivide = drawLeftBorderDivide;
    }


    public boolean isDrawRightBorderDivide() {
        return isDrawRightBorderDivide;
    }


    public void setDrawRightBorderDivide(boolean drawRightBorderDivide) {
        this.isDrawRightBorderDivide = drawRightBorderDivide;
    }


    public boolean isDrawTopBorderDivide() {
        return isDrawTopBorderDivide;
    }


    public void setDrawTopBorderDivide(boolean drawTopBorderDivide) {
        this.isDrawTopBorderDivide = drawTopBorderDivide;
    }


    public boolean isDrawBottomBorderDivide() {
        return isDrawBottomBorderDivide;
    }


    public void setDrawBottomBorderDivide(boolean drawBottomBorderDivide) {
        this.isDrawBottomBorderDivide = drawBottomBorderDivide;
    }
}
