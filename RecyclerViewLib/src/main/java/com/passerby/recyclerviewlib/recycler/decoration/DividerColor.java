package com.passerby.recyclerviewlib.recycler.decoration;

/**
 * Date: 2021/11/22
 * author: ChenZhiJian
 */
public class DividerColor extends DividerStyle {
    /*十六进制，0XFF000000*/
    private int color;


    public DividerColor(int color) {
        this.color = color;
    }


    public void setColor(int color) {
        this.color = color;
    }


    public int getColor() {
        return color;
    }
}
