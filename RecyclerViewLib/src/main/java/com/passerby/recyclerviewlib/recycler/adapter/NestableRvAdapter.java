package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 可嵌套的Adapter，在{@link RvAdapter}的基础上增强了嵌套功能
 * Date: 2021/11/15
 * author: ChenZhiJian
 */
public abstract class NestableRvAdapter<Item, Holder extends RvHolder<Item>> extends RvAdapter<Item, Holder> {

    private final String ADAPTER_ID;
    NestableAdapterController<NestableRvAdapter<?, RvHolder<?>>> adapterController;
    private WeakReference<NestableRvAdapter<?, RvHolder<?>>> parentAdapterReference;
    private final List<NestableRvAdapter<?, RvHolder<?>>> childrenAdapter = new ArrayList<>();


    public NestableRvAdapter(@NonNull Context context) {
        this(context, UUID.randomUUID().toString());
    }


    public NestableRvAdapter(@NonNull Context context, @NonNull String adapterId) {
        super(context);
        ADAPTER_ID = adapterId;
        adapterController = new NestableAdapterController<>((NestableRvAdapter<?, RvHolder<?>>) this);
    }


    /**
     * 添加到父Adapter中
     */
    @CallSuper
    protected void attachedParentAdapter(NestableRvAdapter<?, RvHolder<?>> parentAdapter) {
        parentAdapterReference = new WeakReference<>(parentAdapter);
        //这里可以用动态规划来优化
        teamAction(getRootAdapter(), adapter -> {
            adapter.adapterController.suspend();
            adapter.adapterController.reboot();
        });
    }


    /**
     * 从父Adapter中移除
     */
    @CallSuper
    protected void detachParentAdapter(NestableRvAdapter<?, RvHolder<?>> parentAdapter) {
        if (parentAdapterReference != null) {
            parentAdapterReference.clear();
            parentAdapterReference = null;
        }
        //这里可以用动态规划来优化
        teamAction(getRootAdapter(), adapter -> {
            adapter.adapterController.suspend();
            adapter.adapterController.dispose();
        });
    }


    /**
     * 这个API是全局的，RecyclerView只会读取给它设置的Adapter的参数，所以给其中一个设置，或者其中一个不设置是无效的
     * 要么都设置，要么都不设置，这个问题无法单独处理。所以这里设计成一旦设置，会给最顶级的Adapter设置该属性，让RecyclerView读取它的即可
     */
    @Override
    public void setHasStableIds(boolean hasStableIds) {
        NestableRvAdapter<?, RvHolder<?>> parent = getRootAdapter();
        if (parent != null) {
            parent.setHasStableIds(hasStableIds);
        } else {
            super.setHasStableIds(hasStableIds);
        }
    }


    /**
     * 这个API是全局的，RecyclerView只会读取给它设置的Adapter的参数，所以给其中一个设置，或者其中一个不设置是无效的
     * 要么都设置，要么都不设置，这个问题无法单独处理。所以这里设计成一旦设置，会给最顶级的Adapter设置该属性，让RecyclerView读取它的即可
     */
    @Override
    public void setStateRestorationPolicy(@NonNull StateRestorationPolicy strategy) {
        NestableRvAdapter<?, RvHolder<?>> parent = getRootAdapter();
        if (parent != null) {
            parent.setStateRestorationPolicy(strategy);
        } else {
            super.setStateRestorationPolicy(strategy);
        }
    }


    @NonNull
    @Override
    public final Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return (Holder) dispatchCreateHolder(getRootAdapter(), parent, viewType);
    }


    /**
     * 构造Holder，从根Adapter开始分发下去
     *
     * @param adapter 需要查询的adapter，调用处应传入rootAdapter
     */
    @Nullable
    private RvHolder<?> dispatchCreateHolder(NestableRvAdapter<?, RvHolder<?>> adapter, @NonNull ViewGroup parent, int viewType) {
        if (adapter.adapterController.hasItemType(viewType)) {
            return super.onCreateViewHolder(parent, viewType);
        }
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : adapter.getInternalChildAdapter()) {
            RvHolder<?> rvHolder = childAdapter.dispatchCreateHolder(childAdapter, parent, viewType);
            if (rvHolder != null) {
                return rvHolder;
            }
        }
        if (getParentAdapter() != null) {
            return null;
        } else {
            throw new IllegalArgumentException("dispatchCreateHolder(), No Adapter that can generate Holder was found, type = " + viewType);
        }
    }


    @Override
    public final void onBindViewHolder(@NonNull Holder holder, int position) {
        if (adapterController.isGlobalRangeContainsGlobalPosition(position)) {
            super.onBindViewHolder(holder, convertLocalPosition(position));
            return;
        }
        dispatchBindHolder((NestableRvAdapter<?, RvHolder<?>>) this, holder, position);
    }


    @Override
    public final void onBindViewHolder(@NonNull Holder holder, int position, @NonNull List<Object> payloads) {
        if (adapterController.isGlobalRangeContainsGlobalPosition(position)) {
            super.onBindViewHolder(holder, convertLocalPosition(position), payloads);
            return;
        }
        dispatchBindHolder((NestableRvAdapter<?, RvHolder<?>>) this, holder, position);
    }


    /**
     * 绑定Holder，从根Adapter开始分发下去
     * 调用{@link #bindViewHolder(RecyclerView.ViewHolder, int)}会分发到子Adapter的{@link #onBindViewHolder}中继续判断和执行
     *
     * 这里不直接调用子Adapter{@link #onBindViewHolder}或{@link #onBindHolder}的原因是为了更好的适配系统的API
     * 否则{@link RecyclerView.ViewHolder#getBindingAdapter()}拿到的将是一个错误的对象
     */
    private void dispatchBindHolder(NestableRvAdapter<?, RvHolder<?>> adapter, @NonNull RvHolder<?> holder, int position) {
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : adapter.childrenAdapter) {
            childAdapter.bindViewHolder(holder, position);
        }
    }


    @Override
    public final int getItemViewType(int position) {
        if (adapterController.isGlobalRangeContainsGlobalPosition(position)) {
            int type = getLocalItemViewType(convertLocalPosition(position));
            if (type > 0) {
                adapterController.rememberItemType(type);
            }
            return type;
        }
        if (!childrenAdapter.isEmpty()) {
            for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
                int type = childAdapter.getItemViewType(position);
                if (type > 0) {
                    return type;
                }
            }
        }
        if (getParentAdapter() != null) {
            return 0;
        } else {
            throw new IllegalArgumentException("getItemViewType() Could not get the correct type. position = " + position);
        }
    }


    /**
     * 获取相对当前Adapter内的itemType
     *
     * 这里要注意，在多层嵌套的Adapter中使用多布局，千万不要用0,1,2,3这种大众化的序列为返回值，会出现的情况：
     * 1. BlueAdapter.getItemType = 0 , RedAdapter.getItemType = 0，但是两个Adapter的视图完全不一样，
     * 对于RecyclerView来说，只要返回相同的type就会去同一个缓存池中复用Holder，这时BlueAdapter复用了RedAdapter的视图，就会崩溃
     *
     * 2. RedAdapter1.getItemType = 0, RedAdapter2.getItemType = 0，但其实两个Adapter是相同的，
     * 如果使用itemType隔离，也就是将返回的type内部处理，禁止生成相同type，但这样就会造成多个复用池，
     * 并且相同的Adapter无法复用
     *
     * 所以这里强烈建议使用xml布局的资源id为返回值！！！
     * 查询过程自根Adapter向下查询，为避免频繁查询，返回的type会被记录到Adapter中。
     *
     * @param position 传入相对当前Adapter的position
     */
    public abstract int getLocalItemViewType(int position);


    @Override
    public final long getItemId(int position) {
        if (adapterController.isGlobalRangeContainsGlobalPosition(position)) {
            return getLocalItemId(convertLocalPosition(position));
        }
        if (!childrenAdapter.isEmpty()) {
            for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
                long itemId = childAdapter.getLocalItemId(position);
                if (itemId != -1) {
                    return itemId;
                }
            }
        }
        return super.getItemId(position);
    }


    public long getLocalItemId(int position) {
        return super.getItemId(position);
    }


    /**
     * 获取当前包含子Adapter在内的Item个数
     *
     * 不要使用这个来获取Adapter有多少个Item，可以使用 {@link #getItemSize()}
     */
    @Override
    public final int getItemCount() {
        int totalItemCount = getItemSize();
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
            totalItemCount += childAdapter.getItemCount();
        }
        return totalItemCount;
    }


    /**
     * 防止有些人不看注释直接调{@link #getItemCount()}，特意搞一个方法迷惑，迫使点进来看看
     *
     * @return 这个方法返回当前Adapter的Item个数
     */
    public int getLocalItemCount() {
        return getItemSize();
    }


    /**
     * 系统API，根据全局position转换本地position
     */
    @Override
    public final int findRelativeAdapterPositionIn(@NonNull RecyclerView.Adapter<? extends RecyclerView.ViewHolder> adapter, @NonNull RecyclerView.ViewHolder viewHolder, int globalPosition) {
        //将全局position转换成本地position，系统要用，避免出问题，这里禁止重写
        if (adapterController.isGlobalRangeContainsGlobalPosition(globalPosition)) {
            return convertLocalPosition(globalPosition);
        }
        return RecyclerView.NO_POSITION;
    }


    /**
     * 相对全局Adapter的Holder被回收了
     * 如果只想处理自己Adapter，请用{@link #onLocalViewRecycled(RvHolder)}
     *
     * @param holder 被回收的Holder
     */
    @Override
    public void onViewRecycled(@NonNull Holder holder) {
        if (holder.getBindingAdapter() == this) {
            onLocalViewRecycled(holder);
        }
        super.onViewRecycled(holder);
    }


    /**
     * 相对当前Adapter的Holder被回收了
     *
     * @param holder 被回收的Holder
     */
    public void onLocalViewRecycled(@NonNull Holder holder) {

    }


    /**
     * 相对全局Adapter的Holder添加到RecyclerView失败了
     * 如果只想处理自己Adapter，请用{@link #onLocalFailedToRecycleView(RvHolder)}
     *
     * @param holder 添加失败的Holder
     */
    @Override
    public boolean onFailedToRecycleView(@NonNull Holder holder) {
        if (holder.getBindingAdapter() == this) {
            return onLocalFailedToRecycleView(holder);
        }
        return super.onFailedToRecycleView(holder);
    }


    /**
     * 相对当前Adapter的Holder添加到RecyclerView失败了
     *
     * @param holder 添加失败的Holder
     */
    public boolean onLocalFailedToRecycleView(@NonNull Holder holder) {
        return super.onFailedToRecycleView(holder);
    }


    /**
     * 相对全局Adapter的Holder添加到视图上时
     * 如果只想处理自己Adapter，请用{@link #onLocalViewAttachedToWindow(RvHolder)}
     *
     * @param holder 添加Holder
     */
    @Override
    public void onViewAttachedToWindow(@NonNull Holder holder) {
        if (holder.getBindingAdapter() == this) {
            onLocalViewAttachedToWindow(holder);
        }
        super.onViewAttachedToWindow(holder);
    }


    /**
     * 相对当前Adapter的Holder添加到视图上时
     */
    public void onLocalViewAttachedToWindow(@NonNull Holder holder) {

    }


    /**
     * 相对全局Adapter的Holder添加到视图上时
     * 如果只想处理自己Adapter，请用{@link #onLocalViewDetachedFromWindow(RvHolder)}
     *
     * @param holder 移除的Holder
     */
    @Override
    public void onViewDetachedFromWindow(@NonNull Holder holder) {
        if (holder.getBindingAdapter() == this) {
            onLocalViewDetachedFromWindow(holder);
        }
        super.onViewDetachedFromWindow(holder);
    }


    /**
     * 相对当前Adapter的Holder添加到视图上时
     */
    public void onLocalViewDetachedFromWindow(@NonNull Holder holder) {

    }


    /**
     * 注册当前Adapter的事件，推荐使用
     *
     * @param observer 观察者
     */
    public void registerLocalAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        adapterController.registerLocalAdapterDataObserver(observer);
    }


    /**
     * 注销当前Adapter的事件，推荐使用
     *
     * @param observer 观察者
     */
    public void unregisterLocalAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        adapterController.unregisterLocalAdapterDataObserver(observer);
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
            childAdapter.onAttachedToRecyclerView(recyclerView);
        }
    }


    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
            childAdapter.onDetachedFromRecyclerView(recyclerView);
        }
    }


    /**
     * 刷新当前Adapter相关的全部数据，如果只关心自己Adapter的话，推荐用这个
     * 如果要刷新RecyclerView所有数据，可以使用{@link #notifyDataSetChanged()}，如非需要，不推荐使用
     *
     * 请注意，调用这个刷新，回调的是{@link RecyclerView.AdapterDataObserver#onItemRangeChanged(int, int)}
     * 如果将来有需要控制回调到{@link RecyclerView.AdapterDataObserver#onChanged()}，则需要调整一下逻辑
     */
    public void notifyLocalDataSetChanged() {
        adapterController.notifyLocalDataSetChanged();
    }


    /**
     * 需要整体执行一个功能的时候调用
     */
    void teamAction(NestableRvAdapter<?, ? extends RvHolder<?>> rootAdapter, NestableAdapterAction action) {
        if (rootAdapter == null) {
            rootAdapter = getRootAdapter();
        }
        action.onAction(rootAdapter);
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : rootAdapter.childrenAdapter) {
            childAdapter.teamAction(childAdapter, action);
        }
    }


    /**
     * ---------------------------------公开的方法---------------------------------
     **/

    @Override
    public void clearItems() {
        store.clear();
        notifyLocalDataSetChanged();
    }


    /**
     * 添加子Adapter
     *
     * @param child 子Adapter
     */
    public <Adapter extends NestableRvAdapter<?, ? extends RvHolder<?>>> NestableRvAdapter<Item, Holder> addAdapter(@NonNull Adapter child) {
        return addAdapter(childrenAdapter.size(), child);
    }


    /**
     * 添加子Adapter
     *
     * @param index 插入某个位置
     * @param child 子Adapter
     */
    public <Adapter extends NestableRvAdapter<?, ? extends RvHolder<?>>> NestableRvAdapter<Item, Holder> addAdapter(int index, @NonNull Adapter child) {
        if (index < 0 || index > childrenAdapter.size()) {
            throw new IndexOutOfBoundsException("Index must be between 0 and " + childrenAdapter.size() + ". Given:" + index);
        }
        if (child.getParentAdapter() != null) {
            throw new IllegalStateException("child is already attached parent");
        }
        if (!childrenAdapter.contains(child)) {
            childrenAdapter.add(index, (NestableRvAdapter<?, RvHolder<?>>) child);
            child.attachedParentAdapter((NestableRvAdapter<?, RvHolder<?>>) this);
            //如果根Adapter已经添加到RecyclerView中，添加子Adapter的时候会主动通知子Adapter被添加了
            if (getRecyclerView() != null) {
                child.onAttachedToRecyclerView(getRecyclerView());
            }
            //不管有没有添加到RecyclerView中都要刷新一遍数据，因为所有的Adapter都是在刷新的过程中动态计算自己的索引区域
            int notifyStartPosition = child.adapterController.getGlobalStartPosition();
            notifyItemRangeInserted(convertLocalPosition(notifyStartPosition), child.getItemCount());
        }
        return this;
    }


    /**
     * 替换某个子Adapter
     *
     * @param index 替换的位置
     * @param child 子Adapter
     */
    public <Adapter extends NestableRvAdapter<?, ? extends RvHolder<?>>> void setAdapter(int index, @NonNull Adapter child) {
        if (index < 0 || index > childrenAdapter.size()) {
            throw new IndexOutOfBoundsException("Index must be between 0 and " + childrenAdapter.size() + ". Given:" + index);
        }
        if (child.getParentAdapter() != null) {
            throw new IllegalStateException("child is already attached parent");
        }
        if (index < childrenAdapter.size() && !childrenAdapter.contains(child)) {
            childrenAdapter.set(index, (NestableRvAdapter<?, RvHolder<?>>) child);
            child.attachedParentAdapter((NestableRvAdapter<?, RvHolder<?>>) this);
            //如果根Adapter已经添加到RecyclerView中，添加子Adapter的时候会主动通知子Adapter被添加了
            if (getRecyclerView() != null) {
                child.onAttachedToRecyclerView(getRecyclerView());
            }
            //不管有没有添加到RecyclerView中都要刷新一遍数据，因为所有的Adapter都是在刷新的过程中动态计算自己的索引区域
            int notifyStartPosition = child.adapterController.getGlobalStartPosition();
            notifyItemRangeChanged(convertLocalPosition(notifyStartPosition), child.getItemCount());
        }
    }


    /**
     * 移除子Adapter
     *
     * @param fromRoot 是否从根开始往下查询，否则从自己子Adapter往下查询，如果可以确认是自己子Adapter，最好传true，避免查询性能问题
     * @param child 子Adapter
     */
    public boolean removeAdapter(@NonNull NestableRvAdapter<?, ? extends RvHolder<?>> child, boolean fromRoot) {
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = fromRoot ? getRootAdapter() : (NestableRvAdapter<?, RvHolder<?>>) this;
        if (rootAdapter.childrenAdapter.remove(child)) {
            int notifyStartPosition = child.adapterController.getGlobalStartPosition();
            child.detachParentAdapter((NestableRvAdapter<?, RvHolder<?>>) this);
            if (getRecyclerView() != null) {
                //如果根Adapter已经添加到RecyclerView中，添加子Adapter的时候会主动通知子Adapter被添加了
                child.onDetachedFromRecyclerView(getRecyclerView());
            }
            //不管有没有添加到RecyclerView中都要刷新一遍数据，因为所有的Adapter都是在刷新的过程中动态计算自己的索引区域
            notifyItemRangeRemoved(convertLocalPosition(notifyStartPosition), child.getItemCount());
            return true;
        }
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : rootAdapter.childrenAdapter) {
            if (childAdapter.removeAdapter(child, false)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 移除子Adapter
     *
     * @param fromRoot 是否从根开始往下查询，否则从自己子Adapter往下查询，如果可以确认是自己子Adapter，最好传true，避免查询性能问题
     * @param adapterId adapterId，每个都是唯一的
     */
    public boolean removeAdapter(@NonNull String adapterId, boolean fromRoot) {
        if (getAdapterId().equals(adapterId)) {
            if (getParentAdapter() != null) {
                return getParentAdapter().removeAdapter(this, false);
            }
            //自己是不能移除掉自己，讲道理应该抛个异常。不过为了正确性这里返回true，避免继续往下查询
            return true;
        }
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : (fromRoot ? getRootAdapter() : this).childrenAdapter) {
            if (childAdapter.removeAdapter(adapterId, false)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 查找Adapter，
     *
     * @param adapterId adapterId，每个都是唯一的
     * @param fromRoot 是否从根开始往下查询，否则从自己子Adapter往下查询，如果可以确认是自己子Adapter，最好传true，避免查询性能问题
     * @return 查询到返回目标Adapter，查询不到返回空
     */
    @Nullable
    public NestableRvAdapter<?, RvHolder<?>> findChildAdapter(String adapterId, boolean fromRoot) {
        if (getAdapterId().equals(adapterId)) {
            return (NestableRvAdapter<?, RvHolder<?>>) this;
        }
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : (fromRoot ? getRootAdapter() : this).childrenAdapter) {
            NestableRvAdapter<?, RvHolder<?>> result = childAdapter.findChildAdapter(adapterId, false);
            if (result != null) {
                return result;
            }
        }
        return null;
    }


    /**
     * 获取父Adapter
     */
    @Nullable
    public NestableRvAdapter<?, RvHolder<?>> getParentAdapter() {
        return parentAdapterReference == null ? null : parentAdapterReference.get();
    }


    /**
     * 获取根Adapter，如果顶级是自己也会返回自己
     */
    public NestableRvAdapter<?, RvHolder<?>> getRootAdapter() {
        //有的人很狡猾，构建了嵌套的adapter，却拿子Adapter去设置RecyclerView，所以第一步先查询RecyclerView的Adapter
        if (getRecyclerView() != null && getRecyclerView().getAdapter() instanceof NestableRvAdapter<?, ?>) {
            return (NestableRvAdapter<?, RvHolder<?>>) getRecyclerView().getAdapter();
        }
        NestableRvAdapter<?, RvHolder<?>> parentAdapter = getParentAdapter();
        while (parentAdapter != null && parentAdapter.getParentAdapter() != null) {
            parentAdapter = parentAdapter.getParentAdapter();
        }
        return parentAdapter != null ? parentAdapter : (NestableRvAdapter<?, RvHolder<?>>) this;
    }


    /**
     * 获取子Adapter列表
     */
    public List<NestableRvAdapter<?, RvHolder<?>>> getChildrenAdapter() {
        return new ArrayList<>(childrenAdapter);
    }


    //只在内部使用，直接获取
    List<NestableRvAdapter<?, RvHolder<?>>> getInternalChildAdapter() {
        return childrenAdapter;
    }


    /**
     * 获取直接子Adapter个数
     */
    public int getChildrenAdapterCount() {
        return childrenAdapter.size();
    }


    /**
     * 获取所有子Adapter个数
     */
    public int getAllChildrenAdapterCount() {
        if (childrenAdapter.isEmpty()) {
            return getChildrenAdapterCount();
        }
        int allCount = 0;
        for (NestableRvAdapter<?, RvHolder<?>> adapter : childrenAdapter) {
            allCount += adapter.getAllChildrenAdapterCount();
        }
        return allCount;
    }


    /**
     * 获取Adapter唯一标识ID
     */
    public final String getAdapterId() {
        return ADAPTER_ID;
    }


    /**
     * 是否包含这个子adapter，只往下查询
     *
     * @param adapter 子Adapter
     */
    public boolean containsChildrenAdapter(NestableRvAdapter<?, RvHolder<?>> adapter) {
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : childrenAdapter) {
            if (childAdapter.containsChildrenAdapter(adapter)) {
                return true;
            }
        }
        return childrenAdapter.contains(adapter);
    }


    /**
     * 当前Adapter索引转换成全局索引
     *
     * @param localPosition 当前Adapter的索引
     * @return 如果被转换的索引不在这个Adapter范围，则返回-1
     */
    public int convertGlobalPosition(int localPosition) {
        return adapterController.convertGlobalPosition(localPosition);
    }


    /**
     * 全局索引转换成当前Adapter的索引
     *
     * @param globalPosition 全局索引
     * @return 如果被转换的索引不在这个Adapter范围，则返回-1
     */

    public int convertLocalPosition(int globalPosition) {
        return adapterController.convertLocalPosition(globalPosition);
    }


    /**
     * 获取相对全局索引的起始位置
     */
    public int getGlobalStartPosition() {
        return adapterController.getGlobalStartPosition();
    }


    /**
     * 获取相对全局索引的结束位置
     */
    public int getGlobalEndPosition() {
        return adapterController.getGlobalEndPosition();
    }

}
