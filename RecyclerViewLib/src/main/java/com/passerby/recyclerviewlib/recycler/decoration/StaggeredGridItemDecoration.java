package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Canvas;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridHelperKt;

/**
 * Date: 2021/12/23
 * author: ChenZhiJian
 *
 * 1. 中间的分割线会存在1个像素的误差，原因：
 * 1) 是除不尽
 * 2) 第一个View的右边位置是300，而第二个左边的位置可能返回299
 * 如果用不同的颜色分别绘制上下左右分割线就可以看出
 *
 * 2. 因为判断不了最后一行，会出现以下一些暂时发现的问题，这个问题应该是无法解决的，他们不能通过计算准确判断
 * 1) 垂直5列，最后几个的位置判断有问题
 * 2) 所有对最后一行底部分割线控制的功能全部无效
 *
 * 所以最后一行的底部间距不能动态的控制，只能默认显示
 *
 * 综合以上的原因，不建议使用分割线的所有功能，自己弄好间距填充个背景当分割线
 */
public class StaggeredGridItemDecoration extends GridItemDecoration {

    public StaggeredGridItemDecoration() {
    }


    public StaggeredGridItemDecoration(int spacingWidth, int spacingHeight) {
        super(spacingWidth, spacingHeight);
    }


    public StaggeredGridItemDecoration(int spacingWidth, int spacingHeight, boolean isShowVerticalBorder, boolean isShowHorizontalBorder) {
        super(spacingWidth, spacingHeight, isShowVerticalBorder, isShowHorizontalBorder);
    }


    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.onDraw(c, parent, state);
        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            //修改ItemDecoration刷新标识
            StaggeredGridHelperKt.setInsetsDirty(child, true);
        }
    }
}
