//修改包名，可以访问default成员
package androidx.recyclerview.widget

import android.view.View

internal fun View.setInsetsDirty(isInsetsDirty: Boolean) {
    (this.layoutParams as? RecyclerView.LayoutParams)?.apply {
        //这个是ItemDecoration的缓存标识，不需要刷新ItemDecoration的时候就会判断这个值
        //但是目前StaggeredGrid会自动重排，并且不刷新ItemDecoration，所以需要强制修改这个值需要让他在任何时候都要重新刷新
        this.mInsetsDirty = isInsetsDirty
    }
}

