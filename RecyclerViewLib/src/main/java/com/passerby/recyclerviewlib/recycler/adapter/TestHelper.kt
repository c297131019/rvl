package com.passerby.recyclerviewlib.recycler.adapter

import org.jetbrains.annotations.TestOnly
import java.lang.StringBuilder

/**
 * Description :
 * @Author by chenz
 * @Date 2022/2/12 14:05
 */


@TestOnly
internal fun NestableRvAdapter<*, RvHolder<*>>.printLevel(): String {
    var current: NestableRvAdapter<*, RvHolder<*>>? = this
    var parent = parentAdapter
    var level = 0
    val strings = arrayListOf<String>()
    while (parent != null) {
        parent = parent.parentAdapter
        level++
    }
    parent = current
    do {
        parent = parent?.parentAdapter
        strings.add("${level--.tabString()}[${
            parent?.childrenAdapter?.indexOf(current)
        }]{${current?.adapterInfo()}}\n")
        current = parent
    } while (parent != null)
    return strings.reversed().toString()
}


private fun NestableRvAdapter<*, *>?.adapterInfo(): String {
    if (this == null) {
        return ""
    }
    return "globalStartPosition = ${adapterController.globalStartPosition}, itemSize = ${itemSize}, childSize = ${childrenAdapter.size}"
}

private fun Int.tabString(): String {
    val sb = StringBuilder()
    (0 until this).forEach {
        sb.append("\t")
    }
    return sb.toString()
}