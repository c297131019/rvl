package com.passerby.recyclerviewlib.recycler.layoutManager

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

/**
 * Description : GridLayoutManager专业版
 * @Author by chenz
 * @Date 2022/1/13 18:36
 */
internal class GridLayoutManagerPro : GridLayoutManager {

    /*是否可以滑动*/
    private var isScrollable = true

    constructor(context: Context?, spanCount: Int) : super(context, spanCount)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes)

    constructor(context: Context?, spanCount: Int, orientation: Int, reverseLayout: Boolean)
            : super(context, spanCount, orientation, reverseLayout)


    override fun canScrollVertically(): Boolean {
        return isScrollable && super.canScrollVertically()
    }

    override fun canScrollHorizontally(): Boolean {
        return isScrollable && super.canScrollHorizontally()
    }

    /**
     * 设置是否可以滑动
     */
    fun setScrollable(isScrollable: Boolean) {
        this.isScrollable = isScrollable
    }
}