package com.passerby.recyclerviewlib.recycler.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.annotation.FloatRange
import androidx.recyclerview.widget.RecyclerView
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder
import com.passerby.recyclerviewlib.recycler.layoutManager.GridLayoutManagerPro
import com.passerby.recyclerviewlib.recycler.layoutManager.LinearLayoutManagerPro
import com.passerby.recyclerviewlib.recycler.layoutManager.StaggeredGridLayoutManagerPro

/**
 * Description : RecyclerView专业版
 * @Author by chenz
 * @Date 2022/1/13 0:08
 */
class SuperRecyclerViewPro : RecyclerView, RecyclerViewFeature {

    /*RecyclerView的属性*/
    private val attrs: RecyclerAttrs

    /*RecyclerView的成员*/
    private val member = RecyclerMember()

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        this.attrs = RecyclerAttrs(context, attrs)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        //设置LayoutManager
        initLayoutManager()
        //设置羽化值
        initFadeEdge()
        //设置装饰
        removeItemDecoration(attrs.decoration)
        addItemDecoration(attrs.decoration)
    }

    /***
     * 初始化LayoutManager
     */
    private fun initLayoutManager() {
        layoutManager = when (attrs.layoutType) {
            LayoutType.Linear -> {
                LinearLayoutManagerPro(context, attrs.orientation.rawType,
                    attrs.reverseLayout)
            }
            LayoutType.Grid -> {
                GridLayoutManagerPro(context, attrs.spanCount,
                    attrs.orientation.rawType,
                    attrs.reverseLayout)
            }
            LayoutType.Staggered -> {
                StaggeredGridLayoutManagerPro(attrs.orientation.rawType,
                    attrs.orientation.rawType).apply {
                    reverseLayout = attrs.reverseLayout
                }
            }
        }
    }

    /**
     * 初始化设置羽化边缘
     */
    private fun initFadeEdge() {
        //只要有一边有值就启用
        isVerticalFadingEdgeEnabled =
            attrs.fadeEdge.fadeLeft + attrs.fadeEdge.fadeRight > 0f
        isHorizontalFadingEdgeEnabled =
            attrs.fadeEdge.fadeTop + attrs.fadeEdge.fadeBottom > 0f
    }

    /**
     * 设置边缘羽化值
     */
    fun setFadeEdge(
        @FloatRange(from = 0.0, to = 1.0) left: Float = attrs.fadeEdge.fadeLeft,
        @FloatRange(from = 0.0, to = 1.0) top: Float = attrs.fadeEdge.fadeTop,
        @FloatRange(from = 0.0, to = 1.0) right: Float = attrs.fadeEdge.fadeRight,
        @FloatRange(from = 0.0, to = 1.0) bottom: Float = attrs.fadeEdge.fadeBottom) {
        attrs.fadeEdge.fadeLeft = left
        attrs.fadeEdge.fadeTop = top
        attrs.fadeEdge.fadeRight = right
        attrs.fadeEdge.fadeBottom = bottom
        initFadeEdge()
        //需要requestLayout?
    }

    /**
     * 左边羽化值
     */
    override fun getLeftFadingEdgeStrength(): Float {
        return attrs.fadeEdge.fadeLeft
    }

    /**
     * 顶部羽化值
     */
    override fun getTopFadingEdgeStrength(): Float {
        return attrs.fadeEdge.fadeTop
    }

    /**
     * 右边羽化值
     */
    override fun getRightFadingEdgeStrength(): Float {
        return attrs.fadeEdge.fadeRight
    }

    /**
     * 底部羽化值
     */
    override fun getBottomFadingEdgeStrength(): Float {
        return attrs.fadeEdge.fadeBottom
    }

    @Deprecated("废弃了", ReplaceWith("用setFadeEdge()代替"))
    override fun setFadingEdgeLength(length: Int) {
        super.setFadingEdgeLength(length)
    }

    override fun setStateView(view: View) {
        TODO("Not yet implemented")
    }

    override fun getStateView(view: View) {
        TODO("Not yet implemented")
    }

    override fun isStateViewVisible(): Boolean {
        TODO("Not yet implemented")
    }

    override fun setEmptyView(view: View) {
        TODO("Not yet implemented")
    }

    override fun getEmptyView(): View {
        TODO("Not yet implemented")
    }

    override fun isEmptyViewVisible(): Boolean {
        TODO("Not yet implemented")
    }

    override fun setLoadingView(view: View) {
        TODO("Not yet implemented")
    }

    override fun getLoadingView(): View {
        TODO("Not yet implemented")
    }

    override fun isLoadingViewVisible(): Boolean {
        TODO("Not yet implemented")
    }

    override fun addHeaderHolder(view: View) {
        TODO("Not yet implemented")
    }

    override fun removeHeaderHolder(view: View) {
        TODO("Not yet implemented")
    }

    override fun addFooterHolder(view: View) {
        TODO("Not yet implemented")
    }

    override fun removeFooterHolder(view: View) {
        TODO("Not yet implemented")
    }

    override fun getHeaderCount(): Int {
        TODO("Not yet implemented")
    }

    override fun getFooterCount(): Int {
        TODO("Not yet implemented")
    }

    override fun getHoldersByType(type: Int): List<RvHolder<*>> {
        TODO("Not yet implemented")
    }
}