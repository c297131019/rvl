package com.passerby.recyclerviewlib.recycler.view

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import com.passerby.recyclerviewlib.R
import com.passerby.recyclerviewlib.recycler.decoration.*

/**
 * Description : RecyclerView属性
 * @Author by chenz
 * @Date 2022/1/13 0:18
 */
internal class RecyclerAttrs(context: Context, attrs: AttributeSet?) {

    /*布局类型*/
    var layoutType = LayoutType.Linear

    /*布局方向*/
    var orientation = Orientation.Vertical

    /*列数*/
    var spanCount = 1

    /*分割线*/
    var decoration: AbsItemDecoration

    /*是否翻转列表*/
    var reverseLayout: Boolean

    /*是否可以滑动*/
    var isScrollable: Boolean

    /*羽化边缘值*/
    var fadeEdge: FadeEdge

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.SuperRecyclerViewPro)
        typedArray.run {
            layoutType = when (getInt(R.styleable.SuperRecyclerViewPro_srvp_layoutType, 1)) {
                1 -> LayoutType.Linear
                2 -> LayoutType.Grid
                else -> LayoutType.Staggered
            }
            orientation =
                when (getInt(R.styleable.SuperRecyclerViewPro_srvp_layoutOrientation, 1)) {
                    1 -> Orientation.Vertical
                    else -> Orientation.Horizontal
                }
            decoration = takeDecoration(layoutType, this)
            fadeEdge = takeFadeEdge(this)
            spanCount = getInt(R.styleable.SuperRecyclerViewPro_srvp_spanCount, spanCount)
            reverseLayout = getBoolean(R.styleable.SuperRecyclerViewPro_srvp_reverseLayout, false)
            isScrollable = getBoolean(R.styleable.SuperRecyclerViewPro_srvp_isScrollable, true)
        }
        typedArray.recycle()
    }

    /**
     * 获取装饰
     */
    private fun takeDecoration(layoutType: LayoutType,
                               typedArray: TypedArray): AbsItemDecoration {
        return typedArray.run {
            val spacingWidth =
                getDimension(R.styleable.SuperRecyclerViewPro_srvp_decorationSpacingWidth, 0f).toInt()
            val spacingHeight =
                getDimension(R.styleable.SuperRecyclerViewPro_srvp_decorationSpacingHeight, 0f).toInt()
            when (layoutType) {
                LayoutType.Linear -> {
                    LinearItemDecoration(spacingWidth, spacingHeight)
                }
                else -> {
                    StaggeredGridItemDecoration(spacingWidth, spacingHeight)
                }
            }.apply {
                //设置分割线样式
                when (val style = takeDividerStyle(typedArray)) {
                    is DividerColor -> {
                        setDividerColor(style)
                    }
                    is DividerDrawable -> {
                        setDividerDrawable(style)
                    }
                }
                //设置装饰特有的属性
                when (this) {
                    is StaggeredGridItemDecoration -> {
                        setShowVerticalBorder(
                            getBoolean(R.styleable.SuperRecyclerViewPro_srvp_decorationVerticalBorder,
                                false))
                        setShowHorizontalBorder(
                            getBoolean(R.styleable.SuperRecyclerViewPro_srvp_decorationHorizontalBorder,
                                false))
                    }
                }
            }
        }
    }

    /**
     * 获取分割线样式
     */
    private fun takeDividerStyle(typedArray: TypedArray): DividerStyle? {
        return typedArray.run {
            return when {
                getColor(R.styleable.SuperRecyclerViewPro_srvp_dividerColor, -1) != -1 -> {
                    DividerColor(
                        typedArray.getColor(R.styleable.SuperRecyclerViewPro_srvp_dividerColor, 0))
                }
                getDrawable(R.styleable.SuperRecyclerViewPro_srvp_dividerDrawable) != null -> {
                    DividerDrawable(
                        typedArray.getDrawable(R.styleable.SuperRecyclerViewPro_srvp_dividerDrawable))
                }
                else -> null
            }?.apply {
                val verticalWidth =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerVerticalWidth, 0f)
                val horizontalHeight =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerHorizontalHeight, 0f)
                var topPadding =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerVerticalTopPadding, 0f)
                var bottomPadding =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerVerticalBottomPadding, 0f)
                var leftPadding =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerHorizontalLeftPadding, 0f)
                var rightPadding =
                    getDimension(R.styleable.SuperRecyclerViewPro_srvp_dividerHorizontalRightPadding, 0f)
                var gravity = when (getInt(R.styleable.SuperRecyclerViewPro_srvp_dividerGravity, 3)) {
                    1 -> Gravity.Start
                    2 -> Gravity.End
                    else -> Gravity.Center
                }
                setVerticalWidth(verticalWidth.toInt())
                setHorizontalHeight(horizontalHeight.toInt())
                horizontalLeftPadding = leftPadding.toInt()
                horizontalRightPadding = rightPadding.toInt()
                verticalTopPadding = topPadding.toInt()
                verticalBottomPadding = bottomPadding.toInt()
                setGravity(gravity)
            }
        }
    }

    /**
     * 获取羽化值
     */
    private fun takeFadeEdge(typedArray: TypedArray): FadeEdge {
        return typedArray.run {
            FadeEdge(
                getFloat(R.styleable.SuperRecyclerViewPro_srvp_fadeLeftEdge, 0f),
                getFloat(R.styleable.SuperRecyclerViewPro_srvp_fadeTopEdge, 0f),
                getFloat(R.styleable.SuperRecyclerViewPro_srvp_fadeRightEdge, 0f),
                getFloat(R.styleable.SuperRecyclerViewPro_srvp_fadeBottomEdge, 0f),
            )
        }
    }

}