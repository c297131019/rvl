package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Description : 递归时统一的操作
 */
interface NestableAdapterAction {
    void onAction(NestableRvAdapter<?, ? extends RvHolder<?>> adapter);
}
