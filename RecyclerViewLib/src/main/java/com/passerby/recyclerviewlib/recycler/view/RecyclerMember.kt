package com.passerby.recyclerviewlib.recycler.view

import android.view.View
import com.passerby.recyclerviewlib.recycler.view.OnBottomEdgeListener
import com.passerby.recyclerviewlib.recycler.view.OnLeftEdgeListener
import com.passerby.recyclerviewlib.recycler.view.OnRightEdgeListener
import com.passerby.recyclerviewlib.recycler.view.OnTopEdgeListener

/**
 * Description : RecyclerView内部成员
 * @Author by chenz
 * @Date 2022/1/18 15:18
 */
internal class RecyclerMember {
    /*空视图*/
    var emptyView: View? = null

    /*状态View，loading、net等状态*/
    var stateView: View? = null

    /*加载更多View*/
    var loadingView: View? = null

    /*滑动顶部边缘监听集合*/
    var onTopEdgeListeners = arrayListOf<OnTopEdgeListener>()

    /*滑动底部边缘监听集合*/
    var onBottomEdgeListeners = arrayListOf<OnBottomEdgeListener>()

    /*滑动左边边缘监听集合*/
    var onLeftEdgeListeners = arrayListOf<OnLeftEdgeListener>()

    /*滑动右边边缘监听集合*/
    var onRightEdgeListeners = arrayListOf<OnRightEdgeListener>()
}