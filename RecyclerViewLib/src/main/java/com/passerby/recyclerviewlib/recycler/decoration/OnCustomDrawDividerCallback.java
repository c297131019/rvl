package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Date: 2021/12/27
 * author: ChenZhiJian
 */
interface OnCustomDrawDividerCallback {
    /**
     * 自定义绘制分割线
     *
     * @param canvas 画布
     * @param paint 画笔，自己维护
     * @param drawRect 绘制的区域
     */
    void onDraw(Canvas canvas, Paint paint, Rect drawRect);
}
