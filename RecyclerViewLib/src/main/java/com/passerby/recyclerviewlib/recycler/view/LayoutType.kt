package com.passerby.recyclerviewlib.recycler.view

/**
 * Description : 布局类型
 * @Author by chenz
 * @Date 2022/1/13 0:18
 */
enum class LayoutType {
    /*线性*/
    Linear,

    /*网格*/
    Grid,

    /*瀑布流*/
    Staggered
}