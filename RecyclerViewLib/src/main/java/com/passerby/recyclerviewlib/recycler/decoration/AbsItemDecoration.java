package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 * Decoration顶级类，只提供一些状态的判断，通用的设置，不涉及任何绘制、测量流程
 * Date: 2021/11/22
 * author: ChenZhiJian
 */
public abstract class AbsItemDecoration extends RecyclerView.ItemDecoration {

    /*方向*/
    protected final static int VERTICAL = LinearLayout.VERTICAL;
    protected final static int HORIZONTAL = LinearLayout.HORIZONTAL;

    /*垂直间距宽度*/
    protected int spacingWidth;
    /*水平间距高度*/
    protected int spacingHeight;

    /*纯色分割线*/
    protected DividerColor dividerColor;

    /*Drawable分割线*/
    protected DividerDrawable dividerDrawable;

    /*测量View的位置*/
    protected Rect mViewBounds = new Rect();

    /*绘制分割线的位置*/
    protected Rect dividerRect = new Rect();

    /*分割线画笔*/
    protected Paint dividerPaint = new Paint();

    /*提供给自定义绘制分割线的画笔*/
    protected Paint backupPaint = new Paint();

    protected OnGetHeaderOrFooterCountCallback onGetCountCallback;

    protected OnCustomDrawDividerCallback onCustomDrawDividerCallback;


    public AbsItemDecoration() {
        this(0, 0);
    }


    public AbsItemDecoration(int spacingWidth, int spacingHeight) {
        this.spacingWidth = spacingWidth;
        this.spacingHeight = spacingHeight;
        dividerPaint.setAntiAlias(true);
        backupPaint.setAntiAlias(true);
    }


    /**
     * 获取View所在列
     *
     * @param holder holder
     */
    protected int getSpanIndex(RecyclerView.ViewHolder holder) {
        ViewGroup.LayoutParams layoutParams = holder.itemView.getLayoutParams();
        if (layoutParams instanceof StaggeredGridLayoutManager.LayoutParams) {
            return ((StaggeredGridLayoutManager.LayoutParams) layoutParams).getSpanIndex();
        } else if (layoutParams instanceof GridLayoutManager.LayoutParams) {
            return ((GridLayoutManager.LayoutParams) layoutParams).getSpanIndex();
        } else if (layoutParams instanceof RecyclerView.LayoutParams) {
            return 0;
        }
        return 0;
    }


    /**
     * 是否垂直方向
     *
     * @param layoutManager LayoutManager
     */
    protected boolean isVerticalOrientation(RecyclerView.LayoutManager layoutManager) {
        return getOrientation(layoutManager) == RecyclerView.VERTICAL;
    }


    /**
     * 获取方向
     *
     * @param layoutManager LayoutManager
     */
    protected int getOrientation(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).getOrientation();
        } else if (layoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) layoutManager).getOrientation();
        } else if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).getOrientation();
        }
        return RecyclerView.VERTICAL;
    }


    /**
     * 获取列数
     *
     * @param layoutManager LayoutManager
     */
    protected int getSpanCount(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof GridLayoutManager) {
            return ((GridLayoutManager) layoutManager).getSpanCount();
        } else if (layoutManager instanceof LinearLayoutManager) {
            return 1;
        }
        return 0;
    }


    /**
     * 获取position在垂直方向所在的位置
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected Gravity getVerticalGravity(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        if (isSingle(layoutManager)) {
            return Gravity.Single;
        } else if (isLastRow(parent, layoutManager, holder, position)) {
            return Gravity.End;
        } else if (isFirstRow(parent, layoutManager, holder, position)) {
            return Gravity.Start;
        } else {
            return Gravity.Center;
        }
    }


    /**
     * 获取position在水平方向所在的位置
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected Gravity getHorizontalGravity(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        if (isSingle(layoutManager)) {
            return Gravity.Single;
        } else if (isLastColumn(parent, layoutManager, holder, position)) {
            return Gravity.End;
        } else if (isFirstColumn(parent, layoutManager, holder, position)) {
            return Gravity.Start;
        } else {
            return Gravity.Center;
        }
    }


    /**
     * 是否只有一个元素
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param layoutManager LayoutManager
     */
    protected boolean isSingle(RecyclerView.LayoutManager layoutManager) {
        return layoutManager.getItemCount() == 1;
    }


    /**
     * 是否只有一行
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param layoutManager LayoutManager
     */
    protected boolean isSingleRow(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                int itemCount = layoutManager.getItemCount();
                int spanCount = getSpanCount(layoutManager);
                return itemCount <= spanCount;
            } else {
                return getSpanCount(layoutManager) == 1;
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                int itemCount = layoutManager.getItemCount();
                int spanCount = getSpanCount(layoutManager);
                return itemCount <= spanCount;
            } else {
                return getSpanCount(layoutManager) == 1;
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }


    /**
     * 是否只有一列
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param layoutManager LayoutManager
     */
    protected boolean isSingleColumn(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                return getSpanCount(layoutManager) == 1;
            } else {
                int itemCount = layoutManager.getItemCount();
                int spanCount = getSpanCount(layoutManager);
                return itemCount <= spanCount;
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                return getSpanCount(layoutManager) == 1;
            } else {
                int itemCount = layoutManager.getItemCount();
                int spanCount = getSpanCount(layoutManager);
                return itemCount <= spanCount;
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    /**
     * 是否数据区第一行
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected boolean isFirstRow(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //小于列数就是第一行，
                return position < getSpanCount(layoutManager);
            } else {
                //span就是水平方向的总行数，获取view分配到的行数，小于1就是第一行
                int spanIndex = ((StaggeredGridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex < 1;
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //小于列数就是第一行，
                return position < getSpanCount(layoutManager);
            } else {
                //span就是水平方向的总行数，获取view分配到的行数，小于1就是第一行
                int spanIndex = ((GridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex < 1;
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //垂直方向只有第一个数据才是第一行
                return position < 1;
            } else {
                //水平方向永远只有一行
                return true;
            }
        }
        return false;
    }


    /**
     * 是否数据区最后一行
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected boolean isLastRow(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        //这个count目前没有判断包含头部尾部，以后需要获取除去头部尾部后的实际dataItem数量
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //这个判断有问题，永远都计算不了最后一行!!!!
                int surplus = layoutManager.getItemCount() % getSpanCount(layoutManager);
                return position + surplus >= layoutManager.getItemCount();
            } else {
                //span就是水平方向的总行数，获取view分配到的行数，等于span-1就是最后一行
                int spanIndex = ((StaggeredGridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex + 1 >= getSpanCount(layoutManager);
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                int spanCount = getSpanCount(layoutManager);
                return (position / spanCount) == (layoutManager.getItemCount() - 1) / spanCount;
            } else {
                //span就是水平方向的总行数，获取view分配到的行数，等于span-1就是最后一行
                int spanIndex = ((GridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex + 1 >= getSpanCount(layoutManager);
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //垂直方向最后一个数据就是最后一行
                return position + 1 >= layoutManager.getItemCount();
            } else {
                //水平方向只有一行
                return true;
            }
        }
        return false;
    }


    /**
     * 是否数据区第一列
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected boolean isFirstColumn(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //获取view分配到的列数，小于1就是第一列
                int spanIndex = ((StaggeredGridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex < 1;
            } else {
                return position < getSpanCount(layoutManager);
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //获取view分配到的列数，小于1就是第一列
                int spanIndex = ((GridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex < 1;
            } else {
                return position < getSpanCount(layoutManager);
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //垂直方向只有一列
                return true;
            } else {
                //水平方向只有第一个数据才是第一列
                return position < 1;
            }
        }
        return false;
    }


    /**
     * 是否数据区最后一列
     * to do 目前无法确保position是除去头部尾部之外的dataItem的索引，后期需要在调用处确定
     *
     * @param parent RecyclerView
     * @param layoutManager LayoutManager
     * @param holder ViewHolder
     * @param position position
     */
    protected boolean isLastColumn(RecyclerView parent, RecyclerView.LayoutManager layoutManager, RecyclerView.ViewHolder holder, int position) {
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //获取当前view所在的列+1，大于等于列数就是最后一列
                int spanIndex = ((StaggeredGridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex + 1 >= getSpanCount(layoutManager);
            } else {
                int surplus = layoutManager.getItemCount() % getSpanCount(layoutManager);
                //这个判断有问题，永远都计算不了最后一列!!!!
                return position + surplus >= layoutManager.getItemCount();
            }
        } else if (layoutManager instanceof GridLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //获取当前view所在的列+1，大于等于列数就是最后一列
                int spanIndex = ((GridLayoutManager.LayoutParams)
                    holder.itemView.getLayoutParams()).getSpanIndex();
                return spanIndex + 1 >= getSpanCount(layoutManager);
            } else {
                int spanCount = getSpanCount(layoutManager);
                return (position / spanCount) == (layoutManager.getItemCount() - 1) / spanCount;
            }
        } else if (layoutManager instanceof LinearLayoutManager) {
            if (isVerticalOrientation(layoutManager)) {
                //垂直方向只有一列
                return true;
            } else {
                //水平方向最后一个数据就是最后一列
                return position + 1 >= layoutManager.getItemCount();
            }
        }
        return false;
    }


    /**
     * 测量绘制分割线的矩形
     *
     * @param style divider样式
     * @param dividerRect 需要将divider绘制的区域填充到这里
     * @param direction 绘制分割线的方向，是垂直的还是水平的，与列表方向无关。怎么传就怎么解析
     * @param left 分割线x轴起始位置
     * @param top 分割线y轴起始位置
     * @param right 分割线x轴结束位置
     * @param bottom 分割线y轴结束位置
     */
    protected Rect measureDividerRect(RecyclerView parent, DividerStyle style, Rect dividerRect,
                                      Gravity verticalGravity, Gravity horizontalGravity,
                                      Direction direction, int left, int top, int right, int bottom) {
        dividerRect.set(left, top, right, bottom);
        return dividerRect;
    }


    /**
     * 获取View的原始四边位置，也就是View的真正大小的四边
     *
     * @param view ViewHolder.itemView
     * @param outBounds 将测量好的坐标填充到这里
     */
    protected void getViewBounds(View view, Rect outBounds) {
        outBounds.set(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
    }


    /**
     * 减去view的margin边距
     *
     * @param view ViewHolder.itemView
     * @param outBounds 将测量好的坐标填充到这里
     */
    protected void minusBoundsWithMargins(View view, Rect outBounds) {
        final RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
        outBounds.set(outBounds.left - lp.leftMargin,
                      outBounds.top - lp.topMargin,
                      outBounds.right + lp.rightMargin,
                      outBounds.bottom + lp.bottomMargin);
    }


    /**
     * 减去view的Translation边距
     *
     * @param view ViewHolder.itemView
     * @param outBounds 将测量好的坐标填充到这里
     */
    protected void minusBoundsWithTranslation(View view, Rect outBounds) {
        outBounds.set(outBounds.left + Math.round(view.getTranslationX()),
                      outBounds.top + Math.round(view.getTranslationY()),
                      outBounds.right + Math.round(view.getTranslationX()),
                      outBounds.bottom + Math.round(view.getTranslationY()));
    }


    /**
     * 获取Divider
     */
    protected DividerStyle getDivider() {
        if (dividerColor != null) {
            return dividerColor;
        } else if (dividerDrawable != null) {
            return dividerDrawable;
        }
        return null;
    }


    /**
     * 设置纯色分割线
     *
     * @param dividerColor 纯色的divider样式
     */
    public void setDividerColor(DividerColor dividerColor) {
        this.dividerColor = dividerColor;
        this.dividerDrawable = null;
        this.onCustomDrawDividerCallback = null;
    }


    /**
     * 设置Drawable分割线
     *
     * @param dividerDrawable Drawable的divider样式
     */
    public void setDividerDrawable(DividerDrawable dividerDrawable) {
        this.dividerDrawable = dividerDrawable;
        this.dividerColor = null;
        this.onCustomDrawDividerCallback = null;
    }


    /**
     * 设置间距的宽度
     *
     * @param spacingWidth 垂直间距的宽度
     */
    public void setSpacingWidth(int spacingWidth) {
        this.spacingWidth = spacingWidth;
    }


    /**
     * 设置间距高度
     *
     * @param spacingHeight 水平间距的高度
     */
    public void setSpacingHeight(int spacingHeight) {
        this.spacingHeight = spacingHeight;
    }


    /**
     * 获取垂直间距的宽度
     */
    public int getSpacingWidth() {
        return spacingWidth;
    }


    /**
     * 获取水平间距的高度
     */
    public int getSpacingHeight() {
        return spacingHeight;
    }


    /**
     * 设置头部尾部回调
     */
    public void setOnGetCountCallback(OnGetHeaderOrFooterCountCallback callback) {
        this.onGetCountCallback = callback;
    }


    /**
     * 设置自定义绘制分割线回调
     */
    public void setOnCustomDrawDividerCallback(OnCustomDrawDividerCallback callback) {
        this.onCustomDrawDividerCallback = callback;
        this.dividerColor = null;
        this.dividerDrawable = null;
    }
}