package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;


import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import androidx.annotation.NonNull;

import static android.util.Log.d;
import static android.util.Log.e;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 */
public abstract class CheckableRvAdapter<Item, Holder extends CheckableRvHolder<Item>> extends RvAdapter<Item, Holder> {

    private static final String TAG = "CheckableRvAdapter";

    /*存储选中的数据，使用弱引用Map，当数据无引用时可以自动移除*/
    private final WeakHashMap<Item, State<Item>> stateCache = new WeakHashMap<>();
    private final List<OnItemCheckListener<Item>> onItemCheckListeners = new ArrayList<>();
    private int maxCheckCount;


    /**
     * 不启用选中功能，当普通Adapter用
     */
    public CheckableRvAdapter(@NonNull Context context) {
        super(context);
    }


    /**
     * 启用选中功能
     */
    public CheckableRvAdapter(@NonNull Context context, int maxCheckCount) {
        super(context);
        this.maxCheckCount = maxCheckCount;
    }


    /**
     * 更新Holder，会传递两层
     * 可以在Adapter{@link #onUpdate(Holder, Object, int, boolean)}或{@link #onUpdate(Holder, Object, int, boolean, List)}直接处理
     * 也可以在Holder{@link Holder#onUpdate(Object, int, boolean)}或{@link Holder#onUpdate(Object, int, boolean, List)}处理
     */
    @Override
    protected void onBindHolder(@NonNull Holder holder, int position, List<Object> payloads) {
        Item item = getItem(position);
        holder.onBind(item, position);
        if (payloads == null || payloads.isEmpty()) {
            holder.onUpdate(item, position, isCheckedItem(item));
            onUpdate(holder, item, position, isCheckedItem(item));
        } else {
            holder.onUpdate(item, position, isCheckedItem(item), payloads);
            onUpdate(holder, item, position, isCheckedItem(item), payloads);
        }
    }


    /**
     * @param item 更新的数据
     * @param position 更新的下标
     * @deprecated 不再使用这个方法更新
     */
    @Override
    @Deprecated
    protected final void onUpdate(@NonNull Holder holder,@NonNull Item item, int position) {
        super.onUpdate(holder, item, position);
    }


    /**
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     * @deprecated 不再使用这个方法更新
     */
    @Override
    @Deprecated
    protected final void onUpdate(@NonNull Holder holder,@NonNull Item item, int position, List<Object> payloads) {
        super.onUpdate(holder, item, position, payloads);
    }


    /**
     * Holder更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     */
    protected void onUpdate(@NonNull Holder holder,@NonNull Item item, int position, boolean isChecked) {
        //to do
    }


    /**
     * Holder 更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     */
    protected void onUpdate(@NonNull Holder holder,@NonNull Item item, int position, boolean isChecked, List<Object> payloads) {
        //to do
    }


    /**
     * Item选中事件，用于处理选中拦截，只需要返回是否允许选中即可
     *
     * @param item 数据
     * @param position 数据下标
     * @param checked 当前操作状态
     * @return 是否允许选中
     */
    protected boolean onItemCheckChange(@NonNull Item item, int position, boolean checked) {
        return true;
    }


    @Override
    public Item setItem(int index, Item item) {
        Item oldItem = super.setItem(index, item);
        //虽然stateMap会主动移除弱引用，但是可能有延迟或者被外面长期持有，所有要主动移除
        removeState(oldItem);
        return oldItem;
    }


    @Override
    public List<Item> setItems(int startIndex, List<Item> items) {
        List<Item> olds = super.setItems(startIndex, items);
        //虽然stateMap会主动移除弱引用，但是可能有延迟或者被外面长期持有，所有要主动移除
        removeStates(olds);
        return olds;
    }


    @Override
    public Item removeItem(int index) {
        Item removed = super.removeItem(index);
        removeState(removed);
        return removed;
    }


    @Override
    public Item removeItem(Item item) {
        removeState(item);
        return super.removeItem(item);
    }


    @Override
    public List<Item> removeItems(List<Item> items) {
        removeStates(items);
        return super.removeItems(items);
    }


    @Override
    public void clearItems() {
        clearChecked();
        super.clearItems();
    }


    /**
     * 添加一个状态，被添加的都是选中的
     */
    private void putState(Item item) {
        stateCache.put(item, new State<>(item, true));
    }


    /**
     * 移除一个状态，被移除的一定是取消选中的，或者是已经不存在列表中的
     */
    private void removeState(Item item) {
        stateCache.remove(item);
    }


    /**
     * 移除状态
     */
    private void removeStates(List<Item> items) {
        for (Item item : items) {
            removeState(item);
        }
    }


    /**
     * 设置可选中的数量，但不会影响已选中的个数
     * 比如之前设置了5，已选了5，然后再设置1，不会取消已有的选择，这时多出来的可以做取消选中
     */
    public void setMaxCheckCount(int checkCount) {
        this.maxCheckCount = checkCount;
    }


    /**
     * 最大选择可选中数
     */
    public int getMaxCheckCount() {
        return maxCheckCount;
    }


    /**
     * 是否单选模式
     */
    private boolean isSingleMode() {
        return maxCheckCount == 1;
    }


    /**
     * 记录选中的数据
     *
     * @param item 选中的数据
     * @param isChecked 是否选中
     * @return 是否操作成功
     */
    public boolean setChecked(Item item, boolean isChecked) {
        if (item == null) {
            e(TAG, "setChecked: item is null");
            return false;
        }
        if (getMaxCheckCount() == 0) {
            e(TAG, "setChecked: maxCheckCount is zero");
            return false;
        }
        int position = getItemPosition(item);
        /*只要有一个返回false就不能选中*/
        if (!checkListenerOpinion(item, position, isChecked) || !onItemCheckChange(item, position, isChecked)) {
            d(TAG, "setChecked: check operation did not pass");
            return false;
        }
        if (position != -1) {
            if (isChecked) {
                /*已选，不重复选择*/
                if (isCheckedItem(item)) {
                    e(TAG, "setChecked: item is already checked");
                    return false;
                }
                /*已选中数大于可选中数，不能选，单选多选判断都是一样的*/
                if (getCheckedSize() >= getMaxCheckCount()) {
                    e(TAG, "setChecked: over checkCount, because checkedSize = " + getCheckedSize() + ", maxCheckCount = " + getMaxCheckCount());
                    return false;
                }
                /*通过上面的条件才可以选中*/
                putState(item);
            } else {
                /*取消选中，如果没有这个值就不需要取消了*/
                if (stateCache.get(item) == null) {
                    e(TAG, "setChecked: item uncheck fail, because not exist");
                    return false;
                }
                /*取消意味着移除*/
                removeState(item);
                notifyItemChanged(position);
            }
            return true;
        } else {
            //选中失败，没有找到这个数据
            e(TAG, "setChecked: not found item");
            return false;
        }
    }


    /**
     * 记录选中的数据
     *
     * @param position 选中数据下标
     * @param isChecked 是否选中
     * @return 是否操作成功
     */
    public boolean setChecked(int position, boolean isChecked) {
        return setChecked(getItem(position), isChecked);
    }


    /**
     * 批量选中
     *
     * @param items 选中的数据
     * @param isChecked 是否选中
     * @return 是否操作成功，但不表示完全成功，只要有一个成功也算成功，具体成功或失败请看日志或调试
     */
    public boolean setChecked(@NonNull List<Item> items, boolean isChecked) {
        if (getCheckedSize() >= getMaxCheckCount()) {
            e(TAG, "setChecked: over checkCount, because checkedSize = " + getCheckedSize() + ", maxCheckCount = " + getMaxCheckCount());
            return false;
        }
        if (getMaxCheckCount() < items.size()) {
            e(TAG, "setChecked: over checkCount, because items.size = " + items.size() + ", maxCheckCount = " + getMaxCheckCount());
            return false;
        }
        boolean result = false;
        for (Item item : items) {
            result |= setChecked(item, isChecked);
        }
        return result;
    }


    /**
     * 批量选中
     *
     * @param positions 选中的下标
     * @param isChecked 是否选中
     * @return 是否操作成功，但不表示完全成功，只要有一个成功也算成功，具体成功或失败请看日志或调试
     */
    public boolean setChecked(boolean isChecked, int... positions) {
        boolean result = false;
        for (int position : positions) {
            result |= setChecked(getItem(position), isChecked);
        }
        return result;
    }


    /**
     * 清除所有选中
     */
    public void clearChecked() {
        for (Item item : stateCache.keySet()) {
            setChecked(item, false);
        }
        stateCache.clear();
    }


    /**
     * 获取选中的个数
     */
    public int getCheckedSize() {
        return stateCache.size();
    }


    /**
     * 返回选中的数据
     */
    public List<Item> getCheckedItems() {
        return new ArrayList<>(stateCache.keySet());
    }


    /**
     * 返回选中的索引
     * 由于存储的时候不记录索引，所以返回的时候需要去查索引，数据量越大性能越差，200条以内10ms左右，
     * 是因为存储的时候记录索引的话，在add或set的时候需要调整已经存储过的数据的位置，也是挺耗时的操作
     * 而获取选中的position不是频繁的操作，所以考虑后决定将耗时放在这里
     */
    public List<Integer> getCheckedPositions() {
        ArrayList<Integer> positions = new ArrayList<>();
        for (Item item : stateCache.keySet()) {
            int position = getItemPosition(item);
            if (position >= 0) {
                positions.add(position);
            }
        }
        return positions;
    }


    /**
     * 判断是否选中position
     *
     * @param position 传入需要判断的position
     */
    public boolean isCheckedPosition(int position) {
        return isCheckedItem(getItem(position));
    }


    /**
     * 判断是否选中Item
     *
     * @param item 传入需要判断的Item
     */
    public boolean isCheckedItem(Item item) {
        return stateCache.containsKey(item);
    }


    /**
     * 添加选中事件
     */
    public void addOnItemCheckListener(OnItemCheckListener<Item> listener) {
        if (!this.onItemCheckListeners.contains(listener)) {
            this.onItemCheckListeners.add(listener);
        }
    }


    /**
     * 移除选中事件
     */
    public void removeOnItemCheckListener(OnItemCheckListener<Item> listener) {
        this.onItemCheckListeners.remove(listener);
    }


    /**
     * 获取选中监听的结果
     */
    private boolean checkListenerOpinion(Item item, int position, boolean checked) {
        boolean result = true;
        for (OnItemCheckListener<Item> listener : onItemCheckListeners) {
            result &= listener.onItemCheckChange(item, position, checked);
        }
        return result;
    }


    private static class State<Item> {
        private Item item;
        private boolean isChecked;


        private State(Item item, boolean isChecked) {
            this.item = item;
            this.isChecked = isChecked;
        }


        public Item getItem() {
            return item;
        }


        public boolean isChecked() {
            return isChecked;
        }
    }
}
