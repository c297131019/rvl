package com.passerby.recyclerviewlib.recycler.adapter;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Description : 关于嵌套Adapter的一些操作
 * 这里是继承AdapterDataObserver，方便做刷新的操作
 * 因为RecyclerView只会对设置给它的Adapter监听刷新操作，如果是嵌套的Adapter，只能算逻辑Adapter，并不具备实际Adapter的功能
 * 所以需要在Adapter里面获取最顶层的Adapter操作，操作的时候需要偏移一下position
 *
 * 关于刷新的逻辑：
 * 在操作notify相关的刷新时，并不会直接刷新，会在回调里收到刷新的请求，然后通知根Adapter再执行刷新
 * 所以会有个现象就是，发起人调用刷新的时候，发起的Adapter会回调监听了它AdapterDataObserver的对象，但这是空的，因为根Adapter还没刷新
 * 等根Adapter刷新的时候会再遍历子Adapter刷新，
 * 这时候肯定会包含发起的Adapter，所以再次刷新的时候又会回调第二次AdapterDataObserver，并且包含localAdapterDataObserver（待定）
 */
class NestableAdapterController<Adapter extends NestableRvAdapter<?, RvHolder<?>>> extends RecyclerView.AdapterDataObserver {
    //标记更新position用加法还是减法
    private static final int UPDATE_POSITION_RANGE_ADD_OP = 0;
    private static final int UPDATE_POSITION_RANGE_MINUS_OP = 1;

    private PositionRange localPositionRange;
    private PositionRange globalPositionRange;
    private final Adapter attachedAdapter;
    private final List<RecyclerView.AdapterDataObserver> localAdapterDataObservers = new ArrayList<>();
    private final HashSet<Integer> itemTypeStore = new HashSet<>();
    /**
     * 用来标记是哪个Adapter发起的刷新，避免在回调中循环，一次性用途
     */
    private String initiatorAdapterId;
    /**
     * 用来标记当前Adapter正在添加/移除中，所有的notifyXxx回调暂停工作
     * 因为Adapter添加/移除时就需要所有Adapter重新定位，这时的索引已经确定好
     * 但还需要通过notifyXxx通知RecyclerView刷新，如果这时候回调还在工作，就会扰乱索引
     * 一句话说明白，notifyXxx回调并不是为addAdapter/removeAdapter工作的
     * 只是为了Adapter.addItem/removeItem而工作
     */
    private boolean suspend;


    public NestableAdapterController(Adapter attachedAdapter) {
        this.attachedAdapter = attachedAdapter;
        //跟Adapter紧密关联，不需要取消注册，因为即使不添加到recyclerView中，adapter仍可以调用API，即使这样也要监听它的行为
        attachedAdapter.registerAdapterDataObserver(this);
        reboot();
    }


    /**
     * 初始化数据，仅仅为了初始化对象可以在接下来的地方可以使用，
     * Adapter被添加时更新一下数据调用{@link NestableRvAdapter#attachedParentAdapter(NestableRvAdapter)} }
     */
    void reboot() {
        localPositionRange = new PositionRange(0, attachedAdapter.getItemSize());
        globalPositionRange = getGlobalPositionRange(attachedAdapter.getRootAdapter(), 0);
    }


    /**
     * Adapter不可用时调用，比如{@link NestableRvAdapter#detachParentAdapter(NestableRvAdapter)}
     * 或{@link NestableRvAdapter#onDetachedFromRecyclerView(RecyclerView)}
     */
    void dispose() {
        localPositionRange = new PositionRange(0, attachedAdapter.getItemSize());
        globalPositionRange = getGlobalPositionRange(attachedAdapter.getRootAdapter(), 0);
    }


    /**
     * 只有当addAdapter/setAdapter/removeAdapter时才需要调用
     */
    void suspend() {
        suspend = true;
    }


    /**
     * 一次性判断，获取完之后就可以重置了
     */
    boolean isSuspend() {
        try {
            return suspend;
        } finally {
            suspend = false;
        }
    }


    @Override
    @SuppressLint("NotifyDataSetChanged")
    public void onChanged() {
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        //如果自己就是根Adapter，收到回调肯定是自己刷新的，所以就不要在自己的回调里面再重复调用
        //只需标记是自己发起的就行了
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        //如果自己的标记为空，说明自己就是发起人，不为空肯定是父级给自己设置的
        //也就是只有自己不是根Adapter并且自己的标记为空的时候才去刷新
        // to do should we notify more cleverly, maybe in v2 滑稽.jpg
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            rootAdapter.notifyDataSetChanged();
        } else {
            //重置自己的标记，这里留个问题，如果不想发起的Adapter二次回调AdapterDataObserver这里就先别重置，留着判断
            //如果无所谓第二次回调，就在这里清空掉
            if (!isSuspend()) {
                if (isInitiator()) {
                    for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                        observer.onChanged();
                    }
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyDataSetChanged();
            });
        }
    }


    @Override
    public void onItemRangeChanged(int positionStart, int itemCount) {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            //发起请求的时候是本地索引，需要转换全局索引，收到请求的时候已经是全局索引就没必要转换了
            rootAdapter.notifyItemRangeChanged(convertGlobalPosition(positionStart), itemCount);
        } else {
            if (!isSuspend()) {
                if (globalPositionRange.contains(positionStart)) {
                    for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                        observer.onItemRangeChanged(positionStart, itemCount);
                    }
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyItemRangeChanged(positionStart, itemCount);
            });
        }
    }


    @Override
    public void onItemRangeChanged(int positionStart, int itemCount, @Nullable Object payload) {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            //发起请求的时候是本地索引，需要转换全局索引，收到请求的时候已经是全局索引就没必要转换了
            rootAdapter.notifyItemRangeChanged(convertGlobalPosition(positionStart), itemCount, payload);
        } else {
            if (!isSuspend()) {
                if (globalPositionRange.contains(positionStart)) {
                    for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                        observer.onItemRangeChanged(positionStart, itemCount, payload);
                    }
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyItemRangeChanged(positionStart, itemCount, payload);
            });
        }
    }


    @Override
    public void onItemRangeInserted(int positionStart, int itemCount) {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            //发起请求的时候是本地索引，需要转换全局索引，收到请求的时候已经是全局索引就没必要转换了
            rootAdapter.notifyItemRangeInserted(convertGlobalPosition(positionStart), itemCount);
        } else {
            if (!isSuspend()) {
                updatePositionRange(UPDATE_POSITION_RANGE_ADD_OP, isInitiator(), positionStart, itemCount);
                if (globalPositionRange.contains(positionStart)) {
                    for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                        observer.onItemRangeInserted(positionStart, itemCount);
                    }
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyItemRangeInserted(positionStart, itemCount);
            });
        }
    }


    @Override
    public void onItemRangeRemoved(int positionStart, int itemCount) {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            //发起请求的时候是本地索引，需要转换全局索引，收到请求的时候已经是全局索引就没必要转换了
            rootAdapter.notifyItemRangeRemoved(convertGlobalPosition(positionStart), itemCount);
        } else {
            if (!isSuspend()) {
                updatePositionRange(UPDATE_POSITION_RANGE_MINUS_OP, isInitiator(), positionStart, itemCount);
                if (globalPositionRange.contains(positionStart)) {
                    for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                        observer.onItemRangeRemoved(positionStart, itemCount);
                    }
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyItemRangeRemoved(positionStart, itemCount);
            });
        }
    }


    @Override
    public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            //发起请求的时候是本地索引，需要转换全局索引，收到请求的时候已经是全局索引就没必要转换了
            rootAdapter.notifyItemMoved(convertGlobalPosition(fromPosition), convertGlobalPosition(toPosition));
        } else {
            //问题，如果AAdapter移动到BAdapter这里是否需要这样判断
            if (globalPositionRange.contains(fromPosition) && globalPositionRange.contains(toPosition)) {
                for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                    observer.onItemRangeMoved(fromPosition, toPosition, itemCount);
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.notifyItemMoved(fromPosition, toPosition);
            });
        }
    }


    @Override
    public void onStateRestorationPolicyChanged() {
        //判断逻辑参考[onChanged()]
        NestableRvAdapter<?, RvHolder<?>> rootAdapter = attachedAdapter.getRootAdapter();
        if (rootAdapter == attachedAdapter && isInitiatorEmpty()) {
            initiatorAdapterId = attachedAdapter.getAdapterId();
        }
        if (rootAdapter != attachedAdapter && isInitiatorEmpty()) {
            rootAdapter.adapterController.initiatorAdapterId = attachedAdapter.getAdapterId();
            rootAdapter.setStateRestorationPolicy(attachedAdapter.getStateRestorationPolicy());
        } else {
            if (!isSuspend()) {
                for (RecyclerView.AdapterDataObserver observer : localAdapterDataObservers) {
                    observer.onStateRestorationPolicyChanged();
                }
            }
            notifyChildrenDataSetChanged(attachedAdapter, resetInitiator(), adapter -> {
                adapter.setStateRestorationPolicy(attachedAdapter.getStateRestorationPolicy());
            });
        }
    }


    /**
     * 通知所有直接子Adapter调用刷新的API
     * 不必担心会频繁刷新，子Adapter只是逻辑，为了触发他们各自的observer，
     * 真正干活的只有最顶层的Adapter，因为RecyclerView只监听了最顶层的Adapter
     *
     * @param notifyAdapter 务必传入最顶层的Adapter进来 {@link NestableRvAdapter#getRootAdapter()}
     */
    private void notifyChildrenDataSetChanged(@NonNull NestableRvAdapter<?, RvHolder<?>> notifyAdapter, String initiatorAdapterId, @NonNull OnNotifyChangeAction action) {
        if (notifyAdapter.getChildrenAdapterCount() != 0) {
            for (NestableRvAdapter<?, RvHolder<?>> childAdapter : notifyAdapter.getInternalChildAdapter()) {
                //在子Adapter刷新前告诉它谁是发起人
                childAdapter.adapterController.initiatorAdapterId = initiatorAdapterId;
                //这里不需要递归调用，子Adapter刷新后，在监听中自然会调它的子Adapter
                action.onAction(childAdapter);
            }
        }
    }


    /**
     * 重置发起人的标记
     */
    private String resetInitiator() {
        try {
            return initiatorAdapterId;
        } finally {
            initiatorAdapterId = "";
        }
    }


    /**
     * 是否有发起人
     */
    private boolean isInitiatorEmpty() {
        return TextUtils.isEmpty(initiatorAdapterId);
    }


    private boolean isInitiator() {
        return attachedAdapter.getAdapterId().equals(initiatorAdapterId);
    }


    /**
     * 更新索引区间，通过{@link #onItemRangeInserted}和{@link #onItemRangeRemoved}变化的时候动态更新
     * 避免每次都全局查询
     *
     * @param flag 增加或减少
     * @param positionStart 起始位置
     * @param itemCount 修改个数
     */
    private void updatePositionRange(int flag, boolean isInitiator, int positionStart, int itemCount) {

        switch (flag) {
            case UPDATE_POSITION_RANGE_ADD_OP:
                //1.自己前面的索引发生了增加，自己的起始顺延增加，结束更新adapter的长度
                //2.自己区域的索引发生了增加，结束更新adapter的长度
                //3.自己后面的索引发生了增加，不用管
                if (isInitiator) {
                    //是自己发起的，校验一下position的合法性
                    if (globalPositionRange.validity(positionStart)) {
                        globalPositionRange.setLength(attachedAdapter.getItemSize());
                        localPositionRange.setLength(attachedAdapter.getItemSize());
                    }
                } else {
                    //不是自己发起的，判断一下是否小于自己，等于自己是为了判断边界，不用担心出问题，因为明确了不是自己发起的
                    if (positionStart <= globalPositionRange.getStartPosition()) {
                        globalPositionRange.startPositionIncrement(itemCount);
                        globalPositionRange.setLength(attachedAdapter.getItemSize());
                        localPositionRange.setLength(attachedAdapter.getItemSize());
                    }
                }
                break;
            case UPDATE_POSITION_RANGE_MINUS_OP:
                //1.自己前面的索引发生了减少，自己的起始跟进减少，结束更新adapter的长度
                //2.自己区域的索引发生了减少，结束更新adapter的长度
                //3.自己后面的索引发生了减少，不用管
                if (isInitiator) {
                    //是自己发起的，判断一下是否包含这个position
                    if (globalPositionRange.contains(positionStart)) {
                        globalPositionRange.setLength(attachedAdapter.getItemSize());
                        localPositionRange.setLength(attachedAdapter.getItemSize());
                    }
                } else {
                    //不是自己发起的，判断一下是否小于自己，等于自己是为了判断边界，不用担心出问题，因为明确了不是自己发起的
                    if (positionStart <= globalPositionRange.getStartPosition()) {
                        globalPositionRange.startPositionDecrement(itemCount);
                        globalPositionRange.setLength(attachedAdapter.getItemSize());
                        localPositionRange.setLength(attachedAdapter.getItemSize());
                    }
                }
                break;
        }
    }


    /**
     * 获取当前Adapter在全局中的起始索引
     * [adapter(0,10), adapter(10,20), adapter(20, 30)]
     *
     * @param adapter 初始调用要传入rootAdapter
     */
    private PositionRange getGlobalPositionRange(NestableRvAdapter<?, RvHolder<?>> adapter, int startPosition) {
        if (adapter == attachedAdapter) {
            return new PositionRange(startPosition, attachedAdapter.getItemSize());
        }
        startPosition += adapter.getItemSize();
        for (NestableRvAdapter<?, RvHolder<?>> childAdapter : adapter.getInternalChildAdapter()) {
            if (childAdapter == attachedAdapter || childAdapter.containsChildrenAdapter(attachedAdapter)) {
                return getGlobalPositionRange(childAdapter, startPosition);
            } else {
                startPosition += childAdapter.getItemCount();
            }
        }
        throw new IllegalStateException("getGlobalPositionRange() adapter is not a valid state");
    }


    /**
     * 记录属于这个Adapter的ItemType
     *
     * @param itemType ViewHolder类型
     */
    void rememberItemType(int itemType) {
        itemTypeStore.add(itemType);
    }


    /**
     * 是否包含这个类型
     *
     * @param itemType ViewHolder类型
     */
    boolean hasItemType(int itemType) {
        return itemTypeStore.contains(itemType);
    }


    /**
     * 刷新当前Adapter相关的全部数据，如果只关心自己Adapter的话，推荐用这个
     * 如果要刷新RecyclerView所有数据，可以使用{@link NestableRvAdapter#notifyDataSetChanged()}，如非需要，不推荐使用
     *
     * 请注意，调用这个刷新，回调的是{@link RecyclerView.AdapterDataObserver#onItemRangeChanged(int, int)}
     * 如果将来有需要控制回调到{@link RecyclerView.AdapterDataObserver#onChanged()}，则需要调整一下逻辑
     */
    public void notifyLocalDataSetChanged() {
        attachedAdapter.notifyItemRangeChanged(globalPositionRange.getStartPosition(), globalPositionRange.getEndPosition());
    }


    /**
     * 当前Adapter所在的全局索引区间是否包含了指定的全局索引
     *
     * @param globalPosition 指定的全局索引
     */
    public boolean isGlobalRangeContainsGlobalPosition(int globalPosition) {
        return globalPositionRange.contains(globalPosition);
    }


    /**
     * 当前Adapter的索引区间是否包含了全局索引
     *
     * @param globalPosition 全局索引
     */
    public boolean isLocalRangeContainsGlobalPosition(int globalPosition) {
        int convertLocalPosition = convertLocalPosition(globalPosition);
        return localPositionRange.contains(convertLocalPosition);
    }


    /**
     * 全局索引转换成当前Adapter的索引
     *
     * @param globalPosition 全局索引
     * @return 如果被转换的索引不在这个Adapter范围，则返回-1
     */
    public int convertLocalPosition(int globalPosition) {
        return globalPosition - globalPositionRange.getStartPosition();
    }


    /**
     * 当前Adapter索引转换成全局索引
     *
     * @param localPosition 当前Adapter的索引
     * @return 如果被转换的索引不在这个Adapter范围，则返回-1
     */
    public int convertGlobalPosition(int localPosition) {
        return globalPositionRange.getStartPosition() + localPosition;
    }


    /**
     * 获取相对全局索引的起始位置
     */
    public int getGlobalStartPosition() {
        return globalPositionRange.getStartPosition();
    }


    /**
     * 获取相对全局索引的结束位置
     */
    public int getGlobalEndPosition() {
        return globalPositionRange.getEndPosition();
    }


    /**
     * 注册本地Adapter的事件
     *
     * @param observer 观察者
     */
    public void registerLocalAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        if (!localAdapterDataObservers.contains(observer)) {
            localAdapterDataObservers.add(observer);
        }
    }


    /**
     * 注销本地Adapter的事件
     *
     * @param observer 观察者
     */
    public void unregisterLocalAdapterDataObserver(@NonNull RecyclerView.AdapterDataObserver observer) {
        localAdapterDataObservers.remove(observer);
    }


    /**
     * 便于操作刷新的API
     */
    public interface OnNotifyChangeAction {
        void onAction(NestableRvAdapter<?, RvHolder<?>> action);
    }
}
