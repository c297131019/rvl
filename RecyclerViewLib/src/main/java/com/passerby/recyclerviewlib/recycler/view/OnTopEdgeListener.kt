package com.passerby.recyclerviewlib.recycler.view

/**
 * Description : 滑动到顶部边缘回调
 * @Author by chenz
 * @Date 2022/1/18 15:29
 */
interface OnTopEdgeListener {
    fun onTopEdge()
}