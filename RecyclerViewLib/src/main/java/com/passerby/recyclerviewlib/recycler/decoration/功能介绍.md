# Decoration相关文档

#### 目前所提供的功能

ItemDecoration：

- LinearItemDecoration
    为LinearLayoutManager提供分割线，间距的处理

- GridItemDecoration
    为GridLayoutManager提供分割线，间距的处理（待定）

- StaggeredGridItemDecoration
    为StaggeredGridLayoutManager提供分割线，间距的处理
  
DividerStyle：

- DividerStyle
    可以为分割线提供上下左右间距，以及左中右对齐的功能
  
- ColorDividerStyle
    在DividerStyle的基础上，可以为分割线设置颜色值
  
- DrawableDividerStyle
    在DividerStyle的基础上，可以为分割线设置Drawable样式