package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;
import android.view.View;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 * 配套使用的Holder
 */
public class RvHolder<Item> extends RecyclerView.ViewHolder {

    private int position;
    public Item item;
    protected Context context;


    public RvHolder(@NonNull Context context, @NonNull View itemView) {
        super(itemView);
        this.context = context;
    }


    /**
     * 当被依附到RecyclerView的时候会调用
     */
    protected void attach() {}


    /**
     * 当被移除RecycleView的时候会被调用
     */
    protected void detach() {}


    /**
     * 绑定数据
     *
     * @param item 当前和Holder绑定的数据
     * @param position 当前和Holder绑定的下标
     */
    void onBind(Item item, int position) {
        this.item = item;
        this.position = position;
    }


    /**
     * 获取当前Holder绑定的数据
     */
    protected Item item() { return item; }


    /**
     * 获取当前Holder绑定的下标
     */
    protected int position() { return position; }


    /**
     * Holder更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     */
    protected void onUpdate(@NonNull Item item, int position) {}


    /**
     * Holder 更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     */
    protected void onUpdate(@NonNull Item item, int position, List<Object> payloads) {}


    /**
     * Item点击事件
     *
     * @param item Holder绑定的Item
     * @param position Item所在的位置
     */
    protected void onItemClick(@NonNull Item item, int position) {}


    /**
     * Item长按事件
     *
     * @param item Holder绑定的Item
     * @param position Item所在的位置
     */
    protected boolean onItemLongClick(@NonNull Item item, int position) { return false; }

}
