package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.CallSuper;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 普通Adapter，不具备特殊功能，只是封装简化了使用，以及有很多数据的操作
 * 单纯的静态数据加载的列表可以使用，动态加载的列表建议使用封装了状态视图和头尾视图的{@link SuperRvAdapter}
 * Date: 2021/11/15
 * author: ChenZhiJian
 */
public abstract class RvAdapter<Item, Holder extends RvHolder<Item>> extends RecyclerView.Adapter<Holder> {

    protected final Context context;
    final List<Item> store = new ArrayList<>();
    private final LayoutInflater layoutInflater;
    private WeakReference<RecyclerView> recyclerViewReference;
    private List<OnItemClickListener<Holder, Item>> onItemClickListeners;
    private List<OnItemLongClickListener<Holder, Item>> onItemLongClickListeners;


    public RvAdapter(@NonNull Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }


    /**
     * 获取布局Id
     */
    protected abstract int getLayoutId(int viewType);

    /**
     * 构造Holder
     *
     * @param context 上下文
     * @param itemView ItemView
     * @param viewType 多布局类型
     */
    protected abstract Holder createHolder(@NonNull Context context, @NonNull View itemView, int viewType);


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return internalCreateHolder(parent, viewType);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        onBindHolder(holder, position, null);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position, @NonNull List<Object> payloads) {
        onBindHolder(holder, position, payloads);
    }


    /**
     * 更新Holder，会传递两层，代替{@link #onBindViewHolder(Holder, int)}和{@link #onBindViewHolder(Holder, int, List)}
     * 可以在Adapter{@link #onUpdate(Holder, Object, int)}或{@link #onUpdate(Holder, Object, int, List)}直接处理
     * 也可以在Holder{@link Holder#onUpdate(Object, int)}或{@link Holder#onUpdate(Object, int, List)}处理
     */
    protected void onBindHolder(@NonNull Holder holder, int position, List<Object> payloads) {
        Item item = getItem(position);
        holder.onBind(item, position);
        if (payloads == null || payloads.isEmpty()) {
            holder.onUpdate(item, position);
            onUpdate(holder, item, position);
        } else {
            holder.onUpdate(item, position, payloads);
            onUpdate(holder, item, position, payloads);
        }
    }


    /**
     * Holder更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     */
    protected void onUpdate(@NonNull Holder holder, @NonNull Item item, int position) {
        //to do
    }


    /**
     * Holder 更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     */
    protected void onUpdate(@NonNull Holder holder, @NonNull Item item, int position, List<Object> payloads) {
        //to do
    }


    /**
     * 内部构造Holder
     */
    private Holder internalCreateHolder(@NonNull ViewGroup parent, int viewType) {
        int layoutId = getLayoutId(viewType);
        View itemView = layoutInflater.inflate(layoutId, parent, false);
        Holder holder = createHolder(context, itemView, viewType);
        itemView.setOnClickListener(v -> internalOnItemClick(holder));
        itemView.setOnLongClickListener(v -> internalOnItemLongClick(holder));
        return holder;
    }


    @Override
    public int getItemCount() {
        return store.size();
    }


    /**
     * 获取当前类型需要占用几行几列的宽高，有需要可以重写
     * 仅对GridLayoutManager和StaggerGridLayoutManager有效
     *
     * @param itemType item类型
     * @return 占用的行宽或列高
     */
    protected int getSpanSize(int itemType) {
        return 1;
    }


    /**
     * 处理Item点击事件
     * 会传递三层，
     * Holder内部会感知点击
     * Adapter内部会感知点击
     * 外部添加事件会感知点击
     */
    protected void internalOnItemClick(Holder holder) {
        holder.onItemClick(holder.item(), holder.position());
        onItemClick(holder, holder.item(), holder.position());
        if (onItemClickListeners != null) {
            for (OnItemClickListener<Holder, Item> listener : onItemClickListeners) {
                listener.onItemClick(holder, holder.item(), holder.position());
            }
        }
    }


    /**
     * Item点击事件
     *
     * @param holder 点击的Holder
     * @param item Holder绑定的Item
     * @param position Item所在的位置
     */
    protected void onItemClick(Holder holder, Item item, int position) {}


    /**
     * 处理Item长按事件
     * 会传递三层，
     * Holder内部会感知长按
     * Adapter内部会感知长按
     * 外部添加事件会感知长按
     */
    private boolean internalOnItemLongClick(Holder holder) {
        boolean result = false;
        result |= holder.onItemLongClick(holder.item(), holder.position());
        result |= onItemLongClick(holder, holder.item(), holder.position());
        if (onItemLongClickListeners != null) {
            for (OnItemLongClickListener<Holder, Item> listener : onItemLongClickListeners) {
                result |= listener.onItemLongClick(holder, holder.item(), holder.position());
            }
        }
        return result;
    }


    /**
     * Item长按事件
     *
     * @param holder 点击的Holder
     * @param item Holder绑定的Item
     * @param position Item所在的位置
     */
    private boolean onItemLongClick(Holder holder, Item item, int position) { return false; }


    @Override
    @CallSuper
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerViewReference = new WeakReference<>(recyclerView);
    }


    @Override
    @CallSuper
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        if (recyclerViewReference != null) {
            recyclerViewReference.clear();
            recyclerViewReference = null;
        }
    }


    /**
     * 获取RecyclerView
     */
    protected RecyclerView getRecyclerView() {
        return recyclerViewReference != null ? recyclerViewReference.get() : null;
    }


    /**
     * 获取颜色
     */
    protected int getColor(@ColorRes int colorRes) {
        return ContextCompat.getColor(context, colorRes);
    }


    /**
     * 获取字符串
     */
    protected String getString(@StringRes int stringRes) {
        return context.getString(stringRes);
    }


    /**
     * 获取Drawable
     */
    protected Drawable getDrawable(@DrawableRes int drawableRes) {
        return ContextCompat.getDrawable(context, drawableRes);
    }

    /**---------------------------------公开的方法---------------------------------**/

    /**
     * 替换数据
     *
     * @param index 需要替换的下标
     * @param item 需要替换的数据
     */
    public Item setItem(int index, Item item) {
        Item old = store.set(index, item);
        notifyItemChanged(index);
        return old;
    }


    /**
     * 替换指定区域的数据
     *
     * @param startIndex 起始的下标
     * @param items 需要替换的数据
     */
    public List<Item> setItems(int startIndex, List<Item> items) {
        List<Item> olds = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            olds.add(store.set(startIndex + i, items.get(i)));
        }
        notifyItemRangeChanged(startIndex, items.size());
        return olds;
    }


    /**
     * 添加单个数据
     *
     * @param item 添加的数据
     */
    public void addItem(Item item) {
        store.add(item);
        notifyItemInserted(store.size() - 1);
    }


    /**
     * 添加单个数据
     *
     * @param item 添加的数据
     */
    public void addItem(int index, Item item) {
        store.add(index, item);
        notifyItemInserted(index);
    }


    /**
     * 添加一组数据
     *
     * @param items 添加的数据
     */
    public void addItems(List<Item> items) {
        int size = store.size();
        store.addAll(items);
        notifyItemRangeInserted(size, items.size());
    }


    /**
     * 添加一组数据插入到指定位置
     *
     * @param index 插入的下标
     * @param items 添加的数据
     */
    public void addItems(int index, List<Item> items) {
        store.addAll(index, items);
        notifyItemRangeInserted(index, items.size());
    }


    /**
     * 移除单个指定位置的数据
     *
     * @param index 移除的下标
     * @return 返回被移除的数据
     */
    public Item removeItem(int index) {
        if (index > store.size() || index < 0) {
            return null;
        }
        Item item = store.remove(index);
        notifyItemRemoved(index);
        return item;
    }


    /**
     * 移除单个数据
     *
     * @param item 需要移除的数据
     */
    public Item removeItem(Item item) {
        int removeIndex = -1;
        for (int i = 0; i < store.size(); i++) {
            if (item == store.get(i)) {
                removeIndex = i;
                break;
            }
        }
        if (removeIndex != -1) {
            store.remove(removeIndex);
            notifyItemRemoved(removeIndex);
            return item;
        }
        return null;
    }


    /**
     * 移除一组数据
     *
     * @param items 需要移除的数据，可以不是连续的
     */
    public List<Item> removeItems(List<Item> items) {
        List<Item> removed = new ArrayList<>();
        for (Item item : items) {
            Item removeItem = removeItem(item);
            if (removeItem != null) {
                removed.add(removeItem);
            }
        }
        return removed;
    }


    /**
     * 清空所有数据
     */
    public void clearItems() {
        store.clear();
        notifyDataSetChanged();
    }


    /**
     * 清除并添加数据
     */
    public void clearAndAddItems(List<Item> items) {
        clearItems();
        addItems(items);
    }


    /**
     * 获取单个数据
     *
     * @param position 数据的下标
     */
    public Item getItem(int position) {
        if (position < store.size() && position >= 0) {
            return store.get(position);
        }
        return null;
    }


    /**
     * 获取数据所在的位置
     *
     * @param item 需要查询的数据
     * todo 有优化的空间
     */
    public int getItemPosition(Item item) {
        return store.indexOf(item);
    }


    /**
     * 获取所有数据
     * 为了避免外部随意的修改数据之后忘记刷新等问题，只提供数据的访问，修改统一使用内部set、add、remove这些方法
     */
    public List<Item> getItems() {
        return new ArrayList<>(store);
    }


    /**
     * 获取数据长度
     */
    public int getItemSize() {
        return store.size();
    }


    /**
     * 添加一个Item点击事件
     */
    public void addOnItemClickListener(OnItemClickListener<Holder, Item> listener) {
        if (onItemClickListeners == null) {
            onItemClickListeners = new ArrayList<>();
        }
        if (onItemClickListeners.contains(listener)) {
            return;
        }
        onItemClickListeners.add(listener);
    }


    /**
     * 移除一个点击事件
     */
    public void removeOnItemClickListener(OnItemClickListener<Holder, Item> listener) {
        if (onItemClickListeners != null) {
            onItemClickListeners.remove(listener);
        }
    }


    /**
     * 添加一个Item长按事件
     */
    public void addOnItemLongClickListener(OnItemLongClickListener<Holder, Item> listener) {
        if (onItemLongClickListeners == null) {
            onItemLongClickListeners = new ArrayList<>();
        }
        if (onItemLongClickListeners.contains(listener)) {
            return;
        }
        onItemLongClickListeners.add(listener);
    }


    /**
     * 移除一个长按事件
     */
    public void removeOnItemLongClickListener(OnItemLongClickListener<Holder, Item> listener) {
        if (onItemLongClickListeners != null) {
            onItemLongClickListeners.remove(listener);
        }
    }

}
