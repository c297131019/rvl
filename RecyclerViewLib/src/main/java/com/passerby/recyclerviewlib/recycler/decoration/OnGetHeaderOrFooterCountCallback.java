package com.passerby.recyclerviewlib.recycler.decoration;

/**
 * Date: 2021/12/23
 * author: ChenZhiJian
 */
public interface OnGetHeaderOrFooterCountCallback {
    /**
     * 获取头部的个数
     */
    int getHeaderCount();

    /**
     * 获取尾部的个数
     */
    int getFooterCount();
}
