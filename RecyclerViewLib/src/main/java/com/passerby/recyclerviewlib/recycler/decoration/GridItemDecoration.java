package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

/**
 * Date: 2021/12/23
 * author: ChenZhiJian
 */
public class GridItemDecoration extends BaseItemDecoration {

    public GridItemDecoration() {
    }


    public GridItemDecoration(int spacingWidth, int spacingHeight) {
        super(spacingWidth, spacingHeight);
    }


    public GridItemDecoration(int spacingWidth, int spacingHeight, boolean isShowVerticalBorder, boolean isShowHorizontalBorder) {
        super(spacingWidth, spacingHeight, isShowVerticalBorder, isShowHorizontalBorder);
    }


    @Override
    public void onDraw(@NonNull Canvas c, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager == null || (dividerColor == null && dividerDrawable == null && onCustomDrawDividerCallback == null)) {
            return;
        }
        if (layoutManager instanceof GridLayoutManager || layoutManager instanceof StaggeredGridLayoutManager) {
            onDrawDivider(c, parent);
        }
    }


    /**
     * 绘制左边分割线
     */
    @Override
    protected void drawViewLeft(RecyclerView parent, Canvas canvas,
                                Gravity verticalGravity, Gravity horizontalGravity,
                                int left, int top, int right, int bottom) {
        if (horizontalGravity == Gravity.Start && (!isShowVerticalBorder() || !getDivider().isDrawLeftBorderDivide())) {
            return;
        }
        if (spacingWidth == 0 && getDivider().getVerticalWidth() == 0) {
            return;
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.LEFT, left, top, right, bottom);
    }


    /**
     * 绘制右边分割线
     */
    @Override
    protected void drawViewRight(RecyclerView parent, Canvas canvas,
                                 Gravity verticalGravity, Gravity horizontalGravity,
                                 int left, int top, int right, int bottom) {
        if (horizontalGravity == Gravity.End && (!isShowVerticalBorder() || !getDivider().isDrawRightBorderDivide())) {
            return;
        }
        if (spacingWidth == 0 && getDivider().getVerticalWidth() == 0) {
            return;
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.RIGHT, left, top, right, bottom);
    }


    /**
     * 绘制上边分割线
     */
    @Override
    protected void drawViewTop(RecyclerView parent, Canvas canvas,
                               Gravity verticalGravity, Gravity horizontalGravity,
                               int left, int top, int right, int bottom) {
        if (verticalGravity == Gravity.Start && (!isShowHorizontalBorder() || !getDivider().isDrawTopBorderDivide())) {
            return;
        }
        if (spacingHeight == 0 && getDivider().getHorizontalHeight() == 0) {
            return;
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.TOP, left, top, right, bottom);
    }


    /**
     * 绘制下边分割线
     */
    @Override
    protected void drawViewBottom(RecyclerView parent, Canvas canvas,
                                  Gravity verticalGravity, Gravity horizontalGravity,
                                  int left, int top, int right, int bottom) {
        if (verticalGravity == Gravity.End && (!isShowHorizontalBorder() || !getDivider().isDrawBottomBorderDivide())) {
            return;
        }
        if (spacingHeight == 0 && getDivider().getHorizontalHeight() == 0) {
            return;
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.BOTTOM, left, top, right, bottom);
    }


    /**
     * 测量需要绘制的分割线的矩形
     */
    @Override
    protected Rect measureDividerRect(RecyclerView parent, DividerStyle style, Rect dividerRect,
                                      Gravity verticalGravity, Gravity horizontalGravity,
                                      Direction direction, int left, int top, int right, int bottom) {
        switch (direction) {
            case LEFT:
            case RIGHT:
                dividerRect.top = top;
                dividerRect.bottom = bottom;
                /*只有一个元素不画分割线*/
                if (verticalGravity == Gravity.Single || horizontalGravity == Gravity.Single) {
                    //TODO
                }
                /*间距比分割线小，以最大的计算*/
                else if (right - left <= style.getVerticalWidth()) {
                    dividerRect.left = left;
                    dividerRect.right = left + style.getVerticalWidth();
                    //right-left是取的max(spacingHeight, style.getHorizontalHeight())，所以这里还需要再次确定是不是spacingHeight > style.getHorizontalHeight()
                    //spacingHeight > style.getHorizontalHeight()说明分割线要调整到spacingHeight相等，不能超过
                    if (spacingHeight > style.getHorizontalHeight()) {
                        dividerRect.top += spacingHeight / 2 - style.getHorizontalHeight() / 2;
                        dividerRect.bottom -= spacingHeight / 2 - style.getHorizontalHeight() / 2;
                    }
                }
                /*间距比分割线大，start表示left*/
                else if (style.getGravity() == Gravity.Start) {
                    dividerRect.left = left;
                    dividerRect.right = left + style.getVerticalWidth();
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    dividerRect.bottom -= spacingHeight - style.getHorizontalHeight();
                }
                /*间距比分割线大，center表示center*/
                else if (style.getGravity() == Gravity.Center) {
                    dividerRect.left = left + Math.round(((right - left) - style.getVerticalWidth()) / 2f);
                    dividerRect.right = dividerRect.left + style.getVerticalWidth();
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    if (spacingHeight > style.getHorizontalHeight()) {
                        dividerRect.top += spacingHeight / 2 - style.getHorizontalHeight() / 2;
                        dividerRect.bottom -= spacingHeight / 2 - style.getHorizontalHeight() / 2;
                    }
                }
                /*间距比分割线大，end表示right*/
                else if (style.getGravity() == Gravity.End) {
                    dividerRect.left = right - style.getVerticalWidth();
                    dividerRect.right = right;
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    dividerRect.top += spacingHeight;
                }
                if (verticalGravity == Gravity.Start) {
                    if (!isShowHorizontalBorder() || !getDivider().isDrawTopBorderDivide()) {
                        //没有显示顶部，不能超出View的顶部
                        dividerRect.top = top;
                        dividerRect.top += Math.max(spacingHeight, style.getHorizontalHeight());
                    }
                    dividerRect.top += style.getVerticalTopPadding();
                }
                if (verticalGravity == Gravity.End) {
                    if (!isShowHorizontalBorder() || !getDivider().isDrawBottomBorderDivide()) {
                        //没有显示底部，不能超出View的底部
                        dividerRect.bottom = bottom;
                        dividerRect.bottom -= Math.max(spacingHeight, style.getHorizontalHeight());
                    }
                    dividerRect.bottom -= style.getVerticalBottomPadding();
                }
                break;
            case TOP:
            case BOTTOM:
                dividerRect.left = left;
                dividerRect.right = right;
                /*只有一个元素不画分割线*/
                if (verticalGravity == Gravity.Single || horizontalGravity == Gravity.Single) {
                    //TODO
                }
                /*间距比分割线小，以最大的计算*/
                else if (bottom - top <= style.getHorizontalHeight()) {
                    dividerRect.top = top;
                    dividerRect.bottom = top + style.getHorizontalHeight();
                    if (spacingWidth > style.getVerticalWidth()) {
                        dividerRect.left += spacingWidth / 2 - style.getVerticalWidth() / 2;
                        dividerRect.right -= spacingWidth / 2 - style.getVerticalWidth() / 2;
                    }
                }
                /*间距比分割线大，start表示top*/
                else if (style.getGravity() == Gravity.Start) {
                    dividerRect.top = top;
                    dividerRect.bottom = top + style.getHorizontalHeight();
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    dividerRect.right -= spacingWidth - style.getVerticalWidth();
                }
                /*间距比分割线大，center表示center*/
                else if (style.getGravity() == Gravity.Center) {
                    dividerRect.top = top + Math.round(((bottom - top) - style.getHorizontalHeight()) / 2f);
                    dividerRect.bottom = dividerRect.top + style.getHorizontalHeight();
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    if (spacingWidth > style.getVerticalWidth()) {
                        dividerRect.left += spacingWidth / 2 - style.getVerticalWidth() / 2;
                        dividerRect.right -= spacingWidth / 2 - style.getVerticalWidth() / 2;
                    }
                }
                /*间距比分割线大，end表示bottom*/
                else if (style.getGravity() == Gravity.End) {
                    dividerRect.top = bottom - style.getHorizontalHeight();
                    dividerRect.bottom = bottom;
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    dividerRect.left += spacingWidth;
                }
                if (horizontalGravity == Gravity.Start) {
                    if (!isShowVerticalBorder() || !getDivider().isDrawLeftBorderDivide()) {
                        //没有显示左边，不能超出View的左部
                        dividerRect.left = left;
                        dividerRect.left += Math.max(spacingWidth, style.getVerticalWidth());
                    }
                    dividerRect.left += style.getHorizontalLeftPadding();
                }
                if (horizontalGravity == Gravity.End) {
                    if (!isShowVerticalBorder() || !getDivider().isDrawRightBorderDivide()) {
                        //没有显示左边，不能超出View的左部
                        dividerRect.right = right;
                        dividerRect.right -= Math.max(spacingWidth, style.getVerticalWidth());
                    }
                    dividerRect.right -= style.getHorizontalRightPadding();
                }
                break;
        }
        return dividerRect;
    }


    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager == null) {
            super.getItemOffsets(outRect, view, parent, state);
            return;
        }
        if (layoutManager instanceof GridLayoutManager || layoutManager instanceof StaggeredGridLayoutManager) {
            measureItemOffsets(outRect, view, parent, layoutManager, state);
        } else {
            super.getItemOffsets(outRect, view, parent, state);
        }
    }


    /**
     * 测量GridLayoutManager类型的间距
     */
    protected void measureItemOffsets(Rect outRect, View view, RecyclerView parent,
                                      RecyclerView.LayoutManager layoutManager, RecyclerView.State state) {
        RecyclerView.ViewHolder holder = parent.getChildViewHolder(view);
        int position = holder.getAbsoluteAdapterPosition();
        int orientation = getOrientation(layoutManager);
        Gravity verticalGravity = getVerticalGravity(parent, layoutManager, holder, position);
        Gravity horizontalGravity = getHorizontalGravity(parent, layoutManager, holder, position);
        int spanCount = getSpanCount(layoutManager);
        int spanIndex = getSpanIndex(holder);
        int spacingWidth = Math.max(this.spacingWidth, (getDivider() != null ? getDivider().getVerticalWidth() : 0));
        int spacingHeight = Math.max(this.spacingHeight, (getDivider() != null ? getDivider().getHorizontalHeight() : 0));
        int left, right, top, bottom;
        //抄来的计算方式，均分之后，View左右的大小是不一致的
        if (orientation == VERTICAL) {
            if (isShowVerticalBorder()) {
                left = (int) (((float) (spanCount - spanIndex)) / spanCount * spacingWidth);
                right = (int) (((float) spacingWidth * (spanCount + 1) / spanCount) - left);
            } else {
                left = spanIndex * spacingWidth / spanCount;
                right = spacingWidth - (spanIndex + 1) * spacingWidth / spanCount;
            }
            top = spacingHeight;
            bottom = spacingHeight;
        } else {
            //水平方向to do
            if (isShowHorizontalBorder()) {
                top = (int) (((float) (spanCount - spanIndex)) / spanCount * spacingHeight);
                bottom = (int) (((float) spacingHeight * (spanCount + 1) / spanCount) - top);
            } else {
                top = spanIndex * spacingHeight / spanCount;
                bottom = spacingHeight - (spanIndex + 1) * spacingHeight / spanCount;
            }
            left = spacingWidth;
            right = spacingWidth;
        }
        //...这里需要过滤头部和尾部
        if (isSingle(layoutManager)) {
            //只有一个元素，只处理四边边距
            outRect.set(isShowVerticalBorder() ? left : 0, isShowHorizontalBorder() ? top : 0,
                        isShowVerticalBorder() ? right : 0, isShowHorizontalBorder() ? bottom : 0);
        } else {
            switch (verticalGravity) {
                case Start:
                    switch (horizontalGravity) {
                        case Start:
                            outRect.set(isShowVerticalBorder() ? left : 0, isShowHorizontalBorder() ? top : 0, right, bottom);
                            break;
                        case Center:
                            if (orientation == VERTICAL) {
                                outRect.set(left, isShowHorizontalBorder() ? top : 0, right, bottom);
                            } else {
                                outRect.set(0, isShowHorizontalBorder() ? top : 0, right, bottom);
                            }
                            break;
                        case End:
                            if (orientation == VERTICAL) {
                                outRect.set(left, isShowHorizontalBorder() ? top : 0, isShowVerticalBorder() ? right : 0, bottom);
                            } else {
                                outRect.set(isSingleColumn(layoutManager) ? (isShowVerticalBorder() ? left : 0) : 0, top, isShowVerticalBorder() ? right : 0, bottom);
                            }
                            break;
                    }
                    break;
                case Center:
                    switch (horizontalGravity) {
                        case Start:
                            if (orientation == VERTICAL) {
                                outRect.set(isShowVerticalBorder() ? left : 0, 0, right, bottom);
                            } else {
                                outRect.set(isShowVerticalBorder() ? left : 0, top, right, bottom);
                            }
                            break;
                        case Center:
                            if (orientation == VERTICAL) {
                                outRect.set(left, 0, right, bottom);
                            } else {
                                outRect.set(0, top, right, bottom);
                            }
                            break;
                        case End:
                            if (orientation == VERTICAL) {
                                outRect.set(left, 0, isShowVerticalBorder() ? right : 0, bottom);
                            } else {
                                outRect.set(isSingleColumn(layoutManager) ? (isShowVerticalBorder() ? left : 0) : 0, top, isShowHorizontalBorder() ? right : 0, bottom);
                            }
                            break;
                    }
                    break;
                case End: {
                    switch (horizontalGravity) {
                        case Start:
                            if (orientation == VERTICAL) {
                                outRect.set(isShowVerticalBorder() ? left : 0, isSingleRow(layoutManager) ? (isShowHorizontalBorder() ? top : 0) : 0, right, isShowHorizontalBorder() ? bottom : 0);
                            } else {
                                outRect.set(isShowVerticalBorder() ? left : 0, top, right, isShowHorizontalBorder() ? bottom : 0);
                            }
                            break;
                        case Center:
                            if (orientation == VERTICAL) {
                                outRect.set(left, isSingleRow(layoutManager) ? (isShowHorizontalBorder() ? top : 0) : 0, right, isShowHorizontalBorder() ? bottom : 0);
                            } else {
                                outRect.set(0, top, right, isShowHorizontalBorder() ? bottom : 0);
                            }
                            break;
                        case End:
                            if (orientation == VERTICAL) {
                                outRect.set(left, isSingleRow(layoutManager) ? (isShowHorizontalBorder() ? top : 0) : 0, isShowVerticalBorder() ? right : 0, isShowHorizontalBorder() ? bottom : 0);
                            } else {
                                outRect.set(isSingleColumn(layoutManager) ? (isShowVerticalBorder() ? left : 0) : 0, top, isShowVerticalBorder() ? right : 0, isShowHorizontalBorder() ? bottom : 0);
                            }
                            break;
                    }
                    break;
                }
            }
        }
    }
}
