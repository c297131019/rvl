package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;
import android.view.View;


import java.util.List;

import androidx.annotation.NonNull;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 */
public class CheckableRvHolder<Item> extends RvHolder<Item> {
    public CheckableRvHolder(@NonNull Context context, @NonNull View itemView) {
        super(context, itemView);
    }


    /**
     * @param item 更新的数据
     * @param position 更新的下标
     */
    @Override
    protected final void onUpdate(@NonNull Item item, int position) {
        super.onUpdate(item, position);
    }


    /**
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     */
    @Override
    protected final void onUpdate(@NonNull Item item, int position, List<Object> payloads) {
        super.onUpdate(item, position, payloads);
    }


    /**
     * Holder更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     * @param isChecked 是否选中
     */
    protected void onUpdate(@NonNull Item item, int position, boolean isChecked) {}


    /**
     * Holder 更新的方法
     *
     * @param item 更新的数据
     * @param position 更新的下标
     * @param payloads 局部更新的标识
     * @param isChecked 是否选中
     */
    protected void onUpdate(@NonNull Item item, int position, boolean isChecked, List<Object> payloads) {}
}
