package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Description : 区间
 */
class PositionRange {
    private int startPosition;
    private int length;


    public PositionRange(int startPosition, int length) {
        this.startPosition = startPosition;
        this.length = length;
    }


    public int getStartPosition() {
        return startPosition;
    }


    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }


    public void setLength(int length) {
        this.length = length;
    }


    public int getLength() {
        return length;
    }


    public int getEndPosition() {
        return startPosition + Math.max(length - 1, 0);
    }


    public void startPositionIncrement(int value) {
        startPosition += value;
    }


    public void startPositionDecrement(int value) {
        startPosition -= value;
    }


    public void lengthIncrement(int value) {
        length += value;
    }


    public void lengthDecrement(int value) {
        length -= value;
    }


    /**
     * 适用于区间增加的判断，一般都是往区间中间或者尾部+1增考，这样就需要多判断区间+1的长度
     */
    public boolean validity(int valuePosition) {
        return valuePosition >= startPosition && valuePosition <= startPosition + length;
    }


    /**
     * 使用于区间内的判断，也适用于减少区间，减少区间必然是在区间内减少
     */
    public boolean contains(int valuePosition) {
        return valuePosition >= startPosition && valuePosition <= getEndPosition();
    }


    public boolean isEmpty() {
        return startPosition == 0 && getEndPosition() == 0;
    }


    public void copy(PositionRange destRange) {
        destRange.setStartPosition(startPosition);
        destRange.setLength(length);
    }
}
