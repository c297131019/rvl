package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 * Item长按事件
 */
public interface OnItemLongClickListener<Holder, Item> {
    boolean onItemLongClick(Holder holder, Item item, int position);
}