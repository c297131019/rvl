package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Date: 2021/12/23
 * author: ChenZhiJian
 */
public class LinearItemDecoration extends BaseItemDecoration {

    /*是否显示垂直方向的左边边缘间距*/
    private boolean isShowVerticalLeftBorder;
    /*是否显示水平方向的上边边缘间距*/
    private boolean isShowHorizontalTopBorder;
    /*是否显示垂直方向的右边边缘间距*/
    private boolean isShowVerticalRightBorder;
    /*是否显示水平方向的下边边缘间距*/
    private boolean isShowHorizontalBottomBorder;


    public LinearItemDecoration() {
    }


    public LinearItemDecoration(int spacingWidth, int spacingHeight) {
        super(spacingWidth, spacingHeight);
    }


    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager == null || (dividerColor == null && dividerDrawable == null && onCustomDrawDividerCallback == null)) {
            return;
        }
        if (isSingle(layoutManager) || isSingleRow(layoutManager) || isSingleColumn(layoutManager)) {
            onDrawDivider(canvas, parent);
        }
    }


    @Override
    protected void drawViewLeft(RecyclerView parent, Canvas canvas,
                                Gravity verticalGravity, Gravity horizontalGravity,
                                int left, int top, int right, int bottom) {
        if (spacingWidth == 0 && getDivider().getVerticalWidth() == 0) {
            return;
        }
        //横向的只有第一个可以画左边
        if (!isVerticalOrientation(parent.getLayoutManager())) {
            if (!getDivider().isDrawLeftBorderDivide() && horizontalGravity == Gravity.Start) {
                return;
            }
        } else {
            if (!getDivider().isDrawLeftBorderDivide()) {
                return;
            }
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.LEFT, left, top, right, bottom);
    }


    @Override
    protected void drawViewRight(RecyclerView parent, Canvas canvas,
                                 Gravity verticalGravity, Gravity horizontalGravity,
                                 int left, int top, int right, int bottom) {
        if (spacingWidth == 0 && getDivider().getVerticalWidth() == 0) {
            return;
        }
        //横向的只有最后一个可以画右边
        if (!isVerticalOrientation(parent.getLayoutManager())) {
            if ((!isShowVerticalLeftBorder || !getDivider().isDrawRightBorderDivide()) && horizontalGravity == Gravity.End) {
                return;
            }
        } else {
            if (!isShowVerticalLeftBorder || !getDivider().isDrawRightBorderDivide()) {
                return;
            }
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.RIGHT, left, top, right, bottom);
    }


    @Override
    protected void drawViewTop(RecyclerView parent, Canvas canvas,
                               Gravity verticalGravity, Gravity horizontalGravity,
                               int left, int top, int right, int bottom) {
        if (spacingHeight == 0 && getDivider().getHorizontalHeight() == 0) {
            return;
        }
        //垂直的只有第一个可以画上边
        if (isVerticalOrientation(parent.getLayoutManager())
            && verticalGravity != Gravity.Start) {
            return;
        }
        if (!isShowHorizontalTopBorder || !getDivider().isDrawTopBorderDivide()) {
            return;
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.TOP, left, top, right, bottom);
    }


    @Override
    protected void drawViewBottom(RecyclerView parent, Canvas canvas,
                                  Gravity verticalGravity, Gravity horizontalGravity,
                                  int left, int top, int right, int bottom) {
        if (spacingHeight == 0 && getDivider().getHorizontalHeight() == 0) {
            return;
        }
        if (isVerticalOrientation(parent.getLayoutManager())) {
            if ((!isShowHorizontalBottomBorder || !getDivider().isDrawBottomBorderDivide()) && verticalGravity == Gravity.End) {
                return;
            }
        } else {
            if (!isShowHorizontalBottomBorder || !getDivider().isDrawBottomBorderDivide()) {
                return;
            }
        }
        drawDivider(parent, canvas, verticalGravity, horizontalGravity, Direction.BOTTOM, left, top, right, bottom);
    }


    /**
     * 测量需要绘制的分割线的矩形
     */
    @Override
    protected Rect measureDividerRect(RecyclerView parent, DividerStyle style, Rect dividerRect,
                                      Gravity verticalGravity, Gravity horizontalGravity,
                                      Direction direction, int left, int top, int right, int bottom) {
        boolean isVertical = isVerticalOrientation(parent.getLayoutManager());
        switch (direction) {
            case LEFT:
            case RIGHT:
                dividerRect.top = top;
                dividerRect.bottom = bottom;
                /*只有一个元素不画分割线*/
                if (verticalGravity == Gravity.Single || horizontalGravity == Gravity.Single) {
                    //TODO
                }
                /*间距比分割线小，以最大的计算*/
                else if (right - left <= style.getVerticalWidth()) {
                    dividerRect.left = left;
                    dividerRect.right = left + style.getVerticalWidth();
                    //right-left是取的max(spacingHeight, style.getHorizontalHeight())，所以这里还需要再次确定是不是spacingHeight > style.getHorizontalHeight()
                    //spacingHeight > style.getHorizontalHeight()说明分割线要调整到spacingHeight相等，不能超过
                    if (spacingHeight > style.getHorizontalHeight()) {
                        dividerRect.top += spacingHeight / 2 - style.getHorizontalHeight() / 2;
                        dividerRect.bottom -= spacingHeight / 2 - style.getHorizontalHeight() / 2;
                    }
                }
                /*间距比分割线大，start表示left*/
                else if (style.getGravity() == Gravity.Start) {
                    dividerRect.left = left;
                    dividerRect.right = left + style.getVerticalWidth();
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    dividerRect.bottom -= spacingHeight - style.getHorizontalHeight();
                }
                /*间距比分割线大，center表示center*/
                else if (style.getGravity() == Gravity.Center) {
                    dividerRect.left = left + Math.round(((right - left) - style.getVerticalWidth()) / 2f);
                    dividerRect.right = dividerRect.left + style.getVerticalWidth();
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    if (spacingHeight > style.getHorizontalHeight()) {
                        dividerRect.top += spacingHeight / 2 - style.getHorizontalHeight() / 2;
                        dividerRect.bottom -= spacingHeight / 2 - style.getHorizontalHeight() / 2;
                    }
                }
                /*间距比分割线大，end表示right*/
                else if (style.getGravity() == Gravity.End) {
                    dividerRect.left = right - style.getVerticalWidth();
                    dividerRect.right = right;
                    //默认是跟View的高度对齐的，手动调整避免穿过横向的分割线
                    dividerRect.top += spacingHeight;
                }
                if (!isVertical || verticalGravity == Gravity.Start) {
                    if (!isShowHorizontalTopBorder || !style.isDrawTopBorderDivide() || style.getHorizontalHeight() == 0) {
                        //没有显示顶部，不能超出View的顶部
                        dividerRect.top = top;
                        dividerRect.top += Math.max(spacingHeight, style.getHorizontalHeight());
                    }
                    dividerRect.top += style.getVerticalTopPadding();
                }
                if (!isVertical || verticalGravity == Gravity.End) {
                    if (!isShowHorizontalBottomBorder || !style.isDrawBottomBorderDivide() || style.getHorizontalHeight() == 0) {
                        //没有显示底部，不能超出View的底部
                        dividerRect.bottom = bottom;
                        dividerRect.bottom -= Math.max(spacingHeight, style.getHorizontalHeight());
                    }
                    dividerRect.bottom -= style.getVerticalBottomPadding();
                }
                break;
            case TOP:
            case BOTTOM:
                dividerRect.left = left;
                dividerRect.right = right;
                /*只有一个元素不画分割线*/
                if (verticalGravity == Gravity.Single || horizontalGravity == Gravity.Single) {
                    //TODO
                }
                /*间距比分割线小，以最大的计算*/
                else if (bottom - top <= style.getHorizontalHeight()) {
                    dividerRect.top = top;
                    dividerRect.bottom = top + style.getHorizontalHeight();
                    if (spacingWidth > style.getVerticalWidth()) {
                        dividerRect.left += spacingWidth / 2 - style.getVerticalWidth() / 2;
                        dividerRect.right -= spacingWidth / 2 - style.getVerticalWidth() / 2;
                    }
                }
                /*间距比分割线大，start表示top*/
                else if (style.getGravity() == Gravity.Start) {
                    dividerRect.top = top;
                    dividerRect.bottom = top + style.getHorizontalHeight();
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    dividerRect.right -= spacingWidth - style.getVerticalWidth();
                }
                /*间距比分割线大，center表示center*/
                else if (style.getGravity() == Gravity.Center) {
                    dividerRect.top = top + Math.round(((bottom - top) - style.getHorizontalHeight()) / 2f);
                    dividerRect.bottom = dividerRect.top + style.getHorizontalHeight();
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    if (spacingWidth > style.getVerticalWidth()) {
                        dividerRect.left += spacingWidth / 2 - style.getVerticalWidth() / 2;
                        dividerRect.right -= spacingWidth / 2 - style.getVerticalWidth() / 2;
                    }
                }
                /*间距比分割线大，end表示bottom*/
                else if (style.getGravity() == Gravity.End) {
                    dividerRect.top = bottom - style.getHorizontalHeight();
                    dividerRect.bottom = bottom;
                    //默认是跟View的宽度对齐的，手动调整避免穿过竖向的分割线
                    dividerRect.left += spacingWidth;
                }
                if (isVertical || horizontalGravity == Gravity.Start) {
                    if (!isShowVerticalLeftBorder || !style.isDrawLeftBorderDivide() || style.getVerticalWidth() == 0) {
                        //没有显示左边，不能超出View的左部
                        dividerRect.left = left;
                        dividerRect.left += Math.max(spacingWidth, style.getVerticalWidth());
                    }
                    dividerRect.left += style.getHorizontalLeftPadding();
                }
                if (isVertical || horizontalGravity == Gravity.End) {
                    if (!isShowVerticalRightBorder || !style.isDrawRightBorderDivide() || style.getVerticalWidth() == 0) {
                        //没有显示左边，不能超出View的左部
                        dividerRect.right = right;
                        dividerRect.right -= Math.max(spacingWidth, style.getVerticalWidth());
                    }
                    dividerRect.right -= style.getHorizontalRightPadding();
                }
                break;
        }
        return dividerRect;
    }


    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if (layoutManager == null) {
            super.getItemOffsets(outRect, view, parent, state);
            return;
        }
        if (isSingle(layoutManager) || isSingleRow(layoutManager) || isSingleColumn(layoutManager)) {
            measureItemOffsets(outRect, view, parent, layoutManager, state);
        } else {
            super.getItemOffsets(outRect, view, parent, state);
        }
    }


    /**
     * 测量LinearLayoutManager类型的间距
     */
    protected void measureItemOffsets(Rect outRect, View view, RecyclerView parent,
                                      RecyclerView.LayoutManager layoutManager, RecyclerView.State state) {
        RecyclerView.ViewHolder holder = parent.getChildViewHolder(view);
        int position = holder.getAbsoluteAdapterPosition();
        int orientation = getOrientation(layoutManager);
        int spacingWidth = Math.max(this.spacingWidth, (getDivider() != null ? getDivider().getVerticalWidth() : 0));
        int spacingHeight = Math.max(this.spacingHeight, (getDivider() != null ? getDivider().getHorizontalHeight() : 0));
        //...这里需要过滤头部和尾部
        if (isSingle(layoutManager)) {
            //只有一个元素，只考虑四边边距
            outRect.set(
                isShowVerticalLeftBorder ? spacingWidth : 0,
                isShowHorizontalTopBorder ? spacingHeight : 0,
                isShowVerticalRightBorder ? spacingWidth : 0,
                isShowHorizontalBottomBorder ? spacingHeight : 0);
        } else {
            if (orientation == VERTICAL) {
                switch (getVerticalGravity(parent, layoutManager, holder, position)) {
                    case Start:
                        //第一行只处理上边界间距，不处理下边界，上边界没有分割线，不需要处理
                        outRect.set(isShowVerticalLeftBorder ? spacingWidth : 0,
                                    isShowHorizontalTopBorder ? spacingHeight : 0,
                                    isShowVerticalRightBorder ? spacingWidth : 0,
                                    0);
                        break;
                    case Center:
                        //中间只处理顶部间距，不处理下边界，中间会有分割线，间距与分割线取最大值
                        outRect.set(isShowVerticalLeftBorder ? spacingWidth : 0,
                                    spacingHeight,
                                    isShowVerticalRightBorder ? spacingWidth : 0,
                                    0);
                        break;

                    case End:
                        //最后一行既处理顶部间距，也处理下边界，上边界会有分割线，间距与分割线取最大值
                        outRect.set(isShowVerticalLeftBorder ? spacingWidth : 0,
                                    spacingHeight,
                                    isShowVerticalRightBorder ? spacingWidth : 0,
                                    isShowHorizontalBottomBorder ? spacingHeight : 0);
                        break;
                }
            } else {
                switch (getHorizontalGravity(parent, layoutManager, holder, position)) {
                    case Start:
                        //第一行只处理左边界，不处理右边界，左边没有分割线，不需要处理
                        outRect.set(isShowVerticalLeftBorder ? spacingWidth : 0,
                                    isShowHorizontalTopBorder ? spacingHeight : 0,
                                    0,
                                    isShowHorizontalBottomBorder ? spacingHeight : 0);
                        break;

                    case Center:
                        //中间只处理左边界，不需要处理右边界，左边会有分割线，间距与分割线取最大值
                        outRect.set(spacingWidth,
                                    isShowHorizontalTopBorder ? spacingHeight : 0,
                                    0,
                                    isShowHorizontalBottomBorder ? spacingHeight : 0);
                        break;
                    case End:
                        //最后一行处理左边界，也处理右边界，左边会有分割线，间距与分割线取最大值
                        outRect.set(spacingWidth,
                                    isShowHorizontalTopBorder ? spacingHeight : 0,
                                    isShowVerticalRightBorder ? spacingWidth : 0,
                                    isShowHorizontalBottomBorder ? spacingHeight : 0);
                        break;
                }
            }
        }
    }


    /**
     * 是否显示垂直方向的左边边距，与orientation无关，是屏幕的垂直方向
     */
    public void setShowVerticalLeftBorder(boolean showVerticalLeftBorder) {
        isShowVerticalLeftBorder = showVerticalLeftBorder;
        super.setShowVerticalBorder(isShowVerticalLeftBorder && isShowVerticalRightBorder);
    }


    /**
     * 是否显示水平方向的上边边距，与orientation无关，是屏幕的垂直方向
     */
    public void setShowHorizontalTopBorder(boolean showHorizontalTopBorder) {
        isShowHorizontalTopBorder = showHorizontalTopBorder;
        super.setShowVerticalBorder(isShowHorizontalTopBorder && isShowHorizontalBottomBorder);
    }


    /**
     * 是否显示垂直方向的右边边距，与orientation无关，是屏幕的垂直方向
     */
    public void setShowVerticalRightBorder(boolean showVerticalRightBorder) {
        isShowVerticalRightBorder = showVerticalRightBorder;
        super.setShowVerticalBorder(isShowVerticalLeftBorder && isShowVerticalRightBorder);
    }


    /**
     * 是否显示水平方向的下边边距，与orientation无关，是屏幕的垂直方向
     */
    public void setShowHorizontalBottomBorder(boolean showHorizontalBottomBorder) {
        isShowHorizontalBottomBorder = showHorizontalBottomBorder;
        super.setShowVerticalBorder(isShowHorizontalTopBorder && isShowHorizontalBottomBorder);
    }


    /**
     * 是否显示垂直方向的左边边距，与orientation无关，是屏幕的垂直方向
     */
    public boolean isShowVerticalLeftBorder() {
        return isShowVerticalLeftBorder;
    }


    /**
     * 是否显示水平方向的上边边距，与orientation无关，是屏幕的垂直方向
     */
    public boolean isShowHorizontalTopBorder() {
        return isShowHorizontalTopBorder;
    }


    /**
     * 是否显示垂直方向的右边边距，与orientation无关，是屏幕的垂直方向
     */
    public boolean isShowVerticalRightBorder() {
        return isShowVerticalRightBorder;
    }


    /**
     * 是否显示水平方向的下边边距，与orientation无关，是屏幕的垂直方向
     */
    public boolean isShowHorizontalBottomBorder() {
        return isShowHorizontalBottomBorder;
    }


    @Override
    public void setShowVerticalBorder(boolean isShow) {
        isShowVerticalLeftBorder = isShow;
        isShowVerticalRightBorder = isShow;
        super.setShowVerticalBorder(isShow);
    }


    @Override
    public void setShowHorizontalBorder(boolean isShow) {
        isShowHorizontalTopBorder = isShow;
        isShowHorizontalBottomBorder = isShow;
        super.setShowHorizontalBorder(isShow);
    }
}
