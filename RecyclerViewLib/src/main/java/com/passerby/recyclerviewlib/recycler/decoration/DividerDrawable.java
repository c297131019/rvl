package com.passerby.recyclerviewlib.recycler.decoration;

import android.graphics.drawable.Drawable;

/**
 * Date: 2021/11/22
 * author: ChenZhiJian
 */
public class DividerDrawable extends DividerStyle {
    private Drawable drawable;


    public DividerDrawable(Drawable drawable) {
        this.drawable = drawable;
    }


    public Drawable getDrawable() {
        return drawable;
    }
}
