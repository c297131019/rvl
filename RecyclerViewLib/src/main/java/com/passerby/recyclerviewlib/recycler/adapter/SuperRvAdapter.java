package com.passerby.recyclerviewlib.recycler.adapter;

import android.content.Context;

import androidx.annotation.NonNull;

/**
 * Description : 功能全面的Adapter，可以嵌套Adapter，可以设置状态视图，头尾视图
 */
public abstract class SuperRvAdapter<Item, Holder extends RvHolder<Item>> extends NestableRvAdapter<Item, Holder> {

    public SuperRvAdapter(@NonNull Context context) {
        super(context);
    }

}
