package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 */
public enum CheckMode {
    /*单选*/
    Single,
    /*多选*/
    Multi
}
