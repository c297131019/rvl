package com.passerby.recyclerviewlib.recycler.view

import android.view.View
import com.passerby.recyclerviewlib.recycler.adapter.RvHolder

/**
 * Description : RecyclerView支持的功能
 * @Author by chenz
 * @Date 2022/1/13 18:57
 */
interface RecyclerViewFeature {

    /**
     * 设置状态视图，加载、网络错误或者其他之类的
     */
    fun setStateView(view: View)

    /**
     * 获取状态视图
     */
    fun getStateView(view: View)

    /**
     * 是否在显示状态视图
     */
    fun isStateViewVisible(): Boolean

    /**
     * 设置空视图
     */
    fun setEmptyView(view: View)

    /**
     * 获取空视图
     */
    fun getEmptyView(): View

    /**
     * 是否在显示空视图
     */
    fun isEmptyViewVisible(): Boolean

    /**
     * 设置加载更多视图
     */
    fun setLoadingView(view: View)

    /**
     * 获取加载更多视图
     */
    fun getLoadingView(): View

    /**
     * 是否在显示加载更多
     */
    fun isLoadingViewVisible(): Boolean

    /**
     * 添加头部
     */
    fun addHeaderHolder(view: View)

    /**
     * 移除头部
     */
    fun removeHeaderHolder(view: View)

    /**
     * 添加尾部
     */
    fun addFooterHolder(view: View)

    /**
     * 移除尾部
     */
    fun removeFooterHolder(view: View)

    /**
     * 获取头部个数
     */
    fun getHeaderCount(): Int

    /**
     * 移除头部个数
     */
    fun getFooterCount(): Int

    /**
     * 根据Holder类型查找出所有在缓存中存在的类型
     */
    fun getHoldersByType(type: Int): List<RvHolder<*>>

}