package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 * Item点击事件
 */
public interface OnItemClickListener<Holder, Item> {
    void onItemClick(Holder holder, Item item, int position);
}