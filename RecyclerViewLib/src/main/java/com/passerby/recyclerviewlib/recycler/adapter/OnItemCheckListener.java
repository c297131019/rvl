package com.passerby.recyclerviewlib.recycler.adapter;

/**
 * Date: 2021/11/15
 * author: ChenZhiJian
 * Item的选中事件
 */
public interface OnItemCheckListener<Item> {
    /**
     * Item选中事件，用于处理选中拦截，只需要返回是否允许选中即可
     *
     * @param item 数据
     * @param position 数据下标
     * @param checked 当前操作状态
     * @return 是否允许选中
     */
    boolean onItemCheckChange(Item item, int position, boolean checked);
}
